%define BOOTADDR 0x7C00

[ORG BOOTADDR]
[BITS 16]

%define SECTORSIZE 0x200
%define PAGE_SIZE 0x1000

; Stack grows backward, and there is free space before us, so just use it
%define STACKADDR BOOTADDR

_bootloader_entry:
	jmp dword 0:_bootloader_entry.post_cs_reset ; Reset CS to 0

	.post_cs_reset:
	xor eax, eax
	mov ebx, eax
	mov ecx, eax
	mov edx, eax
	mov edi, eax
	mov esi, eax
	mov ebp, eax
	push dword eax
	popfd

	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	mov esp, STACKADDR

_clear_bss:
	lea edi, [__bss.start]
	mov ecx, __bss.end - __bss.start
	xor al, al
	rep stosb

_vbe_dump:
	mov eax, 0x4F00
	lea edi, [vbe_info_buf]
	mov dword [vbe_info_buf.Sig], "VBE2"
	int 10h

	lea esi, [vbe_info_buf]
	mov ecx, vbe_info_buf.end - vbe_info_buf.start
	call _dump

	; VBE Set Mode
	mov eax, 0x4F02
	mov ebx, 0x110
	int 10h

	out 0xE9, al
	mov al, ah
	out 0xE9, al

	; VBE Get Mode
	mov eax, 0x4F03
	xor ebx, ebx
	int 10h

	out 0xE9, al
	mov al, ah
	out 0xE9, al

	mov al, bl
	out 0xE9, al
	mov al, bh
	out 0xE9, al

	; VBE Get Mode Info
	mov eax, 0x4F01
	mov ecx, 0x110
	lea edi, [vbe_mode_buf]
	int 10h

	out 0xE9, al
	mov al, ah
	out 0xE9, al

	mov ecx, 0x10-0x08
	call _dbg_pad_ff

	lea esi, [vbe_mode_buf]
	mov ecx, vbe_mode_buf.end - vbe_mode_buf.start
	call _dump

_halt:
	cli

	.loop:
	hlt
	jmp .loop

_dump:
	mov dx, 0xE9
	rep outsb
	ret

_dbg_pad_ff:
	mov al, 0xFF
	.loop:
	out 0xE9, al
	loop .loop
	ret

times 512-4*16-2-($-$$) nop

; Partition table
dq 0, 0
dq 0, 0
dq 0, 0
dq 0, 0

; Boot magic
db 0x55, 0xAA

section .bss

__bss.start:

ALIGN 4
vbe_info_buf:
	.start:
	.Sig        resb 4
	.Ver        resw 1
	.OEMPtr     resd 1
	.Cap        resd 1
	.ModesPtr   resd 1
	.MemSz64K   resw 1
	.OEMVer     resw 1
	.VendorPtr  resd 1
	.ProductPtr resd 1
	.ProdRevPtr resd 1
	.VBEAFVer   resw 1
	.AFModesPtr resd 1
	.Res28h     resb 216
	.Scratchpad resb 256
	.end:

ALIGN 4
vbe_mode_buf:
	.start:
	resb 256
	.end:

__bss.end:
