/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <gas_intrin.h>
#include <asm/gdt.h>

.text
.code64

FUNC_DECL_NOPRE(_early64_start)
	xorq %rax, %rax
	xorq %rbx, %rbx
	xorq %rcx, %rcx
	xorq %rdx, %rdx
	xorq %rdi, %rdi
	xorq %rsi, %rsi
	xorq %r8, %r8
	xorq %r9, %r9
	xorq %r10, %r10
	xorq %r11, %r11
	xorq %r12, %r12
	xorq %r13, %r13
	xorq %r14, %r14
	xorq %r15, %r15
	xorq %rbp, %rbp
	xorq %rsp, %rsp

	/* Setup kernel stack. */
	leaq (__kernel_stack_top), %rsp
	movq %rsp, %rbp

	/* Jump to kernel entry point.
	 *   rdi = kmicrofs ptr
	 */
	movq (__early_kmicrofs_ptr), %rdi
	callq _start

.Lloop:
	cli
	hlt
	jmp .Lloop
FUNC_END(_early64_start)

.bss

ALIGN(8)
__kernel_stack:
.skip 2 * 1024 * 1024 /* 2MB stack */
__kernel_stack_top:
