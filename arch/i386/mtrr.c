/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "asm/cpuid.h"
#include "asm/msr.h"
#include "common/kmalloc.h"
#include "common/string.h"
#include "common/printk.h"
#include "arch/interrupt.h"

#include "arch/mtrr.h"

#define CR0_NW          (1 << 29)
#define CR0_CD          (1 << 30)

#define CR4_PGE         (1 << 7)

#define IA32_MTRR_CAP       0xFE
#define IA32_MTRR_DEF_TYPE  0x2FF

#define IA32_MTRR_FIX64K_00000 0x250
#define IA32_MTRR_FIX16K_80000 0x258
#define IA32_MTRR_FIX16K_A0000 0x259
#define IA32_MTRR_FIX4K_C0000  0x268
#define IA32_MTRR_FIX4K_C8000  0x269
#define IA32_MTRR_FIX4K_D0000  0x26A
#define IA32_MTRR_FIX4K_D8000  0x26B
#define IA32_MTRR_FIX4K_E0000  0x26C
#define IA32_MTRR_FIX4K_E8000  0x26D
#define IA32_MTRR_FIX4K_F0000  0x26E
#define IA32_MTRR_FIX4K_F8000  0x26F

#define IA32_MTRR_PHYSBASE0 0x200
#define IA32_MTRR_PHYSMASK0 0x201

/* Number of variable range registers supported */
#define MTRR_CAP_VCNT_MASK 0xFF
/* Fixed range registers supported */
#define MTRR_CAP_FIX_MASK  0x100
/* Write combining type supported */
#define MTRR_CAP_WC_MASK   0x400
/* System management range register supported */
#define MTRR_CAP_SMRR_MASK 0x800

#define MTRR_PHYS_MASK   0xFFFFFF000

#define MTRR_FIX_ENABLE (1 << 10)
#define MTRR_ENABLE     (1 << 11)

struct mtrr_ctx_t {
	uint8_t   is_avail;
	uintptr_t phys_mask;
	uint8_t   vcnt;
	uint8_t   fix_avail;
	uint8_t   wc_avail;
	uint8_t   vrr_used[256];

	uint32_t  def_type;
	uintptr_t saved_cr4;
};
typedef struct mtrr_ctx_t mtrr_ctx_t;


static mtrr_ctx_t mtrr_ctx;

static void pre_mtrr_change(void)
{
	uintptr_t cr0;
	uintptr_t cr3;

	/* Disable interrupts */
	arch_disable_interrupt_push_state();

	/* Backup CR4 */
	__asm__ __volatile__("mov %%cr4, %0 \n" :"=r"(mtrr_ctx.saved_cr4));

	/* Disable global pages */
	__asm__ __volatile__("mov %0, %%cr4 \n" ::"r"(mtrr_ctx.saved_cr4 & (~CR4_PGE)));

	/* Disable caching */
	__asm__ __volatile__("mov %%cr0, %0 \n" :"=r"(cr0));
	__asm__ __volatile__("mov %0, %%cr0 \n" ::"r"((cr0 & (~CR0_NW)) | CR0_CD));

	/* Invalidate/flush caches */
	__asm__ __volatile__("wbinvd \n");

	/* Flush TLBs */
	__asm__ __volatile__("mov %%cr3, %0 \n" :"=r"(cr3));
	__asm__ __volatile__("mov %0, %%cr3 \n" ::"r"(cr3));

	/* Disable MTRRs */
	_wrmsr(IA32_MTRR_DEF_TYPE, mtrr_ctx.def_type & ~MTRR_ENABLE);
}

static void post_mtrr_change(void)
{
	uintptr_t cr0;
	uintptr_t cr3;

	/* Restore MTRRs enabled state */
	_wrmsr(IA32_MTRR_DEF_TYPE, mtrr_ctx.def_type);

	/* Invalidate/flush caches */
	__asm__ __volatile__("wbinvd \n");

	/* Flush TLBs */
	__asm__ __volatile__("mov %%cr3, %0 \n" :"=r"(cr3));
	__asm__ __volatile__("mov %0, %%cr3 \n" ::"r"(cr3));

	/* Enable caching */
	__asm__ __volatile__("mov %%cr0, %0 \n" :"=r"(cr0));
	__asm__ __volatile__("mov %0, %%cr0 \n" ::"r"(cr0 & (~CR0_CD)));

	/* Restore CR4 */
	__asm__ __volatile__("mov %0, %%cr4 \n" ::"r"(mtrr_ctx.saved_cr4));

	/* Restore interrupts state */
	arch_interrupt_pop_state();
}

static inline const char *mtrr_type_to_str(uint8_t type) {
	switch (type)
	{
		case MTRR_TYPE_UC: return "UC";
		case MTRR_TYPE_WC: return "WC";
		case MTRR_TYPE_WT: return "WT";
		case MTRR_TYPE_WP: return "WP";
		case MTRR_TYPE_WB: return "WB";
		default: return "UNK";
	}
}

int arch_mtrr_def_range(void *addr, uint64_t len, uint8_t type)
{
	printk("MTRR: arch_mtrr_def_range: semi-stub!");

	if(!mtrr_ctx.is_avail)
		return -1;

	if((uintptr_t)addr & (~MTRR_PHYS_MASK)
			|| len & (~MTRR_PHYS_MASK))
		return -1;

	switch(type) {
		case MTRR_TYPE_WC:
			if(!mtrr_ctx.wc_avail)
				return -1;
		case MTRR_TYPE_UC:
		case MTRR_TYPE_WT:
		case MTRR_TYPE_WP:
		case MTRR_TYPE_WB:
			break;
		default:
			return -1;
	}

	for(int i = 0; i < mtrr_ctx.vcnt; i++)
	{
		char *ptr;

		if (mtrr_ctx.vrr_used[i])
			continue;

		pre_mtrr_change();

		_wrmsr(IA32_MTRR_PHYSMASK0 + 2 * i, ((MAX_UINTPTR - len) & mtrr_ctx.phys_mask) | MTRR_ENABLE);
		_wrmsr(IA32_MTRR_PHYSBASE0 + 2 * i, (((uintptr_t)addr) & mtrr_ctx.phys_mask) | type);

		post_mtrr_change();

		mtrr_ctx.vrr_used[i] = 1;

		ptr = addr;
		printk("MTRR: vrr %d: %p-%p: %s", i, ptr, &ptr[len], mtrr_type_to_str(type));

		return 0;
	}

	return -1;
}

int arch_mtrr_setup(void)
{
	uint8_t max_phy_addr;
	uint16_t caps;

	mtrr_ctx.is_avail = _cpuid_mtrr_support();
	if(!mtrr_ctx.is_avail) {
		printk("MTRR: Memory type range registers not available.");
		return -1;
	}

	max_phy_addr = _cpuid_max_phy_addr();
	if(max_phy_addr >= sizeof(uintptr_t) * 8)
		mtrr_ctx.phys_mask = MAX_UINTPTR ^ 0xFFF;
	else
		mtrr_ctx.phys_mask = (MAX_UINTPTR >> ((sizeof(uintptr_t) * 8) - max_phy_addr)) ^ 0xFFF;

	caps = _rdmsr(IA32_MTRR_CAP) & 0xFFFF;
	mtrr_ctx.vcnt = caps & MTRR_CAP_VCNT_MASK;
	mtrr_ctx.fix_avail = (caps & MTRR_CAP_FIX_MASK) ? 1 : 0;
	mtrr_ctx.wc_avail = (caps & MTRR_CAP_WC_MASK) ? 1 : 0;

	printk("MTRR: Caps: %d vrrs%s%s",
		mtrr_ctx.vcnt,
		mtrr_ctx.fix_avail ? ", frrs" : "",
		mtrr_ctx.wc_avail ? ", wc" : "");

	/* MTRR enabled/uncacheable by default */
	mtrr_ctx.def_type = MTRR_ENABLE | MTRR_TYPE_UC;

	pre_mtrr_change();

	/* Clear all MTRRs */
	if(mtrr_ctx.fix_avail) {
		_wrmsr(IA32_MTRR_FIX64K_00000, 0);
		_wrmsr(IA32_MTRR_FIX16K_80000, 0);
		_wrmsr(IA32_MTRR_FIX16K_A0000, 0);
		_wrmsr(IA32_MTRR_FIX4K_C0000, 0);
		_wrmsr(IA32_MTRR_FIX4K_C8000, 0);
		_wrmsr(IA32_MTRR_FIX4K_D0000, 0);
		_wrmsr(IA32_MTRR_FIX4K_D8000, 0);
		_wrmsr(IA32_MTRR_FIX4K_E0000, 0);
		_wrmsr(IA32_MTRR_FIX4K_E8000, 0);
		_wrmsr(IA32_MTRR_FIX4K_F0000, 0);
		_wrmsr(IA32_MTRR_FIX4K_F8000, 0);
	}

	for(int i = 0; i < mtrr_ctx.vcnt; i++) {
		_wrmsr(IA32_MTRR_PHYSMASK0 + 2 * i, 0);
		_wrmsr(IA32_MTRR_PHYSBASE0 + 2 * i, 0);
	}

	post_mtrr_change();

	printk("MTRR: Made whole memory uncached by default.");

	return 0;
}
