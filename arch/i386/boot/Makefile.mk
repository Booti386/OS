SUBDIRS=
OPTSUBDIRS=

CUSTBINS= \
    boot.bin \
    kmicrofs.img \
    floppy.img

DEPS-kmicrofs.img= \
    $(TOP)/kernel \
    $(TOP)/kernel.syms

DEPS-floppy.img= \
    boot.bin \
    $(TOP)/kernel

kmicrofs.img:
	@echo " GEN\t\t$@"
	@$(TOP)/tools/mkkmicrofs $@ 0x1000 $(TOP)/kernel $(TOP)/kernel.syms

floppy.img:
	@echo " GEN\t\t$@"
	@cat boot.bin kmicrofs.img /dev/zero | dd of=$@ bs=512 count=2048 >/dev/null 2>&1
