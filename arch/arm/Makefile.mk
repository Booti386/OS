SUBDIRS=

BINS=builtin.o

LIBS-builtin.o=
OBJS-builtin.o= \
	plat.o

CFLAGS=$(CFLAGS_common)
CFLAGS+=-I. -Iinclude -I$(TOP) -I$(TOP)/include
LDFLAGS=$(LDFLAGS_reloc)
