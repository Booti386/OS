HOSTBINS-y := \
    bin2hex \
    mkkmicrofs

OBJS-bin2hex-y := bin2hex.host.c.o

OBJS-mkkmicrofs-y := mkkmicrofs.host.c.o
CFLAGS-mkkmicrofs.host.c.o-y := -I$(TOP)/include
