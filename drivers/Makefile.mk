SUBDIRS-y := bus fd fs input misc power video vm

BINS-y := builtin.o
LIBS-builtin.o-y := \
	bus/builtin.o \
	fd/builtin.o \
	fs/builtin.o \
	input/builtin.o \
	misc/builtin.o \
	power/builtin.o \
	video/builtin.o \
	vm/builtin.o
OBJS-builtin.o-y :=

CFLAGS-y += -I. -Iinclude
LDFLAGS-y := $(LDFLAGS_RELOC-y)
