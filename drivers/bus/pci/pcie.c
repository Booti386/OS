/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <os/types.h>
#include <common/printk.h>

#include "pci.h"
#include "pcie.h"

static int pcie_pci_init(struct pci_dev *dev)
{
	printk("PCIE: Device %x:%x.%x", dev->bus, dev->dev, dev->func);
	return 0;
}

static struct pci_driver pcie_pci_driver = {
	.matches = NULL,
	.init = pcie_pci_init
};

int pcie_register_driver(struct pcie_driver *driver)
{
	(void)driver;
	return 0;
}

int pcie_setup(void)
{
	pci_register_driver(&pcie_pci_driver);

	return 0;
}

