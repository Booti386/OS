/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "arch/interrupt.h"
#include "arch/pic8259a.h"
#include "arch/apic.h"
#include "arch/mmu.h"
#include "arch/mtrr.h"
#include "arch/e820.h"
#include "arch/vga.h"

int arch_setup(void)
{
	arch_paging_setup(NULL);
	arch_vga_setup();

	//arch_memory_map_setup();
	arch_interrupt_setup();
	arch_pic8259a_setup();
	arch_apic_setup();

	arch_mtrr_setup();

	/* Preserve IVT, BDA, EBDA, BIOS ROM, Video ROM, ... */
	arch_memory_map_update((void *)0x9FC00, 0x60400, MEMORY_TYPE_RESERVED);

	return 0;
}

void arch_halt(void)
{
	__asm__ __volatile__ (" \
		.Lhalt_loop: \n\
			cli   \n\
			hlt   \n\
			jmp .Lhalt_loop \n\
	");
}
