/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "asm/io.h"
#include "common/trace.h"
#include "arch/arch.h"
#include "arch/pic8259a.h"

#define IRQ_BASE 0x20

#define INT_HANDLER(x) wrap_int_ ## x
#define INT_HANDLER_STR(x) "wrap_int_" #x

#define INT_WRAPPER(x, func) \
	void INT_HANDLER(x)(void); \
	__asm__( \
		".func " INT_HANDLER_STR(x) ", " INT_HANDLER_STR(x) "\n" \
		INT_HANDLER_STR(x) ": \n" \
		"pushal \n" \
		"pushfl \n" \
		"movl $" #x ", %edi \n"     /* edi = num */ \
		"leal 32+4(%esp), %esi \n"  /* esi = esp */ \
		"subl $8, %esp \n" \
		"movl %edi, (%esp) \n" \
		"movl %esi, 4(%esp) \n" \
		"calll " #func "\n" \
		"addl $8, %esp \n" \
		"popfl \n" \
		"popal \n" \
		"iretl \n" \
		".endfunc \n" \
	);

#define INT_WRAPPER_ERRCODE(x, func) \
	void INT_HANDLER(x)(void); \
	__asm__( \
		".func " INT_HANDLER_STR(x) ", " INT_HANDLER_STR(x) "\n" \
		INT_HANDLER_STR(x) ": \n" \
		"xchgl (%esp), %edx \n"       /* edx = errcode */ \
		"pushal \n" \
		"pushfl \n" \
		"movl $" #x ", %edi \n"       /* edi = num */ \
		"leal 4+32+4(%esp), %esi \n"  /* esi = esp */ \
		"subl $12, %esp \n" \
		"movl %edi, (%esp) \n" \
		"movl %esi, 4(%esp) \n" \
		"movl %edx, 8(%esp) \n" \
		"calll " #func "\n" \
		"addl $12, %esp \n" \
		"popfl \n" \
		"popal \n" \
		"popl %edx \n" \
		"iretl \n" \
		".endfunc \n" \
	);

static void default_exp_handler(int num, uint32_t *esp)
{
	printk("EXCP 0x%x: cs:eip=0x%x:%p, eflags=%p.", num, esp[1], esp[0], esp[2]);
	print_backtrace(&esp[3]);
	arch_halt();
}

static void default_exp_handler_errcode(int num, uint32_t *esp, int errcode)
{
	printk("EXCP 0x%x: cs:eip=0x%x:%p, eflags=%p, errcode=0x%x.", num, esp[1], esp[0], esp[2], errcode);
	print_backtrace(&esp[3]);
	arch_halt();
}

static void default_pgfault_handler_errcode(int num, uint32_t *esp, int errcode)
{
	void *addr;

	__asm__ __volatile__("movl %%cr2, %0" : "=r"(addr));
	printk("EXCP PAGE FAULT(0x%x): cs:eip=0x%x:%p, eflags=%p, addr=%p, errcode=0x%x.", num, esp[1], esp[0], esp[2], addr, errcode);
	print_backtrace(&esp[3]);
	arch_halt();
}

static void irq_handler(int num, void *esp)
{
	num -= IRQ_BASE;
	printk("IRQ %d.", num);

	irq_run_handlers(num);

	/* Before returning from an IRQ, send EOI to the slave PIC if needed, then to the master PIC */
	if(num >= 8)
		arch_pic8259a_slave_eoi();
	arch_pic8259a_master_eoi();
}

static void master_spurious_irq_handler(int num, void *esp)
{
	num -= IRQ_BASE;
	printk("8259a (master): Spurious IRQ %d.", num);

	/* Do not send any EOI */
}

static void slave_spurious_irq_handler(int num, void *esp)
{
	num -= IRQ_BASE;
	printk("8259a (slave): Spurious IRQ %d.", num);

	/* Send an EOI only to the master PIC */
	arch_pic8259a_master_eoi();
}

static void syscall_handler(int num, uint32_t *esp)
{
	printk("syscall");
	print_backtrace(&esp[3]);
}

/* Hack to disable GCC defined but not used warning */
void *arch_interrupt_handlers_list[] = {
	default_exp_handler,
	default_exp_handler_errcode,
	default_pgfault_handler_errcode,
	irq_handler,
	master_spurious_irq_handler,
	slave_spurious_irq_handler,
	syscall_handler
};

INT_WRAPPER(0x0, default_exp_handler)
INT_WRAPPER(0x1, default_exp_handler)
INT_WRAPPER(0x2, default_exp_handler)
INT_WRAPPER(0x3, default_exp_handler)
INT_WRAPPER(0x4, default_exp_handler)
INT_WRAPPER(0x5, default_exp_handler)
INT_WRAPPER(0x6, default_exp_handler)
INT_WRAPPER(0x7, default_exp_handler)
INT_WRAPPER_ERRCODE(0x8, default_exp_handler_errcode)
INT_WRAPPER(0x9, default_exp_handler)
INT_WRAPPER_ERRCODE(0xA, default_exp_handler_errcode)
INT_WRAPPER_ERRCODE(0xB, default_exp_handler_errcode)
INT_WRAPPER_ERRCODE(0xC, default_exp_handler_errcode)
INT_WRAPPER_ERRCODE(0xD, default_exp_handler_errcode)
INT_WRAPPER_ERRCODE(0xE, default_pgfault_handler_errcode)
INT_WRAPPER(0xF, default_exp_handler)
INT_WRAPPER(0x10, default_exp_handler)
INT_WRAPPER_ERRCODE(0x11, default_exp_handler_errcode)
INT_WRAPPER(0x12, default_exp_handler)
INT_WRAPPER(0x13, default_exp_handler)
INT_WRAPPER(0x14, default_exp_handler)
INT_WRAPPER(0x15, default_exp_handler)
INT_WRAPPER(0x16, default_exp_handler)
INT_WRAPPER(0x17, default_exp_handler)
INT_WRAPPER(0x18, default_exp_handler)
INT_WRAPPER(0x19, default_exp_handler)
INT_WRAPPER(0x1A, default_exp_handler)
INT_WRAPPER(0x1B, default_exp_handler)
INT_WRAPPER(0x1C, default_exp_handler)
INT_WRAPPER(0x1D, default_exp_handler)
INT_WRAPPER(0x1E, default_exp_handler)
INT_WRAPPER(0x1F, default_exp_handler)

INT_WRAPPER(0x20, irq_handler)
INT_WRAPPER(0x21, irq_handler)
INT_WRAPPER(0x22, irq_handler)
INT_WRAPPER(0x23, irq_handler)
INT_WRAPPER(0x24, irq_handler)
INT_WRAPPER(0x25, irq_handler)
INT_WRAPPER(0x26, irq_handler)
INT_WRAPPER(0x27, master_spurious_irq_handler)
INT_WRAPPER(0x28, irq_handler)
INT_WRAPPER(0x29, irq_handler)
INT_WRAPPER(0x2A, irq_handler)
INT_WRAPPER(0x2B, irq_handler)
INT_WRAPPER(0x2C, irq_handler)
INT_WRAPPER(0x2D, irq_handler)
INT_WRAPPER(0x2E, irq_handler)
INT_WRAPPER(0x2F, slave_spurious_irq_handler)

/* 'S' */
INT_WRAPPER(0x53, syscall_handler)

#undef INT_WRAPPER_ERRCODE
#undef INT_WRAPPER
#undef INT_HANDLER_STR

static void (*int_array[256])(void) = {
/* CPU exceptions */
	INT_HANDLER(0x0),
	INT_HANDLER(0x1),
	INT_HANDLER(0x2),
	INT_HANDLER(0x3),
	INT_HANDLER(0x4),
	INT_HANDLER(0x5),
	INT_HANDLER(0x6),
	INT_HANDLER(0x7),
	INT_HANDLER(0x8),
	INT_HANDLER(0x9),
	INT_HANDLER(0xA),
	INT_HANDLER(0xB),
	INT_HANDLER(0xC),
	INT_HANDLER(0xD),
	INT_HANDLER(0xE),
	INT_HANDLER(0xF),
	INT_HANDLER(0x10),
	INT_HANDLER(0x11),
	INT_HANDLER(0x12),
	INT_HANDLER(0x13),
	INT_HANDLER(0x14),
	INT_HANDLER(0x15),
	INT_HANDLER(0x16),
	INT_HANDLER(0x17),
	INT_HANDLER(0x18),
	INT_HANDLER(0x19),
	INT_HANDLER(0x1A),
	INT_HANDLER(0x1B),
	INT_HANDLER(0x1C),
	INT_HANDLER(0x1D),
	INT_HANDLER(0x1E),
	INT_HANDLER(0x1F),

/* IRQs */
	INT_HANDLER(0x20),
	INT_HANDLER(0x21),
	INT_HANDLER(0x22),
	INT_HANDLER(0x23),
	INT_HANDLER(0x24),
	INT_HANDLER(0x25),
	INT_HANDLER(0x26),
	INT_HANDLER(0x27),
	INT_HANDLER(0x28),
	INT_HANDLER(0x29),
	INT_HANDLER(0x2A),
	INT_HANDLER(0x2B),
	INT_HANDLER(0x2C),
	INT_HANDLER(0x2D),
	INT_HANDLER(0x2E),
	INT_HANDLER(0x2F),

	[0x53] = INT_HANDLER(0x53),
};

#undef INT_HANDLER
