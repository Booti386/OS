/*
 * Copyright (C) 2015-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef MMU_H
#define MMU_H 1

#include <os/types.h>

extern void *arch_get_phys_addr(void *virt);
extern void *arch_map_phys_pages(void *phys, uint64_t n);
extern void *arch_map_phys(void *phys, uint64_t len);
extern void *arch_map_pages(void *virt, uint64_t n);
extern void *arch_map(void *virt, uint64_t len);
extern int arch_unmap(void *virt, uint64_t len);
extern void *arch_alloc_pages(uint64_t n);
extern void *arch_alloc(uint64_t len);
extern int arch_paging_setup(void *mem_map);

#endif /* MMU_H */
