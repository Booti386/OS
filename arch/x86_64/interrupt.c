/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <macros.h>
#include <arch/interrupt.h>
#include <asm/gdt.h>
#include <common/kmalloc.h>
#include <common/printk.h>
#include <os/types.h>

#include "interrupt_table.h"

#define TASK_GATE 0x5
#define INT_GATE  0xE
#define TRAP_GATE 0xF

struct __attribute__((packed)) idtr_t
{
	uint16_t limit;
	void *addr;
};
typedef struct idtr_t idtr_t;

struct __attribute__((packed)) idtdesc_t {
	uint16_t off_low;
	uint16_t seg_sel;
	uint8_t  res0;
	uint8_t  type     : 4;
	uint8_t  stor_seg : 1;
	uint8_t  dpl      : 2;
	uint8_t  pres     : 1;
	uint16_t off_mid;
	uint32_t off_hig;
	uint32_t res1;
};
typedef struct idtdesc_t idtdesc_t;


struct interrupt_state_entry_t
{
	uint8_t enabled;

	struct interrupt_state_entry_t *prev;
};
typedef struct interrupt_state_entry_t interrupt_state_entry_t;

struct interrupt_ctx_t
{
	uint8_t enabled;
	interrupt_state_entry_t *states;

	idtr_t idtr __attribute__((aligned(8)));
	idtdesc_t idt[INTERRUPT_TABLE_HANDLER_COUNT] __attribute__((aligned(sizeof(idtdesc_t))));
	arch_interrupt_irq_handler_t irq_handlers[INTERRUPT_TABLE_IRQ_COUNT];
};
typedef struct interrupt_ctx_t interrupt_ctx_t;


static interrupt_ctx_t interrupt_ctx;

void arch_disable_interrupt(void)
{
	interrupt_ctx.enabled = 0;
	__asm__ __volatile__("cli \n");
}

void arch_enable_interrupt(void)
{
	__asm__ __volatile__("sti \n");
	interrupt_ctx.enabled = 1;
}

void arch_disable_interrupt_push_state(void)
{
	interrupt_state_entry_t *entry;

	entry = kmalloc(sizeof(*entry));
	entry->prev = interrupt_ctx.states;
	interrupt_ctx.states = entry;

	interrupt_ctx.states->enabled = interrupt_ctx.enabled;

	arch_disable_interrupt();
}

void arch_enable_interrupt_push_state(void)
{
	interrupt_state_entry_t *entry;

	entry = kmalloc(sizeof(*entry));
	entry->prev = interrupt_ctx.states;
	interrupt_ctx.states = entry;

	interrupt_ctx.states->enabled = interrupt_ctx.enabled;

	arch_enable_interrupt();
}

void arch_interrupt_pop_state(void)
{
	interrupt_state_entry_t *entry;

	if(!interrupt_ctx.states)
		return;

	if(interrupt_ctx.states->enabled)
		arch_enable_interrupt();
	else
		arch_disable_interrupt();

	interrupt_ctx.enabled = interrupt_ctx.states->enabled;

	entry = interrupt_ctx.states->prev;
	kfree(interrupt_ctx.states);
	interrupt_ctx.states = entry;
}

void arch_irq_add_handler(unsigned int num, arch_interrupt_irq_handler_t handler)
{
	if (num >= INTERRUPT_TABLE_IRQ_COUNT)
		return;

	interrupt_ctx.irq_handlers[num] = handler;
}

void arch_interrupt_irq_run_handlers(unsigned int num)
{
	arch_interrupt_irq_handler_t handler;

	if (num >= INTERRUPT_TABLE_IRQ_COUNT)
		return;

	handler = interrupt_ctx.irq_handlers[num];
	if (!handler)
		return;

	handler();
}

int arch_interrupt_setup(void)
{
	const arch_interrupt_handler_t *handler_table;

	handler_table = arch_interrupt_get_handler_table();

	interrupt_ctx.states = NULL;

	for (size_t i = 0; i < ARRAY_SIZE(interrupt_ctx.idt); i++)
	{
		uint64_t handler_addr = (uint64_t)handler_table[i];

		if (!handler_addr)
			continue;

		interrupt_ctx.idt[i].res0 = 0;
		interrupt_ctx.idt[i].res1 = 0;

		interrupt_ctx.idt[i].off_low = handler_addr & 0xFFFF;
		interrupt_ctx.idt[i].seg_sel = GDT_CODESEG64 & 0xFFFF;
		interrupt_ctx.idt[i].off_mid = (handler_addr >> 16) & 0xFFFF;
		interrupt_ctx.idt[i].off_hig = handler_addr >> 32;

		interrupt_ctx.idt[i].dpl = 0;
		interrupt_ctx.idt[i].stor_seg = 0;
		interrupt_ctx.idt[i].type = INT_GATE;
		interrupt_ctx.idt[i].pres = 1;
	}

	interrupt_ctx.idtr.limit = sizeof(interrupt_ctx.idt) - 1;
	interrupt_ctx.idtr.addr = &interrupt_ctx.idt;

	for (size_t i = 0; i < ARRAY_SIZE(interrupt_ctx.irq_handlers); i++)
		interrupt_ctx.irq_handlers[i] = NULL;

	__asm__ __volatile__("lidtq %0" :: "m"(interrupt_ctx.idtr));
	arch_enable_interrupt();

	return 0;
}
