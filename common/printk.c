/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "stdarg.h"
#include "common/lib.h"
#include "common/string.h"
#include "asm/io.h"

#define MAX_LINE_LEN 1024

static char log_buffer[1024 * 1024];
static uint32_t log_buffer_index = 0;

static void (*display_func)(const char *) = NULL;

void printk_register_func(void (*func)(const char*))
{
	display_func = func;
}

static void flush_disp_buf(const char *buf) {
	if(display_func)
		display_func(buf);

	if(log_buffer_index + strlen(buf) >= sizeof(log_buffer)) {
		memset(log_buffer, '\0', sizeof(log_buffer));
		log_buffer_index = 0;
	}
	memcpy(&log_buffer[log_buffer_index], buf, strlen(buf));
	log_buffer_index += strlen(buf);
}

void printk(const char *str, ...)
{
	va_list args;
	int cont = 1;

	char *arg_str;
	size_t len;

	char buf[MAX_LINE_LEN + 1];

	char itostr_buf[ITOSTR_BUF_MAXLEN];
	char utostr_buf[UTOSTR_BUF_MAXLEN];
	char x64tostr_buf[X64TOSTR_BUF_MAXLEN];
	char x32tostr_buf[X32TOSTR_BUF_MAXLEN];

	buf[MAX_LINE_LEN] = '\0';

	va_start(args, str);
	for(size_t i=0, j=0; cont && j<MAX_LINE_LEN; i++)
	{
		switch(str[i])
		{
			case '\0':
				cont = 0;
				buf[j] = '\0';
				break;

			case '%':
				switch(str[++i])
				{
					case '\0':
						cont = 0;
						break;

					case '%':
						buf[j++] = '%';
						break;

					case 'c':
						buf[j++] = va_arg(args, int);
						break;

					case 'd':
						itostr(itostr_buf, va_arg(args, int));
						len = strlen(itostr_buf);
						len = (len <= MAX_LINE_LEN - j) ? len : MAX_LINE_LEN - j;
						memcpy(&buf[j], itostr_buf, len);
						j += len;
						break;

					case 'u':
						utostr(utostr_buf, va_arg(args, unsigned int));
						len = strlen(utostr_buf);
						len = (len <= MAX_LINE_LEN - j) ? len : MAX_LINE_LEN - j;
						memcpy(&buf[j], utostr_buf, len);
						j += len;
						break;

					case 'p':
						memcpy(&buf[j], "0x", 2);
						j += 2;

						if(sizeof(uintptr_t) == sizeof(uint64_t)) {
							x64tostr(x64tostr_buf, va_arg(args, uint64_t), 1);
							len = strlen(x64tostr_buf);
							len = (len <= MAX_LINE_LEN - j) ? len : MAX_LINE_LEN - j;
							memcpy(&buf[j], x64tostr_buf, len);
							j += len;
						} else {
							x32tostr(x32tostr_buf, va_arg(args, uint32_t), 1);
							len = strlen(x32tostr_buf);
							len = (len <= MAX_LINE_LEN - j) ? len : MAX_LINE_LEN - j;
							memcpy(&buf[j], x32tostr_buf, len);
							j += len;
						}
						break;

					case 'x':
						x32tostr(x32tostr_buf, va_arg(args, uint32_t), 0);
						len = strlen(x32tostr_buf);
						len = (len <= MAX_LINE_LEN - j) ? len : MAX_LINE_LEN - j;
						memcpy(&buf[j], x32tostr_buf, len);
						j += len;
						break;

					case 'l':
						switch(str[++i])
						{
							case '\0':
								cont = 0;
								break;

							case 'x':
								x64tostr(x64tostr_buf, va_arg(args, uint64_t), 0);
								len = strlen(x64tostr_buf);
								len = (len <= MAX_LINE_LEN - j) ? len : MAX_LINE_LEN - j;
								memcpy(&buf[j], x64tostr_buf, len);
								j += len;
								break;

						}
						break;

					case 's':
					{
						arg_str = va_arg(args, char *);
						len = strlen(arg_str);
						len = (len <= MAX_LINE_LEN - j) ? len : MAX_LINE_LEN - j;
						memcpy(&buf[j], arg_str, len);
						j += len;
						break;
					}
				}
				break;

			default:
				buf[j++] = str[i];
		}
	}

	flush_disp_buf(buf);

	va_end(args);
}
