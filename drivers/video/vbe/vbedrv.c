/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <common/printk.h>
#include <common/string.h>
#include <os/types.h>
#include <fd/fd.h>

#include "info.h"
#include "modes.h"
#include "vbedrv.h"

#include "drivers/vm/vm86/vm86.h"

#include "res/logo-poussin.h"

static struct vbe_ctx vbe_ctx;

static int vbe_ctrlhandler(char *rq, size_t len)
{
	(void)rq;
	(void)len;
	return 0;
}

static int vbe_handler(char *rq, size_t len)
{
	(void)rq;
	(void)len;
	return 0;
}

static inline uint32_t mask_comp(uint32_t comp, uint8_t mask_size, uint8_t field_pos) {
	comp <<= (32 - mask_size);
	return comp >> (32 - mask_size - field_pos);
}

int vbe_setup(void)
{
	struct vbe_ctx *ctx = &vbe_ctx;
	int vbectrl, vbe;
	int ret;
	char instrs[] =
		"\xCD\x10"     /* INT 10h */
		VM86_COP_QUIT; /* VM86_QUIT */

	ctx->vm = NULL;
	vm86_create(&ctx->vm, 1);
	vm86_set_trace(ctx->vm, 0);
	vm86_map_bios_bda(ctx->vm);
	vm86_map_bios_ebda(ctx->vm);
	vm86_map_upper_mem(ctx->vm);
	vm86_write_segment(ctx->vm, VOIDPTR(0x7C00), instrs, sizeof(instrs));

	ret = vbe_fetch_info(ctx);
	if(ret < 0)
		return -1;

	//vm86_set_trace(ctx->vm, 1);
	ret = vbe_fetch_modes(ctx);
	if(ret < 0)
		return -1;

	vbectrl = fd_create("\\dev/vbectl", FD_TYPE_DRVIFACE, 0);
	vbe = fd_create("\\dev/vbe", FD_TYPE_DRVIFACE, 0);

	fd_drviface_sethandler(vbectrl, vbe_ctrlhandler);
	fd_drviface_sethandler(vbe, vbe_handler);

	uint16_t sel_mode = 0xFFFF;
	uint32_t sel_mode_res = 0;

	for (uint16_t i = 0; i < ctx->nb_modes; i++)
	{
		uint16_t mode = ctx->mode_nums[i];
		struct vbe_mode_info *info = &ctx->mode_infos[i];

		if (!info->mode_attrs.bits.hardware_support
				|| !info->mode_attrs.bits.color
				|| !info->mode_attrs.bits.graphics)
			continue;

		if (info->bpp != 32
				|| !info->fb_phys_addr)
			continue;

		uint32_t mode_res = info->width * info->height;
		if (mode_res < sel_mode_res)
			continue;

		sel_mode = mode;
		sel_mode_res = mode_res;
	}

	if (sel_mode == 0xFFFF)
	{
		printk("VBE: No suitable mode found (32 bits, HWSupport, Color, Graphics).");
		return -1;
	}

	printk("VBE: Select mode 0x%x.", sel_mode);

	ret = vbe_set_mode(ctx, sel_mode);
	if(ret < 0)
		return -1;

	memset(ctx->fb, 128, ctx->fb_len);

	uint8_t *ptr = ctx->fb;

	for (uint32_t i = 0; i < logo_poussin.height; i++)
	{
		uint8_t *p = ptr;

		for (uint32_t j = 0; j < logo_poussin.width; j++)
		{
			uint32_t idx = (i * logo_poussin.width + j) * logo_poussin.bytes_per_pixel;
			uint32_t *p1 = (uint32_t *)p;
			uint8_t r, g, b;
			uint32_t pix;
			uint32_t mask;

			r = logo_poussin.pixel_data[idx + 0];
			g = logo_poussin.pixel_data[idx + 1];
			b = logo_poussin.pixel_data[idx + 2];

			pix = mask_comp(r, ctx->cur_mode->red_mask_size, ctx->cur_mode->red_field_pos)
					| mask_comp(g, ctx->cur_mode->green_mask_size, ctx->cur_mode->green_field_pos)
					| mask_comp(b, ctx->cur_mode->blue_mask_size, ctx->cur_mode->blue_field_pos);

			mask = 0xFFFFFFFFu >> (32 - ctx->cur_mode->bpp);
			*p1 = (pix & mask) | (*p1 & ~mask);

			p += ctx->cur_mode->bpp / 8;
		}

		ptr += ctx->cur_mode->width * ctx->cur_mode->bpp / 8;
	}

	return 0;
}
