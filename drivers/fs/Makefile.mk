SUBDIRS-y :=

BINS-y := builtin.o
LIBS-builtin.o-y :=
OBJS-builtin.o-y := \
	corefs.c.o \
	kmicrofs.c.o

CFLAGS-y += -I$(TOP)/drivers
LDFLAGS-y += $(LDFLAGS_RELOC-y)
