SUBDIRS=lib

BINS=builtin.o \
	early0.o

LIBS-builtin.o= \
	lib/builtin.o \
	vm86/builtin.o

OBJS-builtin.o= \
	arch.o \
	vga.o \
	e820.o \
	mmu.o \
	mtrr.o \
	interrupt.o \
	pic8259a.o \
	apic.o \
	sched.o

LIBS-early0.o=
OBJS-early0.o= \
	early32.o

CFLAGS=$(CFLAGS_common)
CFLAGS+=-I. -Iinclude -I$(TOP) -I$(TOP)/include
LDFLAGS=$(LDFLAGS_reloc)
