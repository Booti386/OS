/*
 * Copyright (C) 2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef VM86_H
#define VM86_H

#include <os/types.h>

#define VM86_MEM_SIZE (1024 * 1024)

// Must be >= 8 bytes
#define VM86_SEG_SIZE 0x80
#define VM86_SEG_COUNT (VM86_MEM_SIZE / VM86_SEG_SIZE)
#define VM86_SEG_MASK (~(VM86_SEG_SIZE - 1))

#define VM86_REAL_SEG_OFF(seg, off) \
	((((seg) & 0xFFFF) << 16) | ((off) & 0xFFFF))

#define VM86_REAL2PHYS(addr) \
	((((addr) >> 12) & 0xFFFF0) + ((addr) & 0xFFFF))
#define VM86_REAL2PHYS_SEG_OFF(seg, off) \
	((((seg) & 0xFFFF) << 4) + ((off) & 0xFFFF))

#define VM86_PHYS2REALSEG(addr) \
	(((addr) >> 4) & 0xF000)
#define VM86_PHYS2REALREG(addr) \
	((addr) & 0xFFFF)

#define VM86_PHYS2REAL(addr) \
	((VM86_REALSEG_FROM_PHYS_ADDR(addr) << 16) | VM86_REALREG_FROM_PHYS_ADDR(addr))

/* Control ops */
#define VM86_COP_QUIT "\x0F\x0BVM86QUIT" /* UD2 VM86QUIT */

#define VM86_OP_MOD_MASK (3 << 6)

enum vm86_op_mod {
	VM86_OP_MOD_NODISP_OR_PABS = 0 << 6,
	VM86_OP_MOD_REL8 = 1 << 6,
	VM86_OP_MOD_ABS = 2 << 6,
	VM86_OP_MOD_REG = 3 << 6
};

#define VM86_OP_REG_MASK (7 << 3)

enum vm86_op_reg8 {
	VM86_OP_REG_AL = 0 << 3,
	VM86_OP_REG_CL = 1 << 3,
	VM86_OP_REG_DL = 2 << 3,
	VM86_OP_REG_BL = 3 << 3,
	VM86_OP_REG_AH = 4 << 3,
	VM86_OP_REG_CH = 5 << 3,
	VM86_OP_REG_DH = 6 << 3,
	VM86_OP_REG_BH = 7 << 3
};

enum vm86_op_reg16 {
	VM86_OP_REG_AX = 0 << 3,
	VM86_OP_REG_CX = 1 << 3,
	VM86_OP_REG_DX = 2 << 3,
	VM86_OP_REG_BX = 3 << 3,
	VM86_OP_REG_SP = 4 << 3,
	VM86_OP_REG_BP = 5 << 3,
	VM86_OP_REG_SI = 6 << 3,
	VM86_OP_REG_DI = 7 << 3
};

enum vm86_op_reg32 {
	VM86_OP_REG_EAX = 0 << 3,
	VM86_OP_REG_ECX = 1 << 3,
	VM86_OP_REG_EDX = 2 << 3,
	VM86_OP_REG_EBX = 3 << 3,
	VM86_OP_REG_ESP = 4 << 3,
	VM86_OP_REG_EBP = 5 << 3,
	VM86_OP_REG_ESI = 6 << 3,
	VM86_OP_REG_EDI = 7 << 3
};

enum vm86_op_reg_seg {
	VM86_OP_REG_ES = 0 << 3,
	VM86_OP_REG_CS = 1 << 3,
	VM86_OP_REG_SS = 2 << 3,
	VM86_OP_REG_DS = 3 << 3,
	VM86_OP_REG_FS = 4 << 3,
	VM86_OP_REG_GS = 5 << 3,
	VM86_OP_REG_RES0 = 6 << 3,
	VM86_OP_REG_RES1 = 7 << 3
};

enum vm86_op_reg_digit {
	VM86_OP_REG_0 = 0 << 3,
	VM86_OP_REG_1 = 1 << 3,
	VM86_OP_REG_2 = 2 << 3,
	VM86_OP_REG_3 = 3 << 3,
	VM86_OP_REG_4 = 4 << 3,
	VM86_OP_REG_5 = 5 << 3,
	VM86_OP_REG_6 = 6 << 3,
	VM86_OP_REG_7 = 7 << 3
};

#define VM86_OP_RM_MASK 7

enum vm86_op_rm16 {
	VM86_OP_RM_PBX_SI = 0,
	VM86_OP_RM_PBX_DI = 1,
	VM86_OP_RM_PBP_SI = 2,
	VM86_OP_RM_PBP_DI = 3,
	VM86_OP_RM_PSI = 4,
	VM86_OP_RM_PDI = 5,
/* If MOD == VM86_OP_MOD_NODISP_OR_PABS, arg is [imm16] instead of [bp] */
	VM86_OP_RM_PBP_OR_PABS16 = 6,
	VM86_OP_RM_PBX = 7
};

enum vm86_op_rm32 {
	VM86_OP_RM_PEAX = 0,
	VM86_OP_RM_PECX = 1,
	VM86_OP_RM_PEDX = 2,
	VM86_OP_RM_PEBX = 3,
	VM86_OP_RM_PSIB = 4,
/* If MOD == VM86_OP_MOD_NODISP_OR_PABS, arg is [imm32] instead of [ebp] */
	VM86_OP_RM_PEBP_OR_PABS32 = 5,
	VM86_OP_RM_PESI = 6,
	VM86_OP_RM_PEDI = 7
};

/* If MOD == VM86_OP_MOD_REG */
enum vm86_op_rm_reg8 {
	VM86_OP_RM_AL = 0,
	VM86_OP_RM_CL = 1,
	VM86_OP_RM_DL = 2,
	VM86_OP_RM_BL = 3,
	VM86_OP_RM_AH = 4,
	VM86_OP_RM_CH = 5,
	VM86_OP_RM_DH = 6,
	VM86_OP_RM_BH = 7
};

enum vm86_op_rm_reg16 {
	 VM86_OP_RM_AX = 0,
	 VM86_OP_RM_CX = 1,
	 VM86_OP_RM_DX = 2,
	 VM86_OP_RM_BX = 3,
	 VM86_OP_RM_SP = 4,
	 VM86_OP_RM_BP = 5,
	 VM86_OP_RM_SI = 6,
	 VM86_OP_RM_DI = 7
};

enum vm86_op_rm_reg32 {
	 VM86_OP_RM_EAX = 0,
	 VM86_OP_RM_ECX = 1,
	 VM86_OP_RM_EDX = 2,
	 VM86_OP_RM_EBX = 3,
	 VM86_OP_RM_ESP = 4,
	 VM86_OP_RM_EBP = 5,
	 VM86_OP_RM_ESI = 6,
	 VM86_OP_RM_EDI = 7
};

#define VM86_OP_SIB_BASE_MASK 7

enum vm86_op_sib_base {
	 VM86_OP_SIB_BASE_EAX = 0,
	 VM86_OP_SIB_BASE_ECX = 1,
	 VM86_OP_SIB_BASE_EDX = 2,
	 VM86_OP_SIB_BASE_EBX = 3,
	 VM86_OP_SIB_BASE_ESP = 4,
/* If MOD == VM86_OP_MOD_NODISP_OR_PABS, arg is [imm32] instead of [ebp] */
	 VM86_OP_SIB_BASE_EBP_OR_ABS32 = 5,
	 VM86_OP_SIB_BASE_ESI = 6,
	 VM86_OP_SIB_BASE_EDI = 7
};

#define VM86_OP_SIB_SCALE_SHIFT 6
#define VM86_OP_SIB_SCALE_MASK (3 << VM86_OP_SIB_SCALE_SHIFT)

#define VM86_OP_SIB_IDX_MASK (7 << 3)

enum vm86_op_sib_idx {
	VM86_OP_SIB_IDX_EAX = 0 << 3,
	VM86_OP_SIB_IDX_ECX = 1 << 3,
	VM86_OP_SIB_IDX_EDX = 2 << 3,
	VM86_OP_SIB_IDX_EBX = 3 << 3,
	VM86_OP_SIB_IDX_NONE = 4 << 3,
	VM86_OP_SIB_IDX_EBP = 5 << 3,
	VM86_OP_SIB_IDX_ESI = 6 << 3,
	VM86_OP_SIB_IDX_EDI = 7 << 3
};

enum vm86_prot_mask {
	VM86_PROT_READ  = 1 << 0,
	VM86_PROT_WRITE = 1 << 1,
	VM86_PROT_EXEC  = 1 << 2 /* Ignored */
};

enum vm86_reg_mask {
	VM86_REG_EAX = 1 << 0,
	VM86_REG_EBX = 1 << 1,
	VM86_REG_ECX = 1 << 2,
	VM86_REG_EDX = 1 << 3,
	VM86_REG_EDI = 1 << 4,
	VM86_REG_ESI = 1 << 5,
	VM86_REG_EBP = 1 << 6,
	VM86_REG_ESP = 1 << 7,
	VM86_REG_EIP = 1 << 8,
	VM86_REG_EFLAGS = 1 << 9,
	VM86_REG_CS = 1 << 10,
	VM86_REG_DS = 1 << 11,
	VM86_REG_ES = 1 << 12,
	VM86_REG_FS = 1 << 13,
	VM86_REG_GS = 1 << 14,
	VM86_REG_SS = 1 << 15
};

enum vm86_eflags_shift {
	VM86_EFLAGS_CF_SHIFT   = 0,
	VM86_EFLAGS_PF_SHIFT   = 2,
	VM86_EFLAGS_AF_SHIFT   = 4,
	VM86_EFLAGS_ZF_SHIFT   = 6,
	VM86_EFLAGS_SF_SHIFT   = 7,
	VM86_EFLAGS_TF_SHIFT   = 8,
	VM86_EFLAGS_IF_SHIFT   = 9,
	VM86_EFLAGS_DF_SHIFT   = 10,
	VM86_EFLAGS_OF_SHIFT   = 11,
	VM86_EFLAGS_IOPL_SHIFT = 12,
	VM86_EFLAGS_NT_SHIFT   = 14,
	VM86_EFLAGS_RF_SHIFT   = 16,
	VM86_EFLAGS_VM_SHIFT   = 17,
	VM86_EFLAGS_AC_SHIFT   = 18,
	VM86_EFLAGS_VIF_SHIFT  = 19,
	VM86_EFLAGS_VIP_SHIFT  = 20,
	VM86_EFLAGS_ID_SHIFT   = 21
};

enum vm86_eflags_mask {
	VM86_EFLAGS_CF   = 1 << VM86_EFLAGS_CF_SHIFT,
	VM86_EFLAGS_PF   = 1 << VM86_EFLAGS_PF_SHIFT,
	VM86_EFLAGS_AF   = 1 << VM86_EFLAGS_AF_SHIFT,
	VM86_EFLAGS_ZF   = 1 << VM86_EFLAGS_ZF_SHIFT,
	VM86_EFLAGS_SF   = 1 << VM86_EFLAGS_SF_SHIFT,
	VM86_EFLAGS_TF   = 1 << VM86_EFLAGS_TF_SHIFT,
	VM86_EFLAGS_IF   = 1 << VM86_EFLAGS_IF_SHIFT,
	VM86_EFLAGS_DF   = 1 << VM86_EFLAGS_DF_SHIFT,
	VM86_EFLAGS_OF   = 1 << VM86_EFLAGS_OF_SHIFT,
	VM86_EFLAGS_IOPL = 3 << VM86_EFLAGS_IOPL_SHIFT,
	VM86_EFLAGS_NT   = 1 << VM86_EFLAGS_NT_SHIFT,
	VM86_EFLAGS_RF   = 1 << VM86_EFLAGS_RF_SHIFT,
	VM86_EFLAGS_VM   = 1 << VM86_EFLAGS_VM_SHIFT,
	VM86_EFLAGS_AC   = 1 << VM86_EFLAGS_AC_SHIFT,
	VM86_EFLAGS_VIF  = 1 << VM86_EFLAGS_VIF_SHIFT,
	VM86_EFLAGS_VIP  = 1 << VM86_EFLAGS_VIP_SHIFT,
	VM86_EFLAGS_ID   = 1 << VM86_EFLAGS_ID_SHIFT
};

#define VM86_CC_MASK 0xF

enum vm86_cc {
	VM86_CC_O   = 0x0,
	VM86_CC_NO  = 0x1,
	VM86_CC_B   = 0x2,
	VM86_CC_NB  = 0x3,
	VM86_CC_E   = 0x4,
	VM86_CC_NE  = 0x5,
	VM86_CC_BE  = 0x6,
	VM86_CC_NBE = 0x7,
	VM86_CC_S   = 0x8,
	VM86_CC_NS  = 0x9,
	VM86_CC_P   = 0xA,
	VM86_CC_NP  = 0xB,
	VM86_CC_L   = 0xC,
	VM86_CC_NL  = 0xD,
	VM86_CC_LE  = 0xE,
	VM86_CC_NLE = 0xF
};

enum vm86_excp {
	VM86_EXCP_DE    = 0x0,
	VM86_EXCP_DB    = 0x1,
	VM86_EXCP_NMI   = 0x2,
	VM86_EXCP_BP    = 0x3,
	VM86_EXCP_OF    = 0x4,
	VM86_EXCP_BR    = 0x5,
	VM86_EXCP_UD    = 0x6,
	VM86_EXCP_NM    = 0x7,
	VM86_EXCP_DF    = 0x8,
	VM86_EXCP_CSO   = 0x9,
	VM86_EXCP_TS    = 0xA,
	VM86_EXCP_NP    = 0xB,
	VM86_EXCP_SS    = 0xC,
	VM86_EXCP_GP    = 0xD,
	VM86_EXCP_PF    = 0xE,
	VM86_EXCP_RES15 = 0xF,
	VM86_EXCP_MF    = 0x10,
	VM86_EXCP_AC    = 0x11,
	VM86_EXCP_MC    = 0x12,
	VM86_EXCP_XM    = 0x13,
	VM86_EXCP_VE    = 0x14,
	VM86_EXCP_RES21 = 0x15,
	VM86_EXCP_RES22 = 0x16,
	VM86_EXCP_RES23 = 0x17,
	VM86_EXCP_RES24 = 0x18,
	VM86_EXCP_RES25 = 0x19,
	VM86_EXCP_RES26 = 0x1A,
	VM86_EXCP_RES27 = 0x1B,
	VM86_EXCP_RES28 = 0x1C,
	VM86_EXCP_RES29 = 0x1D,
	VM86_EXCP_SX    = 0x1E,
	VM86_EXCP_RES31 = 0x1F
};

struct vm86_ctx;

#define VM86_GPR(val) ((union vm86_gpr){ (uint32_t)(val) })
#define VM86_RE(reg) ((reg).dw)
#define VM86_RX(reg) ((reg).w.l16)
#define VM86_RH(reg) ((reg).b.l.h8)
#define VM86_RL(reg) ((reg).b.l.l8)

union vm86_gpr
{
	uint32_t dw;
	struct {
		uint16_t l16;
		uint16_t h16;
	} w;
	struct {
		struct {
			uint8_t l8;
			uint8_t h8;
		} l;
		struct {
			uint8_t l8;
			uint8_t h8;
		} h;
	} b;
};

struct vm86_regs {
	enum vm86_reg_mask mask;
	union vm86_gpr eax, ebx, ecx, edx, edi, esi;
	union vm86_gpr ebp, esp;
	uint32_t eip;
	uint32_t eflags;
	uint16_t cs, ds, es, fs, gs, ss;
};


int vm86_setup(void);

int vm86_import_native_ivt(void *ivt, size_t nb_entries);

int vm86_create(struct vm86_ctx **vm86, int use_native_ivt);
int vm86_destroy(struct vm86_ctx **vm86);
int vm86_set_trace(struct vm86_ctx *vm86, int trace);
int vm86_map_segment(struct vm86_ctx *vm86, void *phys_addr, enum vm86_prot_mask prot, void *data, uint32_t len);
int vm86_map_bios_bda(struct vm86_ctx *vm86);
int vm86_map_bios_ebda(struct vm86_ctx *vm86);
int vm86_map_upper_mem(struct vm86_ctx *vm86);
int vm86_unmap_segment(struct vm86_ctx *vm86, void *phys_addr, uint32_t len);
int vm86_write_segment(struct vm86_ctx *vm86, void *phys_addr, const void *data, uint32_t len);
int vm86_read_segment(struct vm86_ctx *vm86, void *data, void *phys_addr, uint32_t len);
int vm86_read_string(struct vm86_ctx *vm86, void *data, void *phys_addr, uint32_t max_size);
int vm86_reset_regs(struct vm86_ctx *vm86);
int vm86_reset_eflags(struct vm86_ctx *vm86);
int vm86_set_regs(struct vm86_ctx *vm86, struct vm86_regs *regs);
int vm86_get_regs(struct vm86_ctx *vm86, struct vm86_regs *regs);
int vm86_clear_regs(struct vm86_ctx *vm86);
int vm86_exec(struct vm86_ctx *vm86, uint16_t timeout_ms);

#endif
