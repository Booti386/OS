SUBDIRS=

BINS=builtin.o

LIBS-builtin.o=
OBJS-builtin.o= \
	memcmp.o \
	memcpy.o \
	memset.o

CFLAGS=$(CFLAGS_common)
CFLAGS+=-I. -Iinclude -I$(TOP) -I$(TOP)/include
LDFLAGS=$(LDFLAGS_reloc)
