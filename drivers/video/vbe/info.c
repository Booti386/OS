/*
 * Copyright (C) 2014-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <arch/arch.h>
#include <arch/mmu.h>
#include <common/kmalloc.h>
#include <common/printk.h>
#include <common/string.h>
#include <os/types.h>

#include "drivers/vm/vm86/vm86.h"

#include "info.h"
#include "modes.h"
#include "vbedrv.h"

int vbe_fetch_info(struct vbe_ctx *ctx)
{
	struct vm86_regs regs;
	uint32_t vbe_info_addr;
	struct vbe_info vbe_info;

	regs.mask = VM86_REG_EAX | VM86_REG_ES | VM86_REG_EDI | VM86_REG_ESP | VM86_REG_EIP;
	VM86_RE(regs.eax) = 0x4F00;
	regs.es = 0x2000;
	VM86_RE(regs.edi) = 0;
	VM86_RE(regs.esp) = 0x7C00;
	regs.eip = 0x7C00;

	vbe_info_addr = VM86_REAL2PHYS_SEG_OFF(regs.es, VM86_RX(regs.edi));

	memset(&vbe_info, 0, sizeof(vbe_info));

	/* Ask to retrieve VBE2+ info */
	memcpy(&vbe_info.signature, "VBE2", sizeof(vbe_info.signature));

	vm86_write_segment(ctx->vm, VOIDPTR(vbe_info_addr), &vbe_info, sizeof(vbe_info));

	vm86_reset_regs(ctx->vm);
	vm86_set_regs(ctx->vm, &regs);
	vm86_exec(ctx->vm, 10);
	vm86_get_regs(ctx->vm, &regs);

	vm86_read_segment(ctx->vm, &vbe_info, VOIDPTR(vbe_info_addr), sizeof(vbe_info));

	if (regs.eax.w.l16 != 0x004F)
		return -1;

	ctx->version.ver = vbe_info.version.ver;
	ctx->oem_version.v.major = vbe_info.oem_ver_bcd.v.major;
	ctx->oem_version.v.minor = vbe_info.oem_ver_bcd.v.minor;

	ctx->oem_name[0] = 0;
	if (vbe_info.oem_name_ptr)
		vm86_read_string(ctx->vm, &ctx->oem_name, VOIDPTR(VM86_REAL2PHYS(vbe_info.oem_name_ptr)), sizeof(ctx->oem_name));

	ctx->vendor_name[0] = 0;
	ctx->product_name[0] = 0;
	ctx->product_rev[0] = 0;
	ctx->nb_modes = 0;

	/* VBE 2.0 */
	if (ctx->version.v.major >= 2)
	{
		if (vbe_info.vendor_name_ptr)
			vm86_read_string(ctx->vm, &ctx->vendor_name, VOIDPTR(VM86_REAL2PHYS(vbe_info.vendor_name_ptr)), sizeof(ctx->vendor_name));

		if (vbe_info.product_name_ptr)
			vm86_read_string(ctx->vm, &ctx->product_name, VOIDPTR(VM86_REAL2PHYS(vbe_info.product_name_ptr)), sizeof(ctx->product_name));

		if (vbe_info.product_rev_ptr)
			vm86_read_string(ctx->vm, &ctx->product_rev, VOIDPTR(VM86_REAL2PHYS(vbe_info.product_rev_ptr)), sizeof(ctx->product_rev));

		ctx->caps_flag.flags = vbe_info.caps_flag.flags;
		ctx->mem_len = vbe_info.mem_nb_64k_blocks * 64 * 1024;

		if (vbe_info.modes_list_ptr)
		{
			vm86_read_segment(ctx->vm, &ctx->mode_nums, VOIDPTR(VM86_REAL2PHYS(vbe_info.modes_list_ptr)), sizeof(ctx->mode_nums));

			while (ctx->nb_modes < VBE_MAX_MODES && ctx->mode_nums[ctx->nb_modes] != 0xFFFF)
				ctx->nb_modes++;
		}
	}

	printk("VBE: VBE version %d.%d.", ctx->version.v.major, ctx->version.v.minor);
	printk("VBE: OEM name \"%s\"", ctx->oem_name);
	printk("VBE: Caps flag 0x%x", ctx->caps_flag.flags);
	printk("VBE: Caps:");
	printk("VBE:  8-bit DAC %ssupported", ctx->caps_flag.bits.dac_8bits ? "" : "not ");
	printk("VBE:  VGA-%scompatible", ctx->caps_flag.bits.not_vga_ctrl ? "in" : "");
	printk("VBE:  %sDAC blank bit", ctx->caps_flag.bits.dac_blank_bit ? "" : "no ");

	/* VBE 3.0 */
	if (ctx->version.v.major >= 3 && ctx->caps_flag.bits.vbe3_stereo)
		printk("VBE:  stereo hardware control via %s connector", ctx->caps_flag.bits.vbe3_stereo_output ? "EVC" : "external stereo");

	printk("VBE: Mem size %d KB", ctx->mem_len / 1024);

	/* VBE 2.0 */
	if (ctx->version.v.major >= 2)
	{
		printk("VBE: OEM version  %d.%d", ctx->oem_version.v.major, ctx->oem_version.v.minor);
		printk("VBE: Vendor name  \"%s\"", ctx->vendor_name);
		printk("VBE: Product name \"%s\"", ctx->product_name);
		printk("VBE: Product rev  \"%s\"", ctx->product_rev);
	}

	return 0;
}
