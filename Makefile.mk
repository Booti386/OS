SUBDIRS-y := tools arch common drivers
boot_subdir := arch/$(ARCH)/boot
OPTSUBDIRS-y := $(boot_subdir)

BINS-y := \
	kernel.o \
	kernel.obj
OBJCOPYBIN_BINS-y := kernel
CUSTBINS-y := kernel.syms

LIBS-kernel.o-y := \
	arch/builtin.o \
	common/builtin.o \
	drivers/builtin.o
OBJS-kernel.o-y := \
	core.c.o

LIBS-kernel.obj-y := kernel.o
OBJS-kernel.obj-y :=
DEPS-kernel.obj-y := arch/$(ARCH)/early0.o

IN-kernel-y := kernel.obj

DEPS-kernel.syms-y := kernel.obj
kernel.syms: kernel.obj
	$(V_ECHO_CMD)echo " NM              $@"
	$(V_SILENT_CMD)$(NM) -P -C -S -n kernel.obj >"$@"

LDFLAGS-kernel.o-y := $(LDFLAGS_RELOC-y)
LDFLAGS-kernel.obj-y := $(LDFLAGS_KERNEL-y)
OBJCOPYFLAGS-kernel-y := $(OBJCOPYFLAGS_BIN-y)

boot: build-boot
build-boot: build-$(boot_subdir)
build-$(boot_subdir): all

clean: clean-boot
clean-boot: clean-$(boot_subdir)
