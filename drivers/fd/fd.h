/*
 * Copyright (C) 2015-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef FD_H
#define FD_H 1

#include <os/types.h>

#include "drviface.h"
#include "dir.h"
#include "stdfile.h"

#define FD_TYPE_NONE     0
#define FD_TYPE_DRVIFACE 1
#define FD_TYPE_DIR      2
#define FD_TYPE_STDFILE  3

#define FD_ATTR_READ      (1 << 0)
#define FD_ATTR_WRITE     (1 << 1)
#define FD_ATTR_EXEC      (1 << 2)

#define FD_ATTR_USR_READ  (FD_ATTR_READ << 6)
#define FD_ATTR_USR_WRITE (FD_ATTR_WRITE << 6)
#define FD_ATTR_USR_EXEC  (FD_ATTR_EXEC << 6)

#define FD_ATTR_USR_RX    (FD_ATTR_USR_READ | FD_ATTR_USR_EXEC)
#define FD_ATTR_USR_RW    (FD_ATTR_USR_READ | FD_ATTR_USR_WRITE)
#define FD_ATTR_USR_RWX   (FD_ATTR_USR_RW | FD_ATTR_USR_EXEC)

#define FD_ATTR_GRP_READ  (FD_ATTR_READ << 3)
#define FD_ATTR_GRP_WRITE (FD_ATTR_WRITE << 3)
#define FD_ATTR_GRP_EXEC  (FD_ATTR_EXEC << 3)

#define FD_ATTR_GRP_RX    (FD_ATTR_GRP_READ | FD_ATTR_GRP_EXEC)
#define FD_ATTR_GRP_RW    (FD_ATTR_GRP_READ | FD_ATTR_GRP_WRITE)
#define FD_ATTR_GRP_RWX   (FD_ATTR_GRP_RW | FD_ATTR_GRP_EXEC)

#define FD_ATTR_OTH_READ  FD_ATTR_READ
#define FD_ATTR_OTH_WRITE FD_ATTR_WRITE
#define FD_ATTR_OTH_EXEC  FD_ATTR_EXEC

#define FD_ATTR_OTH_RX    (FD_ATTR_OTH_READ | FD_ATTR_OTH_EXEC)
#define FD_ATTR_OTH_RW    (FD_ATTR_OTH_READ | FD_ATTR_OTH_WRITE)
#define FD_ATTR_OTH_RWX   (FD_ATTR_OTH_RW | FD_ATTR_OTH_EXEC)

typedef uint16_t fd_attr_t;
typedef uint32_t fd_usrperms_t;
typedef uint32_t fd_grpperms_t;

extern int fd_create(const char *path, int type, int flags);

#endif /* FD_H */
