/*
 * Copyright (C) 2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef STRING_H
#define STRING_H

#include "os/types.h"

extern void *memchr(const void *buf, int c, size_t len);
extern int memcmp(const void *p1, const void *p2, size_t len);
extern void *memcpy(void *dst, const void *src, size_t len);
extern void *memmem(const void *buf, size_t buf_len, const void *expr, size_t expr_len);
extern void *memmove(void *dst, const void *src, size_t len);
extern void *memset(void *dst, int c, size_t len);
extern char *strchr(const char *str, int c);
extern char *strchrnul(const char *str, int c);
extern char *strrchr(const char *str, int c);
extern int strcmp(const char *s1, const char *s2);
extern int strncmp(const char *s1, const char *s2, size_t len);
extern size_t strlen(const char *str);
extern size_t strnlen(const char *str, size_t max_len);
extern char *strstr(const char *str, const char *expr);

#endif
