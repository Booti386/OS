/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <asm/io.h>
#include <arch/interrupt.h>
#include <common/printk.h>
#include <os/types.h>

#define PS2_STATUS_PORT 0x64
#define PS2_OUT_FULL (1 << 0)
#define PS2_IN_FULL  (1 << 1)

#define PS2_CMD_PORT 0x64
#define PS2_RD_CONF_CMD   0x20
#define PS2_WR_CONF_CMD   0x60
#define PS2_DIS_PORT1_CMD 0xAD
#define PS2_EN_PORT1_CMD  0xAE

#define PS2_CONF_PORT1_EN_IRQ (1 << 0)
#define PS2_CONF_PORT2_EN_IRQ (1 << 1)

#define PS2_DATA_PORT 0x60

#define PS2KBD_SCANCODE_CMD 0xF0
#define PS2KBD_EN_SCAN_CMD  0xF4
#define PS2KBD_DIS_SCAN_CMD 0xF5

#define PS2KBD_SCANCODE_GET 0x00
#define PS2KBD_SCANCODE_1   0x01
#define PS2KBD_SCANCODE_2   0x02
#define PS2KBD_SCANCODE_3   0x03

#define PS2KBD_ACK    0xFA
#define PS2KBD_RESEND 0xFE


static unsigned char kbdmap[] = {
	0x1B, 0x1B, 0x1B, 0x1B,	/*      esc     (0x01)  */
	'1', '!', '1', '1',
	'2', '@', '2', '2',
	'3', '#', '3', '3',
	'4', '$', '4', '4',
	'5', '%', '5', '5',
	'6', '^', '6', '6',
	'7', '&', '7', '7',
	'8', '*', '8', '8',
	'9', '(', '9', '9',
	'0', ')', '0', '0',
	'-', '_', '-', '-',
	'=', '+', '=', '=',
	0x08, 0x08, 0x7F, 0x08,	/*      backspace       */
	0x09, 0x09, 0x09, 0x09,	/*      tab     */
	'a', 'A', 'a', 'a',
	'z', 'Z', 'z', 'z',
	'e', 'E', 'e', 'e',
	'r', 'R', 'r', 'r',
	't', 'T', 't', 't',
	'y', 'Y', 'y', 'y',
	'u', 'U', 'u', 'u',
	'i', 'I', 'i', 'i',
	'o', 'O', 'o', 'o',
	'p', 'P', 'p', 'p',
	'^', '^', '^', '^',
	'$', '$', '$', '$',
	0x0A, 0x0A, 0x0A, 0x0A,	/*      enter   */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      ctrl    */
	'q', 'Q', 'q', 'q',
	's', 'S', 's', 's',
	'd', 'D', 'd', 'd',
	'f', 'F', 'f', 'f',
	'g', 'G', 'g', 'g',
	'h', 'H', 'h', 'h',
	'j', 'J', 'j', 'j',
	'k', 'K', 'k', 'k',
	'l', 'L', 'l', 'l',
	'm', 'M', 'm', 'm',
	0x97, '%', 0x97, 0x97,	/*      '"      */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      Lshift  (0x2a)  */
	'<', '>', '<', '<',
	'*', 0xE6, '*', '*',	/*      `~      */
	'w', 'W', 'w', 'w',
	'x', 'X', 'x', 'x',
	'c', 'C', 'c', 'c',
	'v', 'V', 'v', 'v',
	'b', 'B', 'b', 'b',
	'n', 'N', 'n', 'n',
	',', '?', ',', '?',
	';', '.', ';', ';',	/*      ,<      */
	':', '/', ':', ':',	/*      .>      */
	'!', 0x15, '!', '!',	/*      /?      */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      Rshift  (0x36)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x37)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x38)  */
	' ', ' ', ' ', ' ',	/*      space   */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3a)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3b)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3c)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3d)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3e)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3f)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x40)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x41)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x42)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x43)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x44)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x45)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x46)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x47)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x48)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x49)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4a)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4b)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4c)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4d)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4e)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4f)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x50)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x51)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x52)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x53)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x54)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x55)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x56)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x57)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x58)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x59)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5a)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5b)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5c)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5d)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5e)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5f)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x60)  */
	0xFF, 0xFF, 0xFF, 0xFF	/*      (0x61)  */
} ;

static inline uint8_t ps2_read_status(void)
{
	return _inb(PS2_STATUS_PORT);
}

static inline void ps2_write_cmd(uint8_t cmd)
{
	for(int i=0; i<1000000; i++) {
		if(!(ps2_read_status() & PS2_IN_FULL)) {
			_outb(PS2_CMD_PORT, cmd);
			return;
		}
	}

	printk("ps2: Write timeout.");
}

static inline uint8_t ps2_read_result()
{
	for(int i=0; i<1000000; i++) {
		if(ps2_read_status() & PS2_OUT_FULL)
			return _inb(PS2_DATA_PORT);
	}

	printk("ps2: Read timeout.");
	return 0;
}

static inline void ps2kbd_write_cmd(uint8_t cmd)
{
	for(int i=0; i<1000000; i++) {
		if(!(ps2_read_status() & PS2_IN_FULL)) {
			_outb(PS2_DATA_PORT, cmd);
			return;
		}
	}

	printk("ps2: Write timeout.");
}

static inline uint8_t ps2kbd_write_cmd_read_result(uint8_t cmd)
{
	uint8_t result = 0;

	for(int i=0; i<1000000; i++) {
		ps2kbd_write_cmd(cmd);

		result = ps2_read_result();
		if(result == PS2KBD_ACK)
			return result;
		else if(result == PS2KBD_RESEND)
			continue;

		printk("ps2kbd: Unknown command 0x%x result 0x%x.", cmd, result);
	}

	return result;
}

static void ps2kbd_irq_handler(void)
{
	uint8_t keycode = 0;
	int is_pressed = 1;

	keycode = ps2_read_result() - 1;

	if(keycode == 0xE0)
		ps2_read_result();
	else if(keycode >= 0x80) {
		is_pressed = 0;
		keycode -= 0x80;
	}

	printk("ps2kbd: IRQ %c %c", (is_pressed) ? '>' : '<', kbdmap[keycode * 4]);
}

int ps2kbd_setup(void)
{
	uint8_t conf_byte = 0;

	arch_disable_interrupt_push_state();

	ps2_write_cmd(PS2_DIS_PORT1_CMD);

	/* Flush output buffer */
	if(ps2_read_status() & PS2_OUT_FULL)
		ps2_read_result();

	ps2kbd_write_cmd_read_result(PS2KBD_DIS_SCAN_CMD);

	ps2_write_cmd(PS2_RD_CONF_CMD);
	conf_byte = ps2_read_result();
	ps2_write_cmd(PS2_WR_CONF_CMD);
	ps2kbd_write_cmd(conf_byte & (~PS2_CONF_PORT1_EN_IRQ));

	arch_irq_add_handler(1, ps2kbd_irq_handler);

	ps2kbd_write_cmd_read_result(PS2KBD_SCANCODE_CMD);
	ps2kbd_write_cmd_read_result(PS2KBD_SCANCODE_2);

	ps2_write_cmd(PS2_RD_CONF_CMD);
	conf_byte = ps2_read_result();
	ps2_write_cmd(PS2_WR_CONF_CMD);
	ps2kbd_write_cmd(conf_byte | PS2_CONF_PORT1_EN_IRQ);

	ps2kbd_write_cmd_read_result(PS2KBD_EN_SCAN_CMD);

	ps2_write_cmd(PS2_EN_PORT1_CMD);

	arch_interrupt_pop_state();

	printk("ps2kbd: PS2 keyboard configured.");

	return 0;
}
