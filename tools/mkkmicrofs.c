/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fs/kmicrofs.h>
#include <unistd.h>

#include "checksum.h"

#define BUF_LEN 65536

int main(int argc, const char *argv[])
{
	int ret;
	const char *outname = NULL;
	uint32_t align;
	uint32_t align_mask;
	char *test_str;
	const char **innames;
	int nb_infiles;
	FILE *outfile = NULL;
	FILE *infile = NULL;
	kmicrofs_raw_t *raw = NULL;
	kmicrofs_raw_entry_t *entries;
	size_t raw_data_idx;

	if (argc < 4)
	{
		fprintf(stderr, "Usage: %s <out.img> <alignment> <kernel> [file...]\n", argv[0]);
		exit(1);
	}

	outname = argv[1];

	align = strtol(argv[2], &test_str, 0);
	if (!argv[2] || *test_str)
	{
		fprintf(stderr, "Invalid arg[2]: \"%s\" is not a valid number.\n", argv[2]);
		goto exit_error;
	}

	align_mask = align - 1;
	innames = &argv[3];
	nb_infiles = argc - 3;

	outfile = fopen(outname, "wb");
	if (!outfile)
	{
		fprintf(stderr, "Failed to open \"%s\" in write mode.\n", outname);
		goto exit_error;
	}

	raw = malloc(sizeof(raw->header) + nb_infiles * sizeof(kmicrofs_raw_entry_t));
	if (!raw)
	{
		fprintf(stderr, "Failed to allocate memory for output.\n");
		goto exit_error;
	}

	memset(raw, 0, sizeof(raw->header) + nb_infiles * sizeof(kmicrofs_raw_entry_t));
	memcpy(raw->header.sig, KMICROFS_RAW_SIG, sizeof(raw->header.sig));
	raw->header.hdr_chksum = 0;
	raw->header.blob_chksum = 0;
	raw->header.kernel_entry_off = 0;
	raw->header.nb_entries = nb_infiles;
	raw->header.total_len = sizeof(raw->header) + raw->header.nb_entries * sizeof(kmicrofs_raw_entry_t);
	raw->header.entry_list_off = 0;
	entries = (kmicrofs_raw_entry_t *)&raw->blob;
	raw_data_idx = raw->header.entry_list_off + raw->header.nb_entries * sizeof(kmicrofs_raw_entry_t);

	for(uint32_t i = 0; i < raw->header.nb_entries; i++)
	{
		size_t name_len;
		const char *inname;
		void *new_raw;

		if (((sizeof(raw->header) + raw_data_idx) & align_mask) != 0)
		{
			uint32_t diff = align - ((sizeof(raw->header) + raw_data_idx) & align_mask);
			raw_data_idx += diff;
			raw->header.total_len += diff;

			new_raw = realloc(raw, raw->header.total_len);
			if (!new_raw)
			{
				fprintf(stderr, "Failed to allocate memory for output.\n");
				goto exit_error;
			}

			raw = new_raw;
			entries = (kmicrofs_raw_entry_t *)&raw->blob;
		}

		inname = strrchr(innames[i], '/');
		if (inname)
			inname++;
		else
			inname = innames[i];

		name_len = strlen(inname);
		if (!name_len)
		{
			fprintf(stderr, "Input file (%d) does not seem to have a name...\n", i);
			goto exit_error;
		}
		name_len++;
		if(name_len > KMICROFS_MAX_NAME_LEN)
			name_len = KMICROFS_MAX_NAME_LEN;

		infile = fopen(innames[i], "rb");
		if (!infile)
		{
			fprintf(stderr, "Failed to open (%d) \"%s\" read-only.\n", i, innames[i]);
			goto exit_error;
		}

		if (i == 0)
			raw->header.kernel_entry_off = raw->header.entry_list_off + i * sizeof(kmicrofs_raw_entry_t);
		entries[i].data_off = raw_data_idx;
		entries[i].data_len = 0;
		memcpy(&entries[i].name, inname, name_len);

		while (!feof(infile) && !ferror(infile))
		{
			static char buf[BUF_LEN];
			size_t len_read;

			len_read = fread(buf, 1, BUF_LEN, infile);
			if (!len_read)
				continue;

			entries[i].data_len += len_read;
			raw->header.total_len += len_read;

			new_raw = realloc(raw, raw->header.total_len);
			if (!new_raw)
			{
				fprintf(stderr, "Failed to allocate memory for output.\n");
				goto exit_error;
			}

			raw = new_raw;
			entries = (kmicrofs_raw_entry_t *)&raw->blob;
			memcpy(&raw->blob[raw_data_idx], buf, len_read);
			raw_data_idx += len_read;
		}

		ret = ferror(infile);
		fclose(infile);
		infile = NULL;

		if (ret)
		{
			fprintf(stderr, "Failed to read from (%d) \"%s\".\n", i, innames[i]);
			goto exit_error;
		}
	}

	if (raw->header.nb_entries)
		raw->header.blob_chksum = -checksum8(raw->blob, raw->header.total_len - sizeof(raw->header));
	raw->header.hdr_chksum = -checksum8(&raw->header, sizeof(raw->header));

	ret = fwrite(raw, raw->header.total_len, 1, outfile);
	if (ret != 1)
	{
		fprintf(stderr, "Failed to write to \"%s\".\n", outname);
		goto exit_error;
	}

	free(raw);
	fclose(outfile);
	exit(0);

exit_error:
	if (raw)
		free(raw);
	if (infile)
		fclose(infile);
	if (outfile)
		fclose(outfile);
	if (outname)
		remove(outname);
	exit(1);
}
