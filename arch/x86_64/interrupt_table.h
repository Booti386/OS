/*
 * Copyright (C) 2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef INTERRUPT_TABLE_H
#define INTERRUPT_TABLE_H 1

#define INTERRUPT_TABLE_HANDLER_COUNT 256
#define INTERRUPT_TABLE_IRQ_COUNT 16

typedef void (*arch_interrupt_handler_t)(void);

extern const arch_interrupt_handler_t *arch_interrupt_get_handler_table(void);

#define INT_DECL(num) void arch_int_wrap_ ## num (void)

INT_DECL(00);
INT_DECL(01);
INT_DECL(02);
INT_DECL(03);
INT_DECL(04);
INT_DECL(05);
INT_DECL(06);
INT_DECL(07);
INT_DECL(08);
INT_DECL(09);
INT_DECL(0A);
INT_DECL(0B);
INT_DECL(0C);
INT_DECL(0D);
INT_DECL(0E);
INT_DECL(0F);
INT_DECL(10);
INT_DECL(11);
INT_DECL(12);
INT_DECL(13);
INT_DECL(14);
INT_DECL(15);
INT_DECL(16);
INT_DECL(17);
INT_DECL(18);
INT_DECL(19);
INT_DECL(1A);
INT_DECL(1B);
INT_DECL(1C);
INT_DECL(1D);
INT_DECL(1E);
INT_DECL(1F);
INT_DECL(20);
INT_DECL(21);
INT_DECL(22);
INT_DECL(23);
INT_DECL(24);
INT_DECL(25);
INT_DECL(26);
INT_DECL(27);
INT_DECL(28);
INT_DECL(29);
INT_DECL(2A);
INT_DECL(2B);
INT_DECL(2C);
INT_DECL(2D);
INT_DECL(2E);
INT_DECL(2F);
INT_DECL(41);

#endif /* INTERRUPT_TABLE_H */
