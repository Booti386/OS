/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef PIC8259A_H
#define PIC8259A_H

extern void arch_pic8259a_master_eoi(void);
extern void arch_pic8259a_slave_eoi(void);
extern int arch_pic8259a_enable(void);
extern int arch_pic8259a_disable(void);
extern int arch_pic8259a_reconf(void);
extern int arch_pic8259a_setup(void);

#endif
