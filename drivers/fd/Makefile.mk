SUBDIRS-y :=

BINS-y := builtin.o
LIBS-builtin.o-y :=
OBJS-builtin.o-y := \
	dir.c.o \
	drviface.c.o \
	fd.c.o \
	stdfile.c.o

LDFLAGS-y += $(LDFLAGS_RELOC-y)
