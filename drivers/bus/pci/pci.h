/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef PCI_H
#define PCI_H 1

#include <os/types.h>

#define PCI_INVAL_VEN_ID 0xFFFF

#define PCI_VEN_ID_IDX 0
#define PCI_DEV_ID_IDX 2

#define PCI_MATCH_VEN_MASK (1 << 0)
#define PCI_MATCH_DEV_MASK (1 << 1)

struct pci_dev
{
	uint16_t bus;
	uint8_t dev;
	uint8_t func;
	uint16_t ven_id;
	uint16_t dev_id;
	struct pci_driver *drv;
};

struct pci_dev_list
{
	struct pci_dev entry;
	struct pci_dev_list *prev, *next;
};

struct pci_match
{
	uint32_t mask;
	uint16_t ven_id;
	uint16_t dev_id;
};

struct pci_driver
{
	struct pci_match *matches;
	int (*init)(struct pci_dev *dev);
};


extern uint32_t pci_read_dword(struct pci_dev *device, uint8_t index);
extern uint16_t pci_read_word(struct pci_dev *dev, uint8_t index);
extern uint8_t pci_read_byte(struct pci_dev *dev, uint8_t index);
extern int pci_register_driver(struct pci_driver *driver);
extern int pci_setup(void);

#endif /* PCI_H */
