SUBDIRS-y := lib

BINS-y := builtin.o \
	early0.o

LIBS-builtin.o-y := \
	lib/builtin.o

OBJS-builtin.o-y := \
	../i386/apic.c.o \
	../i386/e820.c.o \
	../i386/mtrr.c.o \
	../i386/pic8259a.c.o \
	../i386/vga.c.o \
	arch.c.o \
	mmu.c.o \
	interrupt.c.o \
	interrupt_table.S.o \
	interrupt_table.c.o \
	sched.c.o 

LIBS-early0.o-y :=
OBJS-early0.o-y := \
	early32.S.o \
	early64.S.o

CFLAGS-y += -I. -Iinclude
LDFLAGS-y += $(LDFLAGS_RELOC-y)
