/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <common/printk.h>
#include <common/string.h>
#include <os/types.h>

#include "apmdrv.h"

#include "drivers/vm/vm86/vm86.h"

static struct apm_ctx apm_ctx;

int apm_check_support(struct apm_ctx *ctx)
{
	struct vm86_regs regs;

	regs.mask = VM86_REG_EAX | VM86_REG_EBX | VM86_REG_ECX | VM86_REG_ESP | VM86_REG_EIP | VM86_REG_EFLAGS;
	vm86_get_regs(ctx->vm, &regs);
	VM86_RE(regs.eax) = 0x5300;
	VM86_RE(regs.ebx) = 0x0000;
	VM86_RE(regs.ecx) = 0x0000;
	VM86_RE(regs.esp) = 0x7C00;
	regs.eip = 0x7C00;
	regs.eflags &= ~VM86_EFLAGS_CF;

	return 0;
}

int apm_setup(void)
{
	struct apm_ctx *ctx = &apm_ctx;
	int ret;
	char instrs[] =
		"\xCD\x15"     /* INT 15h */
		VM86_COP_QUIT; /* VM86_QUIT */

	ctx->vm = NULL;
	vm86_create(&ctx->vm, 1);
	vm86_set_trace(ctx->vm, 0);
	vm86_map_bios_bda(ctx->vm);
	vm86_map_bios_ebda(ctx->vm);
	vm86_map_upper_mem(ctx->vm);
	vm86_write_segment(ctx->vm, VOIDPTR(0x7C00), instrs, sizeof(instrs));

	ret = apm_check_support(ctx);
	if (ret < 0)
		return ret;

	return 0;
}
