/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef COREFS_H
#define COREFS_H

extern size_t fs_corefs_create_node(size_t parent_node_id, char *name, int type, fd_attr_t attrs, fd_grpperms_t grp, fd_usrperms_t usr);
extern size_t fs_corefs_read(size_t node_id, size_t offset, void *buf, size_t buf_len);
extern size_t fs_corefs_write(size_t node_id, size_t offset, void *buf, size_t buf_len);
extern size_t fs_corefs_realloc(size_t node_id, size_t len);
extern size_t fs_corefs_free(size_t node_id);
extern int fs_corefs_setup(void);

#endif
