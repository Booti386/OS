/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef KMICROFS_H
#define KMICROFS_H

#ifdef __KERNEL__
#  include <os/types.h>
#else
#  include <stdint.h>
#endif

#define KMICROFS_MAX_NAME_LEN 64
#define KMICROFS_RAW_SIG "_KMICROFS_"

typedef struct
{
	uint32_t data_off;
	uint32_t data_len;
	char name[KMICROFS_MAX_NAME_LEN];
} kmicrofs_raw_entry_t;

typedef struct
{
	struct
	{
		char sig[10]; /* "_KMICROFS_" */
		uint8_t hdr_chksum;
		uint8_t blob_chksum;
		uint32_t kernel_entry_off; /* For the bootloader */
		uint32_t total_len;
		uint32_t nb_entries;
		uint32_t entry_list_off;
	} header;
	char blob[];
} kmicrofs_raw_t;

#ifdef __KERNEL__

extern int fs_kmicrofs_open(const char *filename);
extern int fs_kmicrofs_close(size_t entry_num);
extern size_t fs_kmicrofs_read(size_t entry_num, size_t offset, void *buf, size_t buf_len);
extern int fs_kmicrofs_load(kmicrofs_raw_t *raw_phys);
extern int fs_kmicrofs_setup(void);

#endif

#endif
