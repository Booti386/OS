%define BOOTADDR 0x7C00

[ORG BOOTADDR]
[BITS 16]

%define SECTORSIZE 0x200
%define PAGE_SIZE 0x1000

; Stack grows backward, and there is free space before us, so just use it
%define STACKADDR BOOTADDR

; int 13h AH=2 can only access 63 sectors in a row
%define BUFNBSECTORS 63 ; 31.5 Ko
%define BUFADDR      0x7E00
%define KMICROFSADDR 0x100000
%define CODESEG 0x08
%define DATASEG 0x10

%define CR0_PE (1 << 0)

%define GETSEG(addr) (addr >> 4)
%define GETOFF(addr) (addr & 0xF)

%define WORD(high, low) (((high & 0xFF) << 8) | (low & 0xFF))

_bootloader_entry:
	jmp word 0:_bootloader_entry.post_cs_reset ; Reset CS to 0

	.post_cs_reset:
	xor word ax, ax
	mov word ds, ax
	mov word es, ax

	mov word ss, ax
	lea dword esp, [STACKADDR]

	mov byte [bootdrv], dl

	; Enable A20 gate
	call test_A20
	jz A20_enabled

	; Bios
	mov word ax, 0x2401
	int 0x15
	call test_A20

	; Kbd
	call wait_kbd

	mov  byte al, 0xD1
	out  byte 0x64, al
	call wait_kbd

	mov  byte al, 0xDF
	out  byte 0x60, al
	call wait_kbd

	call test_A20

	; 0xEE
	in byte al, 0xEE
	call test_A20

	; System Control Port A
	in byte al, 0x92
	test byte al, 0x02
	jnz A20_fail

	or byte al, 0x02
	out byte 0x92, al

	call test_A20

A20_fail:
read_fail:
	push word 0xB800
	pop word es
	mov word si, (.txt)
	xor word di, di
	mov word cx, (.txt.end - .txt)
	rep movsb
	jmp _halt

	.txt db 'H', 0x0F, 'A', 0x0F, 'L', 0x0F, 'T', 0x0F, 'E', 0x0F, 'D', 0x0F
	.txt.end:

_halt:
	cli
	hlt
	jmp _halt


test_A20:
	push word es
	push word 0xFFFF
	pop word es
	mov word bx, 0x0500
	mov word ax, [es:bx + 0x10]
	inc word ax
	mov word [bx], ax
	cmp word [es:bx + 0x10], ax
	pop word es
	jne A20_enabled
	ret

wait_kbd:
	mov word cx, 0xFFFF
	.loop:
	in   byte al, 0x64
	test byte al, 2
	loopnz wait_kbd.loop, cx
	ret

A20_enabled:
	cli
	push ds
	push es

	; Enable protected mode
	mov dword eax, cr0
	or  byte al, CR0_PE
	mov dword cr0, eax

	; Load 32 bits gdt
	lgdt [gdtr]

	; Extend ds & es limit to 4GB
	mov word bx, DATASEG
	mov word ds, bx
	mov word es, bx

	; Disable protected mode
	and byte al, ~CR0_PE
	mov dword cr0, eax

	pop es
	pop ds

read_kmicrofs:
	sti
	push dword -1
	.loop:

	xor dword eax, eax
	mov byte ah, 0x42 ; Disk - LBA read
	lea word si, [disk_addr_packet] ; Packet buffer address
	mov byte dl, [bootdrv]
	int 0x13 ; Load kernel into memory
	mov dword eax, [disk_addr_packet.NbBlocks]
	jnc .copy_high
	mov dword [disk_addr_packet.NbBlocks], BUFNBSECTORS ; NbBlocks is trashed on failure

	; Try with legacy sector read
	;xor dword edx,edx
	;mov dword eax, [disk_addr_packet.StartBlock]
	;mov dword ecx, BUFNBSECTORS
	;div dword ecx
	;mov byte dh, al ; dh = head number
	;shr dword eax, 8
	;mov byte ch, al ; ch = low 8 bits of cylinder number
	;shl byte ah, 6
	;mov byte cl, ah ; cl(6-7) = high 2 bits of cylinder number
	; AH = 0x02: Disk - Read sectors
	mov word ax, WORD(0x02, BUFNBSECTORS)
	sub byte al, dl
	; ch
	inc byte dl
	or byte cl, dl ; cl(0-5) = sector number
	; dh
	mov byte dl, [bootdrv]
	push word es
	push word GETSEG(BUFADDR)
	pop word es
	mov word bx, GETOFF(BUFADDR)
	int 0x13 ; Load kernel into memory
	pop word es
	jc read_fail

	.copy_high:
	xor dword ebx, ebx
	mov byte bl, al
	mov dword eax, [disk_addr_packet.StartBlock]
	dec dword eax
	imul dword eax, SECTORSIZE
	lea dword edi, [KMICROFSADDR + eax]
	lea dword esi, [BUFADDR]
	imul dword ecx, ebx, SECTORSIZE / 4
	cld
	rep a32 movsd
	.loop_pre_chk:
	pop dword eax
	cmp dword eax, -1
	jne .loop_chk

	; Get the kmicrofs total size
	mov dword eax, [BUFADDR + 16]

	.loop_chk:
	add dword [disk_addr_packet.StartBlock], ebx
	imul dword ebx, SECTORSIZE
	sub dword eax, ebx
	push dword eax
	ja .loop

read_success:
	cli

	; Disable the PIC, we do not really need to be disturbed by IRQs at this time...
	mov byte al, 0xFF
	out byte 0xA1, al
	out byte 0x21, al

	; Enable protected mode (again)
	mov dword eax, cr0
	or  byte al, CR0_PE
	mov dword cr0, eax

	; Switch to protected mode cs
	jmp dword CODESEG:_bootloader_entry_32

[BITS 32]
_bootloader_entry_32:
	mov word ax, DATASEG
	mov word ds, ax
	mov word es, ax

	mov word ss, ax
	lea dword esp, [STACKADDR]

	lea dword ebx, [KMICROFSADDR] ; Cache KMICROFSADDR into ebx to save code length

	; Fetch the kernel address/size/dst address from kmicrofs
	lea dword eax, [ebx + 28] ; eax = kmicrofs data = kmicrofs addr + sizeof(kmicrofs.header)
	mov dword esi, eax ; esi = kmicrofs data
	add dword eax, [ebx + 12] ; eax = Kernel entry = kmicrofs data + Kernel entry offset

	mov dword edi, [ebx + 16] ; edi = kmicrofs size
	lea dword edi, [ebx + edi + PAGE_SIZE] ; edi = kmicrofs end = kmicrofs addr + kmicrofs size + 1 page
	and dword edi, ~(PAGE_SIZE - 1) ; edi = kernel dst addr = next page after kmicrofs end, page-aligned

	; esi = kmicrofs data
	add dword esi, [eax + 0] ; esi = Kernel addr = kmicrofs data + kernel offset

	mov dword ecx, [eax + 4] ; ecx = Kernel size
	shr dword ecx, 2 ; ecx = Kernel size in dwords
	inc dword ecx ; ecx = Kernel size in dwords + trailing bytes possibly discarded by shr

	mov dword eax, edi ; eax = kernel dst addr, we'll need this later

	; Move the kernel to its final place
	; edi = Kernel dst addr
	; esi = Kernel addr
	; ecx = Kernel size in dwords
	cld
	rep movsd

	; Transmit kmicrofs addr to the kernel
	push dword ebx

	; Jump to the kernel
	jmp dword eax ; eax = kernel dst addr

bootdrv db 0

ALIGN 8
disk_addr_packet:
	.start:
	.Size       db disk_addr_packet.end - disk_addr_packet.start ; The size of the structure, 0x10 (disk_addr_packet) or 0x18 (disk_addr_packet_64)
	.Res0       db 0 ; Reserved (must be 0)
	.NbBlocks   dw BUFNBSECTORS ; Number of blocks to transfer
	.DstAddr    dd (GETSEG(BUFADDR) << 16) | GETOFF(BUFADDR) ; 32 bits real address of the transfer buffer
	.StartBlock dq 1 ; The first block index
	.end:
; Disabled to keep precious space:
;	.DstAddr64  dq BUFADDR ; 64 bits flat address of the transfer buffer, optional, used only if Size = 0x18 and DstAddr = 0xFFFF:0xFFFF
;	.end64:

ALIGN 8
gdt:
	; Null descriptor
	dq 0
	; 32-bit code
	dq 0x00CF9B000000FFFF
	; 32-bit data
	dq 0x00CF93000000FFFF
gdt_end:

ALIGN 8
gdtr:
	.limit dw 3 * 8 - 1
	.addr  dd gdt

times 512-4*16-2-($-$$) nop

; Partition table
dq 0, 0
dq 0, 0
dq 0, 0
dq 0, 0

; Boot magic
db 0x55, 0xAA
