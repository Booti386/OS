/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

.code32

/* Override default memcmp */
.func _memcmp, memcmp
.global memcmp
memcmp:
/* Extract args from stack */
	xchgl 4(%esp), %edi /* cdecl preserve */
	xchgl 8(%esp), %esi /* cdecl preserve */
	movl 12(%esp), %edx
	
/* Ensure the length is a multiple of 4 */
	movl %edx, %ecx
	andl $0x3, %edx
	shrl $0x2, %ecx

	cld
	repe cmpsl
	movl %edx, %ecx
	repe cmpsb
	movl 4(%esp), %edi /* cdecl restore */
	movl 8(%esp), %esi /* cdecl restore */
	jb _inf
	jg _sup

_eq:
	xorl %eax, %eax
	ret

_inf:
	movl $-1, %eax
	ret

_sup:
	movl $1, %eax
	ret
