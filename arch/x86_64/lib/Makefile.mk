SUBDIRS-y :=

BINS-y := builtin.o

LIBS-builtin.o-y :=
OBJS-builtin.o-y := \
	memcmp.S.o \
	memcpy.S.o \
	memset.S.o

CFLAGS-y += -I. -Iinclude
LDFLAGS-y += $(LDFLAGS_RELOC-y)
