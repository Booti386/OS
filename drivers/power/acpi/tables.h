/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef ACPI_TABLES_H
#define ACPI_TABLES_H

#include "os/types.h"

#define RSDP_SIG       "RSD PTR "
#define RSDP_REV0_LEN  20
#define RSDP_REV2_LEN  36

struct __attribute__((packed)) rsdp_t
{
	struct
	{
		char     sig[8]; /* "RSD PTR " */
		uint8_t  chksum;
		char     oem_id[6];
		uint8_t  rev;
		uint32_t rsdt_addr;
	} v1;

	/* ACPI 2.0/RSDP rev 2 */
	struct
	{
		uint32_t len;
		uint64_t xsdt_addr;
		uint8_t  ext_chksum;
		uint8_t  res0[3];
	} v2;
};
typedef struct rsdp_t rsdp_t;

struct __attribute__((packed)) desc_header_t
{
	char     sig[4];
	uint32_t len;
	uint8_t  rev;
	uint8_t  chksum;
	char     oem_id[6];
	char     oem_tbl_id[8];
	uint32_t oem_rev;
	uint32_t creator_id;
	uint32_t creator_rev;
};
typedef struct desc_header_t desc_header_t;

struct __attribute__((packed)) rsdt_t
{
	desc_header_t header;
	uint32_t      entries[];
};
typedef struct rsdt_t rsdt_t;

struct __attribute__((packed)) xsdt_t
{
	desc_header_t header;
	uint64_t      entries[];
};
typedef struct xsdt_t xsdt_t;

#endif
