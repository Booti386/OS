/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef PCIE_H
#define PCIE_H 1

#include <os/types.h>

#include "pci.h"

#define PCIE_MATCH_VEN_MASK (1 << 0)
#define PCIE_MATCH_DEV_MASK (1 << 1)

struct pcie_dev
{
	struct pci_dev *pci_dev;
	struct pcie_driver *drv;
};

struct pcie_match
{
	uint32_t mask;
	uint16_t ven_id;
	uint16_t dev_id;
};

struct pcie_driver
{
	struct pcie_match *matches;

	int (*init)(struct pcie_dev *dev);
};


extern int pcie_register_driver(struct pcie_driver *driver);
extern int pcie_setup(void);

#endif /* PCIE_H */
