/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef MEMORY_MAP_H
#define MEMORY_MAP_H

#include "os/types.h"

#define MEMORY_TYPE_USABLE    1
/* MEMORY_TYPE_RESERVED is platform
 * specific, and can be everything but 1.
 */

struct memory_map_t
{
	void *address;
	uintptr_t length;
	uint8_t type;
	struct memory_map_t *next;
};
typedef struct memory_map_t memory_map_t;

#endif
