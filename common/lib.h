/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIB_H
#define LIB_H

#include "os/types.h"

#define UTOSTR_BUF_MAXLEN   11
#define ITOSTR_BUF_MAXLEN   12
#define X32TOSTR_BUF_MAXLEN 9
#define X64TOSTR_BUF_MAXLEN 17

extern unsigned int strtou(const char *str);
extern int strtoi(const char *str);
extern char *utostr(char *dst, unsigned int v);
extern char *itostr(char *dst, unsigned int v);
extern char *x32tostr(char *dst, uint32_t v, int zeroing);
extern char *x64tostr(char *dst, uint64_t v, int zeroing);

#endif
