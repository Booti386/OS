/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef ASM_CPUID_H
#define ASM_CPUID_H

#include "os/types.h"

static inline void _cpuid(uint32_t eax, uint32_t *ret_eax, uint32_t *ret_ebx, uint32_t *ret_ecx, uint32_t *ret_edx)
{
	uint32_t ebx = 0;
	uint32_t ecx = 0;
	uint32_t edx = 0;

	__asm__ __volatile__ ("cpuid\n" :"=a"(eax), "=b"(ebx), "=c"(ecx), "=d"(edx) :"a"(eax), "b"(0), "c"(0), "d"(0));
	if(ret_eax)
		*ret_eax = eax;
	if(ret_ebx)
		*ret_ebx = ebx;
	if(ret_ecx)
		*ret_ecx = ecx;
	if(ret_edx)
		*ret_edx = edx;
}

static inline uint32_t _cpuid_eax(uint32_t eax)
{
	_cpuid(eax, &eax, NULL, NULL, NULL);
	return eax;
}

static inline uint32_t _cpuid_ebx(uint32_t eax)
{
	uint32_t ebx = 0;
	_cpuid(eax, NULL, &ebx, NULL, NULL);
	return ebx;
}

static inline uint32_t _cpuid_ecx(uint32_t eax)
{
	uint32_t ecx = 0;
	_cpuid(eax, NULL, NULL, &ecx, NULL);
	return ecx;
}

static inline uint32_t _cpuid_edx(uint32_t eax)
{
	uint32_t edx = 0;
	_cpuid(eax, NULL, NULL, NULL, &edx);
	return edx;
}

static inline int _cpuid_mtrr_support(void)
{
	return _cpuid_edx(1) & (1 << 12) ? 1 : 0; /* cpuid.mtrr */
}

static inline int _cpuid_apic_support(void)
{
	return _cpuid_edx(1) & (1 << 9) ? 1 : 0; /* cpuid.apic */
}

static inline int _cpuid_sse_support(void)
{
	return _cpuid_edx(1) & (1 << 25) ? 1 : 0; /* cpuid.sse */
}

static inline int _cpuid_sse2_support(void)
{
	return _cpuid_edx(1) & (1 << 26) ? 1 : 0; /* cpuid.sse2 */
}

static inline int _cpuid_sse3_support(void)
{
	return _cpuid_ecx(1) & (1 << 0) ? 1 : 0; /* cpuid.sse3 */
}

static inline int _cpuid_ssse3_support(void)
{
	return _cpuid_ecx(1) & (1 << 9) ? 1 : 0; /* cpuid.ssse3 */
}

static inline int _cpuid_ssse4_1_support(void)
{
	return _cpuid_ecx(1) & (1 << 19) ? 1 : 0; /* cpuid.sse4.1 */
}

static inline int _cpuid_ssse4_2_support(void)
{
	return _cpuid_ecx(1) & (1 << 20) ? 1 : 0; /* cpuid.sse4.2 */
}

static inline int _cpuid_ssse4a_support(void)
{
	if(_cpuid_eax(0x80000000) >= 0x80000001)
		return _cpuid_ecx(0x80000001) & (1 << 6) ? 1 : 0; /* cpuid.sse4a */
	return 0;
}

static inline int _cpuid_avx_support(void)
{
	return _cpuid_ecx(1) & (1 << 28) ? 1 : 0; /* cpuid.avx */
}

static inline uint8_t _cpuid_max_phy_addr(void)
{
	if(_cpuid_eax(0x80000000) >= 0x80000008)
		return _cpuid_eax(0x80000008) & 0xFF; /* cpuid.MAXPHYADDR */
	return 32;
}

static inline uint8_t _cpuid_max_lin_addr(void)
{
	if(_cpuid_eax(0x80000000) >= 0x80000008)
		return (_cpuid_eax(0x80000008) >> 8) & 0xFF; /* cpuid.MAXLINADDR */
	return 32;
}

#endif
