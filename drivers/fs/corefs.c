/*
 * Copyright (C) 2014-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <common/kmalloc.h>
#include <common/string.h>
#include <common/printk.h>
#include <fd/fd.h>
#include <os/types.h>

struct corefs_entry
{
	uint64_t node_num;
	char *name;

	struct corefs_entry *parent;

	int type;
	fd_attr_t attrs;
	fd_grpperms_t grp;
	fd_usrperms_t usr;

	uint64_t timestamp_nano;
	uint64_t timestamp_edit_nano;
	uint64_t timestamp_access_nano;

	struct corefs_entry *prev;
	struct corefs_entry *next;
};

struct corefs_dir_node
{
	struct corefs_entry generic;
	size_t nb_entries;
	struct corefs_entry *entries;
};

struct corefs_file_node
{
	struct corefs_entry generic;
	size_t data_len;
	void *data;
};

struct corefs_drv_ctx
{
	struct corefs_dir_node root;
	size_t next_node_id;
	struct corefs_entry **nodes_list;
};

static struct corefs_drv_ctx drv_ctx = {
	.root = {
		.generic = {
			.node_num = 0,
			.name = NULL,
			.parent = NULL,
			.type = FD_TYPE_DIR,
			.attrs = FD_ATTR_USR_RWX | FD_ATTR_GRP_RWX | FD_ATTR_OTH_RX,
			.grp = 0,
			.usr = 0,
			.prev = NULL,
			.next = NULL
		},
	},
	.next_node_id = 1,
	.nodes_list = NULL
};


size_t fs_corefs_create_node(size_t parent_node_id, char *name, int type, fd_attr_t attrs, fd_grpperms_t grp, fd_usrperms_t usr)
{
	struct corefs_dir_node *parent_node;
	struct corefs_entry *previous_child_node, *child_node;
	size_t name_len;

	parent_node = (struct corefs_dir_node *)drv_ctx.nodes_list[parent_node_id];
	if(!parent_node)
		return 0;
	if(parent_node->generic.type != FD_TYPE_DIR)
		return 0;

	if(!parent_node->nb_entries)
		previous_child_node = NULL;
	else
		previous_child_node = &parent_node->entries[0];
	for (size_t i = 0; i < parent_node->nb_entries; i++)
		previous_child_node = previous_child_node->next;

	if(type == FD_TYPE_DIR)
		child_node = kmalloc(sizeof(struct corefs_dir_node));
	else
		child_node = kmalloc(sizeof(struct corefs_file_node));
	if(!child_node)
		return 0;

	name_len = strlen(name);
	child_node->name = kmalloc(name_len);
	if(!child_node->name) {
		kfree(child_node);
		return 0;
	}

	child_node->node_num = drv_ctx.next_node_id++;
	memcpy(child_node->name, name, name_len);

	child_node->parent = drv_ctx.nodes_list[parent_node_id];

	child_node->type = type;
	child_node->attrs = attrs;
	child_node->grp = grp;
	child_node->usr = usr;

	child_node->prev = NULL;
	child_node->next = NULL;

	if(type == FD_TYPE_DIR) {
		((struct corefs_dir_node *)child_node)->nb_entries = 0;
		((struct corefs_dir_node *)child_node)->entries = NULL;
	} else {
		((struct corefs_file_node *)child_node)->data_len = 0;
		((struct corefs_file_node *)child_node)->data = NULL;
	}

	if(previous_child_node) {
		previous_child_node->next = child_node;
		child_node->prev = previous_child_node;
	} else {
		parent_node->entries = child_node;
	}
	parent_node->nb_entries++;

	return child_node->node_num;
}

size_t fs_corefs_read(size_t node_id, size_t offset, void *buf, size_t buf_len)
{
	struct corefs_file_node *node;
	size_t len;
	char *data;

	node = (struct corefs_file_node *)drv_ctx.nodes_list[node_id];
	if(!node)
		return -1;
	if(node->generic.type == FD_TYPE_DIR)
		return -1;

	if(offset >= node->data_len)
		return -1;

	if(!node->data_len) {
		if(offset > 0)
			return -1;
		return 0;
	}

	if(!node->data) {
		printk("corefs: ERROR: Corrupted node 0x%x: length %d with null data pointer.", node->generic.node_num, node->data_len);
		return -1;
	}

	len = (offset + buf_len <= node->data_len) ? buf_len : node->data_len - offset;
	data = node->data;

	memcpy(buf, &data[offset], len);

	return len;
}

size_t fs_corefs_write(size_t node_id, size_t offset, void *buf, size_t buf_len)
{
	struct corefs_file_node *node;
	size_t len;
	char *data;

	node = (struct corefs_file_node *)drv_ctx.nodes_list[node_id];
	if(!node)
		return -1;
	if(node->generic.type == FD_TYPE_DIR)
		return -1;

	if(offset >= node->data_len)
		return -1;

	if(!node->data_len) {
		if(offset > 0)
			return -1;
		return 0;
	}

	if(!node->data) {
		printk("corefs: ERROR: Corrupted node 0x%x: length %d with null data pointer.", node->generic.node_num, node->data_len);
		return -1;
	}

	len = (buf_len < node->data_len) ? buf_len : node->data_len;
	data = node->data;

	if(offset + len > node->data_len)
		len = node->data_len - offset;

	memcpy(&data[offset], buf, len);

	return len;
}

int fs_corefs_realloc(size_t node_id, size_t len)
{
	struct corefs_file_node *node;
	void *new_data;

	node = (struct corefs_file_node *)drv_ctx.nodes_list[node_id];
	if(!node)
		return -1;
	if(node->generic.type == FD_TYPE_DIR)
		return -1;

	if(node->data_len == len)
		return 0;

	if(!node->data_len && node->data) {
		printk("corefs: ERROR: Corrupted node 0x%x: length %d with null data pointer.", node->generic.node_num, node->data_len);
		return -1;
	}

	if(!len) {
		if(!node->data_len)
			return 0;
		node->data_len = 0;
		kfree(node->data);
		node->data = NULL;
		return 0;
	}

	new_data = kmalloc(len);
	if(!new_data)
		return -1;

	if(node->data_len && len)
		memcpy(new_data, node->data, (node->data_len < len) ? node->data_len : len);

	if(node->data_len)
		kfree(node->data);
	node->data_len = len;
	node->data = new_data;

	return 0;
}

int fs_corefs_free_node(size_t node_id)
{
	struct corefs_entry *node;

	node = drv_ctx.nodes_list[node_id];
	if(!node)
		return -1;

	/* Do not delete the root entry */
	if(node == &drv_ctx.root.generic)
		return -1;

	/* Do not delete a directory if not empty */
	if(node->type == FD_TYPE_DIR) {
		if(((struct corefs_dir_node *)node)->nb_entries)
			return -1;
	} else {
		/* Free file data */
		fs_corefs_realloc(node_id, 0);
	}

	if(node->prev)
		node->prev->next = node->next;
	if(node->next)
		node->next->prev = node->prev;

	((struct corefs_dir_node *)node->parent)->nb_entries--;
	if(!node->prev)
		((struct corefs_dir_node *)node->parent)->entries = node->next;

	kfree(node->name);
	kfree(node);

	return 0;
}

int fs_corefs_setup(void)
{


	printk("corefs: Root node created.");

	return 0;
}
