/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef ASM_GDT_H
#define ASM_GDT_H

#define GDT_CODESEG16 0x08
#define GDT_DATASEG16 0x10
#define GDT_CODESEG32 0x18
#define GDT_DATASEG32 0x20
#define GDT_USRCODESEG32 0x28
#define GDT_USRDATASEG32 0x30
#define GDT_CODESEG64 0x38
#define GDT_DATASEG64 0x40
#define GDT_USRCODESEG64 0x48
#define GDT_USRDATASEG64 0x50

#endif
