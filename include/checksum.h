/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef CHECKSUM_H
#define CHECKSUM_H 1

#if __KERNEL__
#  include <os/types.h>
#else
#  include <stddef.h>
#endif

static inline int checksum8(void *data, size_t len) {
	uint8_t *d = data;
	uint8_t sum = 0;
	size_t i;
	size_t len_u64 = len & ~7;
	size_t len_u8  = len &  7;

	if (!len)
		return -1;

	i = 0;
	while (i < len_u64)
	{
		sum += d[i] + d[i + 1] + d[i + 2] + d[i + 3]
				+ d[i + 4] + d[i + 5] + d[i + 6] + d[i + 7];
		i += 8;
	}
	d += i;

	i = 0;
	while (i < len_u8)
		sum += d[i++];

	return sum;
}

static inline int checksum16(void *data, size_t len) {
	uint16_t *d = data;
	uint16_t sum = 0;
	size_t i;
	size_t len_u64 = (len & ~15) >> 1;
	size_t len_u16 = (len &  15) >> 1;

	if (!len)
		return -1;

	i = 0;
	while (i < len_u64)
	{
		sum += d[i] + d[i + 1] + d[i + 2] + d[i + 3];
		i += 4;
	}
	d += i;

	i = 0;
	while (i < len_u16)
		sum += d[i++];

	return sum;
}

#endif /* CHECKSUM_H */
