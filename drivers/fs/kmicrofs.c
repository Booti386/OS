/*
 * Copyright (C) 2015-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <checksum.h>
#include <arch/mmu.h>
#include <common/kmalloc.h>
#include <common/printk.h>
#include <common/string.h>
#include <fd/fd.h>
#include <fs/kmicrofs.h>
#include <os/types.h>

struct kmicrofs_entry
{
	char name[KMICROFS_MAX_NAME_LEN + 1];
	size_t data_len;
	void *data;
};

struct kmicrofs_ctx
{
	size_t nb_entries;
	struct kmicrofs_entry *entries;
};

static struct kmicrofs_ctx kmicrofs_ctx = {
	.nb_entries = 0,
	.entries = NULL
};


int fs_kmicrofs_open(const char *filename)
{
	size_t name_len;

	name_len = strlen(filename);
	if(name_len > KMICROFS_MAX_NAME_LEN)
		return -1;

	for(size_t i = 0; i < kmicrofs_ctx.nb_entries; i++)
		if(strlen(kmicrofs_ctx.entries[i].name) == name_len
				&& memcmp(kmicrofs_ctx.entries[i].name, filename, name_len) == 0)
			return i;
	return -1;
}

int fs_kmicrofs_close(size_t entry_num)
{
	if(entry_num >= kmicrofs_ctx.nb_entries)
		return -1;
	return 0;
}

size_t fs_kmicrofs_read(size_t entry_num, size_t offset, void *buf, size_t buf_len)
{
	struct kmicrofs_entry *entry;
	size_t len;
	char *data;

	if(entry_num >= kmicrofs_ctx.nb_entries)
		return 0;
	entry = (struct kmicrofs_entry *)&kmicrofs_ctx.entries[entry_num];
	if(offset >= entry->data_len)
		return 0;
	if(!entry->data_len)
		return 0;

	len = (offset + buf_len < entry->data_len) ? buf_len : entry->data_len - offset;
	data = entry->data;

	memcpy(buf, &data[offset], len);
	return len;
}

int fs_kmicrofs_load(kmicrofs_raw_t *raw_phys)
{
	kmicrofs_raw_t *raw;
	uint32_t total_len;
	char *blob;
	kmicrofs_raw_entry_t *raw_entry;

	raw = arch_map_phys(raw_phys, sizeof(raw->header));
	if(!raw)
		return -1;

	if(memcmp(raw->header.sig, KMICROFS_RAW_SIG, sizeof(raw->header.sig)) != 0)
		return -1;
	if(checksum8(&raw->header, sizeof(raw->header)) != 0)
		return -1;

	total_len = raw->header.total_len;
	arch_unmap(raw, sizeof(raw->header));
	raw = arch_map_phys(raw_phys, total_len);
	if(!raw)
		return -1;

	kmicrofs_ctx.nb_entries = raw->header.nb_entries;
	if(!kmicrofs_ctx.nb_entries)
	{
		kmicrofs_ctx.entries = NULL;
		return 0;
	}
	if(((raw->header.blob_chksum + checksum8(raw->blob, raw->header.total_len - sizeof(raw->header))) & 0xFF) != 0)
		return -1;

	blob = raw->blob;
	raw_entry = (kmicrofs_raw_entry_t *)&blob[raw->header.entry_list_off];

	kmicrofs_ctx.entries = kmalloc(kmicrofs_ctx.nb_entries * sizeof(kmicrofs_ctx.entries[0]));
	if (!kmicrofs_ctx.entries)
		return -1;

	for (size_t i = 0; i < raw->header.nb_entries; i++)
	{
		memcpy(kmicrofs_ctx.entries[i].name, raw_entry[i].name, KMICROFS_MAX_NAME_LEN);
		kmicrofs_ctx.entries[i].name[KMICROFS_MAX_NAME_LEN] = 0;
		kmicrofs_ctx.entries[i].data_len = raw_entry[i].data_len;
		kmicrofs_ctx.entries[i].data = kmalloc(kmicrofs_ctx.entries[i].data_len);
		memcpy(kmicrofs_ctx.entries[i].data, &blob[raw_entry[i].data_off], kmicrofs_ctx.entries[i].data_len);
	}

	printk("kmicrofs: Initialized %d entries.", kmicrofs_ctx.nb_entries);
	return 0;
}

int fs_kmicrofs_setup(void)
{
	return 0;
}
