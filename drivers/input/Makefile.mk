SUBDIRS-y := kbd mouse

BINS-y := builtin.o
LIBS-builtin.o-y := \
	kbd/builtin.o \
	mouse/builtin.o
OBJS-builtin.o-y :=

LDFLAGS-y += $(LDFLAGS_RELOC-y)
