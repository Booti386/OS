/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef MTRR_H
#define MTRR_H

#include "os/types.h"

#define MTRR_TYPE_UC 0x0
#define MTRR_TYPE_WC 0x1
#define MTRR_TYPE_WT 0x4
#define MTRR_TYPE_WP 0x5
#define MTRR_TYPE_WB 0x6

extern int arch_mtrr_def_range(void *addr, uint64_t len, uint8_t type);
extern int arch_mtrr_setup(void);

#endif
