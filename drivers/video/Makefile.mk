SUBDIRS-y := vbe

BINS-y := builtin.o
LIBS-builtin.o-y := \
	vbe/builtin.o
OBJS-builtin.o-y := \
	video.c.o

CFLAGS-y += -I$(TOP)/drivers
LDFLAGS-y += $(LDFLAGS_RELOC-y)
