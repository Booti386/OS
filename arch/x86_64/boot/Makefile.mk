SUBDIRS-y :=

CUSTBINS-y := \
    ../../i386/boot/boot.bin \
    kmicrofs.img \
    floppy.img

DEPS-../../i386/boot/boot.bin-y := \
	../../i386/boot/boot.asm

DEPS-kmicrofs.img-y := \
    $(TOP)/kernel \
    $(TOP)/kernel.syms

DEPS-floppy.img-y := \
    ../../i386/boot/boot.bin \
    kmicrofs.img

../../i386/boot/boot.bin:
	@echo " NASM            $@"
	@$(NASM) -o $@ ../../i386/boot/boot.asm -f bin


%.bin: %.asm $$(DEPS-$$@)
	@echo " NASM\t\t$@"
	@nasm -o $@ $< -f bin

kmicrofs.img:
	@echo " GEN             $@"
	@$(TOP)/tools/mkkmicrofs $@ 0x1000 $(TOP)/kernel $(TOP)/kernel.syms

floppy.img:
	@echo " GEN             $@"
	@cat ../../i386/boot/boot.bin kmicrofs.img /dev/zero | dd of=$@ bs=512 count=2048 >/dev/null 2>&1
