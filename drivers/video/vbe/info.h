/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef INFO_H
#define INFO_H 1

#include "os/types.h"

struct vbe_ctx;

union vbe_ver
{
	uint16_t ver;
	struct __attribute__((packed)) {
		uint8_t minor;
		uint8_t major;
	} v;
};

union vbe_ver_bcd
{
	char ver[2];
	struct __attribute__((packed)) {
		uint8_t major;
		uint8_t minor;
	} v;
};

union vbe_caps
{
	uint32_t flags;
	struct __attribute__((packed)) {
		uint32_t dac_8bits          : 1;
		uint32_t not_vga_ctrl       : 1;
		uint32_t dac_blank_bit      : 1;
		uint32_t vbe3_stereo        : 1;
		uint32_t vbe3_stereo_output : 1;
		uint32_t af_hard_cursor     : 1;
		uint32_t af_hard_clipping   : 1;
		uint32_t af_transp_bitblt   : 1;
		uint32_t res0               : 24;
	} bits;
};

/* Table returned by int 0x10 with ax=0x4F00 */
struct __attribute__((packed)) vbe_info
{
	/* VBE 1.x */
	char signature[4];
	union vbe_ver version;
	uint32_t oem_name_ptr;
	union vbe_caps caps_flag;
	uint32_t modes_list_ptr;
	uint16_t mem_nb_64k_blocks;

	/* VBE 2.0 */
	union vbe_ver_bcd oem_ver_bcd;
	uint32_t vendor_name_ptr;
	uint32_t product_name_ptr;
	uint32_t product_rev_ptr;

	char res0[222];

	char oem_blob[256];
};

extern int vbe_fetch_info(struct vbe_ctx *ctx);

#endif /* INFO_H */
