%define BOOTADDR 0x7C00

[ORG BOOTADDR]
[BITS 16]

%define SECTORSIZE 0x200
%define PAGE_SIZE 0x1000

; Stack grows backward, and there is free space before us, so just use it
%define STACKADDR BOOTADDR

; int 13h AH=2 can only access 63 sectors in a row
%define BUFNBSECTORS 63 ; 31.5 Ko
%define BUFADDR      0x7E00
%define KMICROFSADDR 0x100000
%define CODESEG 0x08
%define DATASEG 0x10

%define CR0_PE (1 << 0)

%define GETSEG(addr) (addr >> 4)
%define GETOFF(addr) (addr & 0xF)

%define WORD(high, low) (((high & 0xFF) << 8) | (low & 0xFF))

_bootloader_entry:
	jmp dword 0:_bootloader_entry.post_cs_reset ; Reset CS to 0

	.post_cs_reset:
	xor word ax, ax
	mov word ss, ax
	mov word ds, ax
	mov word es, ax
	lea dword esp, [STACKADDR]

	mov byte [bootdrv], dl

	; Enable A20 gate
	call test_A20
	jz A20_enabled

	; Bios
	mov word ax, 0x2401
	int 0x15
	call test_A20

	; Kbd
	call wait_kbd

	mov  byte al, 0xD1
	out  byte 0x64, al
	call wait_kbd

	mov  byte al, 0xDF
	out  byte 0x60, al
	call wait_kbd

	call test_A20

	; 0xEE
	in byte al, 0xEE
	call test_A20

	; System Control Port A
	in byte al, 0x92
	test byte al, 0x02
	jnz A20_fail

	or byte al, 0x02
	out byte 0x92, al

	call test_A20

A20_fail:
read_fail:
	push word 0xB800
	pop word es
	mov word si, (.txt)
	xor word di, di
	mov word cx, (.txt.end - .txt)
	rep movsb
	jmp _halt

	.txt db 'H', 0x0F, 'A', 0x0F, 'L', 0x0F, 'T', 0x0F, 'E', 0x0F, 'D', 0x0F
	.txt.end:

_halt:
	cli
	hlt
	jmp _halt


test_A20:
	push word es
	push word 0xFFFF
	pop word es
	mov word bx, 0x0500
	mov byte [bx], 1
	mov byte [es:bx + 0x10], 0
	pop word es
	cmp byte [bx], 1
	jz A20_enabled
	ret

wait_kbd:
	mov word cx, 0xFFFF
	.loop:
	in   byte al, 0x64
	test byte al, 2
	loopnz wait_kbd.loop, cx
	ret

A20_enabled:
	cli
	push ds
	push es

	; Enable protected mode
	mov dword eax, cr0
	or  byte al, CR0_PE
	mov dword cr0, eax

	; Load 32 bits gdt
	lgdt [gdtr]

	; Extend ds & es limit to 4GB
	mov word bx, DATASEG
	mov word ds, bx
	mov word es, bx

	; Disable protected mode
	and byte al, ~CR0_PE
	mov dword cr0, eax

	pop es
	pop ds

read_kmicrofs:
	sti

	xor eax, eax
	mov ebx, eax
	mov ecx, eax
	mov edx, eax
	mov edi, eax
	mov esi, eax
	mov ebp, eax
	push dword eax
	popfd
	sti

	; mov cs, ax
	jmp dword 0:.next
.next:

	mov ds, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	mov ax, 0x2000
	mov es, ax
	mov eax, 0x4F00
	mov esp, 0x7c00
	int 10h

bootdrv db 0

ALIGN 8
disk_addr_packet:
	.start:
	.Size       db disk_addr_packet.end - disk_addr_packet.start ; The size of the structure, 0x10 (disk_addr_packet) or 0x18 (disk_addr_packet_64)
	.Res0       db 0 ; Reserved (must be 0)
	.NbBlocks   dw BUFNBSECTORS ; Number of blocks to transfer
	.DstAddr    dd (GETSEG(BUFADDR) << 16) | GETOFF(BUFADDR) ; 32 bits real address of the transfer buffer
	.StartBlock dq 1 ; The first block index
	.end:
; Disabled to keep precious space:
;	.DstAddr64  dq BUFADDR ; 64 bits flat address of the transfer buffer, optional, used only if Size = 0x18 and DstAddr = 0xFFFF:0xFFFF
;	.end64:

ALIGN 8
gdt:
	; Null descriptor
	dq 0
	; 32-bit code
	dq 0x00CF9B000000FFFF
	; 32-bit data
	dq 0x00CF93000000FFFF
gdt_end:

ALIGN 8
gdtr:
	.limit dw 3 * 8 - 1
	.addr  dd gdt

times 512-4*16-2-($-$$) nop

; Partition table
dq 0, 0
dq 0, 0
dq 0, 0
dq 0, 0

; Boot magic
db 0x55, 0xAA
