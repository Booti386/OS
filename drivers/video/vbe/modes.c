/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <arch/arch.h>
#include <arch/mmu.h>
#include <arch/mtrr.h>
#include <common/kmalloc.h>
#include <common/printk.h>
#include <common/string.h>
#include <os/types.h>

#include "info.h"
#include "modes.h"
#include "vbedrv.h"

#include "drivers/vm/vm86/vm86.h"

int vbe_set_mode(struct vbe_ctx *ctx, uint16_t mode)
{
	struct vm86_regs regs;

	regs.mask = VM86_REG_EAX | VM86_REG_EBX | VM86_REG_ESP | VM86_REG_EIP;
	VM86_RE(regs.eax) = 0x4F02;
	VM86_RE(regs.ebx) = mode;
	VM86_RE(regs.esp) = 0x7C00;
	regs.eip = 0x7C00;

	vm86_reset_regs(ctx->vm);
	vm86_set_regs(ctx->vm, &regs);
	vm86_exec(ctx->vm, 10);
	vm86_get_regs(ctx->vm, &regs);

	if (VM86_RX(regs.eax) != 0x004F)
	{
		printk("VBE: Set mode 0x%x failed (AH=0x%x).", mode, VM86_RH(regs.eax));
		return -1;
	}

	for (int i = 0; i < ctx->nb_modes; i++)
	{
		void *fb_phys_addr = VOIDPTR(ctx->mode_infos[i].fb_phys_addr);

		if (ctx->mode_nums[i] == mode
				&& fb_phys_addr)
		{
			ctx->cur_mode_idx = i;
			ctx->cur_mode = &ctx->mode_infos[i];
			ctx->fb_len = ctx->cur_mode->bpp * ctx->cur_mode->width * ctx->cur_mode->height / 8;
			ctx->fb = arch_map_phys(fb_phys_addr, ctx->fb_len);

			arch_mtrr_def_range(fb_phys_addr, ctx->mem_len, MTRR_TYPE_WC);
			break;
		}
	}

	return 0;
}

static int vbe_fetch_mode_info(struct vbe_ctx *ctx, struct vbe_mode_info *mode_info, int mode)
{
	struct vm86_regs regs;
	uint32_t mode_info_addr;

	regs.mask = VM86_REG_EAX | VM86_REG_ECX | VM86_REG_ES | VM86_REG_EDI | VM86_REG_ESP | VM86_REG_EIP;
	regs.eax.dw = 0x4F01;
	regs.ecx.dw = mode;
	regs.es = 0x2000;
	regs.edi.dw = 0;
	regs.esp.dw = 0x7C00;
	regs.eip = 0x7C00;

	mode_info_addr = VM86_REAL2PHYS_SEG_OFF(regs.es, regs.edi.w.l16);

	memset(mode_info, 0, sizeof(*mode_info));

	vm86_write_segment(ctx->vm, VOIDPTR(mode_info_addr), mode_info, sizeof(*mode_info));

	vm86_reset_regs(ctx->vm);
	vm86_set_regs(ctx->vm, &regs);
	vm86_exec(ctx->vm, 10);
	vm86_get_regs(ctx->vm, &regs);

	vm86_read_segment(ctx->vm, mode_info, VOIDPTR(mode_info_addr), sizeof(*mode_info));

	if(regs.eax.w.l16 != 0x004F)
		return -1;

	return 0;
}

int vbe_fetch_modes(struct vbe_ctx *ctx)
{
	int ret;

	if(!ctx->nb_modes) {
		ctx->mode_infos = NULL;
		printk("VBE: Dummy implementation without modes.");
		return 0;
	}

	ctx->mode_infos = kmalloc(ctx->nb_modes * sizeof(*ctx->mode_infos));
	if(!ctx->mode_infos)
		return -1;

	ctx->cur_mode = NULL;

	printk("VBE: Supported modes (%d):", ctx->nb_modes);

	for(int i = 0; i < ctx->nb_modes; i++) {
		struct vbe_mode_info *cur_mode_info;

		cur_mode_info = &ctx->mode_infos[i];

		ret = vbe_fetch_mode_info(ctx, cur_mode_info, ctx->mode_nums[i]);
		if(ret < 0) {
			printk("VBE:  0x%x: No info available.", ctx->mode_nums[i] & 0x7FF);
			continue;
		}

		printk("VBE:  0x%x: %dx%d, %d bits, 0x%x, %s, %s, %s",
			ctx->mode_nums[i] & 0x7FF,
			cur_mode_info->width,
			cur_mode_info->height,
			cur_mode_info->bpp,
			cur_mode_info->fb_phys_addr,
			cur_mode_info->mode_attrs.bits.hardware_support ? "HWSupport" : "NoHWSupport",
			cur_mode_info->mode_attrs.bits.color ? "Color" : "Monochrome",
			cur_mode_info->mode_attrs.bits.graphics ? "Graphics" : "Text");
	}

	return 0;
}
