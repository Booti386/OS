/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <gas_intrin.h>

.file __FILE__

.code64

/* Override default memcpy */
FUNC_DECL(memcpy)
/* Ensure the length is a multiple of 8 */
	movq %rdx, %rcx
	andq $0x7, %rdx
	shrq $0x3, %rcx

	cld
	rep movsq
	movq %rdx, %rcx
	rep movsb

	ret
FUNC_END(memcpy)
