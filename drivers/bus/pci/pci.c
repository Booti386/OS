/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <os/types.h>
#include <asm/io.h>
#include <common/kmalloc.h>
#include <common/string.h>
#include <common/printk.h>

#include "pci.h"

#define PCI_CONFIG_ADDR 0xCF8
#define PCI_CONFIG_DATA 0xCFC

struct pci_drv_ctx
{
	struct pci_dev_list *devs;
};

static struct pci_drv_ctx drv_ctx;

uint32_t pci_read_dword(struct pci_dev *device, uint8_t index)
{
	uint32_t addr;
	uint32_t bus = (uint32_t)device->bus;
	uint32_t dev = (uint32_t)device->dev;
	uint32_t func = (uint32_t)device->func;

	addr = (1 << 31) /* Enable bit */
		| (bus << 16)
		| ((dev & 0x1F) << 11)
		| ((func & 0x7) << 8)
		| (index & (~0x3));

	_outl(PCI_CONFIG_ADDR, addr);

	return _inl(PCI_CONFIG_DATA);
}

uint16_t pci_read_word(struct pci_dev *dev, uint8_t index)
{
	return (uint16_t)(pci_read_dword(dev, index & (~0x3)) >> (index & 0x2));
}

uint8_t pci_read_byte(struct pci_dev *dev, uint8_t index)
{
	return (uint8_t)(pci_read_dword(dev, index & (~0x3)) >> (index & 0x3));
}

int pci_register_driver(struct pci_driver *driver)
{
	struct pci_dev_list *dev = drv_ctx.devs;

	while (dev)
	{
		/* TODO: Check match */
		dev->entry.drv = driver;
		dev->entry.drv->init(&dev->entry);

		dev = dev->next;
	}

	return 0;
}

static struct pci_dev_list *pci_enumerate_devices(void)
{
	struct pci_dev       test_dev;
	struct pci_dev_list *dev_list = NULL;
	struct pci_dev_list *dev_ptr = NULL;
	struct pci_dev_list *alloc = NULL;

	memset(&test_dev, 0, sizeof(struct pci_dev));

	/* TODO: Improve (do smart search) */
	for (test_dev.bus = 0; test_dev.bus < 256; test_dev.bus++)
	{
		for (test_dev.dev = 0; test_dev.dev < 32; test_dev.dev++)
		{
			for (test_dev.func = 0; test_dev.func < 8; test_dev.func++)
			{
				test_dev.ven_id = pci_read_word(&test_dev, PCI_VEN_ID_IDX);

				if (test_dev.ven_id == PCI_INVAL_VEN_ID)
					continue;

				test_dev.dev_id = pci_read_word(&test_dev, PCI_DEV_ID_IDX);

				alloc = kmalloc(sizeof(struct pci_dev_list));
				if (!alloc)
					return NULL;

				if (!dev_list)
					dev_list = alloc;
				else
					dev_ptr->next = alloc;

				alloc->prev = dev_ptr;
				alloc->next = NULL;
				dev_ptr = alloc;

				memcpy(&dev_ptr->entry, &test_dev, sizeof(dev_ptr->entry));
				dev_ptr->entry.drv = NULL;
			}
		}
	}

	return dev_list;
}

int pci_setup(void)
{
	drv_ctx.devs = pci_enumerate_devices();

	return 0;
}
