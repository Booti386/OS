/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef E820_H
#define E820_H

#include "os/types.h"
#include "memory_map.h"

#define MEMORY_TYPE_AVAILABLE 1
#define MEMORY_TYPE_RESERVED  2
#define MEMORY_TYPE_ACPI      3
#define MEMORY_TYPE_ACPI_NVS  4

extern int arch_memory_map_setup(void);
extern memory_map_t *arch_memory_map_get(void);
extern int arch_memory_map_update(void *address, uint64_t length, uint8_t type);

#endif
