CFLAGS_ARCH-y := -m32 -fno-PIC -mno-sse -mno-mmx -fstack-protector -mstack-protector-guard=global -mno-red-zone -I$(TOP)/arch/i386/include/common
LDFLAGS_ARCH-y := -melf_i386
