/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "asm/io.h"
#include "common/printk.h"
#include "arch/interrupt.h"
#include "arch/pic8259a.h"

#define PIC1_COMMAND 0x20
#define PIC2_COMMAND 0xA0

#define PIC1_DATA 0x21
#define PIC2_DATA 0xA1

#define ICW1_INIT 0x10
#define ICW1_ICW4 0x01

#define ICW4_8086 0x01

#define PIC_EOI 0x20

struct pic8259a_ctx_t {
	uint8_t enabled;
	uint8_t is_setup;
};
typedef struct pic8259a_ctx_t pic8259a_ctx_t;


static pic8259a_ctx_t pic8259a_ctx;

void arch_pic8259a_master_eoi(void)
{
	if(!pic8259a_ctx.enabled)
		return;
	_outb(PIC1_COMMAND, PIC_EOI);
}

void arch_pic8259a_slave_eoi(void)
{
	if(!pic8259a_ctx.enabled)
		return;
	_outb(PIC2_COMMAND, PIC_EOI);
}

int arch_pic8259a_enable(void)
{
	if(pic8259a_ctx.enabled)
		return 0;
	arch_disable_interrupt_push_state();
	_outb(PIC1_DATA, 0);
	_outb(PIC2_DATA, 0);
	arch_interrupt_pop_state();

	pic8259a_ctx.enabled = 1;
	printk("8259a: PIC enabled.");

	return 0;
}

int arch_pic8259a_disable(void)
{
	if(!pic8259a_ctx.enabled)
		return 0;
	pic8259a_ctx.enabled = 0;

	arch_disable_interrupt_push_state();
	_outb(PIC1_DATA, 0xFF);
	_outb(PIC2_DATA, 0xFF);
	arch_interrupt_pop_state();

	printk("8259a: PIC disabled.");

	return 0;
}

int arch_pic8259a_reconf(void)
{
	if(!pic8259a_ctx.is_setup)
		return 0;

	arch_disable_interrupt_push_state();

	/* ICW1 */
	_outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4);
	_outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);

	/* ICW2 */
	_outb(PIC1_DATA, 0x20); /* Master PIC IRQ0 is mapped to int 0x20 */
	_outb(PIC2_DATA, 0x28); /* Slave PIC IRQ8 is mapped to int 0x28 */

	/* ICW3 */
	_outb(PIC1_DATA, 0x04); /* Master PIC IRQ2 is mapped to slave PIC */
	_outb(PIC2_DATA, 0x02); /* Slave PIC is defined as cascaded */

	/* ICW4 */
	_outb(PIC1_DATA, ICW4_8086); /* Master PIC operates in 8086 mode */
	_outb(PIC2_DATA, ICW4_8086); /* Slave PIC operates in 8086 mode */

	arch_interrupt_pop_state();

	return 0;
}

int arch_pic8259a_setup(void)
{
	pic8259a_ctx.enabled = 0;
	pic8259a_ctx.is_setup = 1;

	arch_pic8259a_reconf();
	arch_pic8259a_enable();

	return 0;
}
