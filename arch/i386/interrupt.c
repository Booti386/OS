/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

static void irq_run_handlers(int num);

#include "os/types.h"
#include "asm/gdt.h"
#include "common/kmalloc.h"
#include "common/printk.h"
#include "ints.h"

#define TASK_GATE 0x5
#define INT_GATE  0xE
#define TRAP_GATE 0xF

struct __attribute__((packed)) idtr_t
{
	uint16_t limit;
	void *addr;
};
typedef struct idtr_t idtr_t;

struct __attribute__((packed)) idtdesc_t {
	uint16_t off_low;
	uint16_t seg_sel;
	uint8_t  res0;
	uint8_t  type     : 4;
	uint8_t  stor_seg : 1;
	uint8_t  dpl      : 2;
	uint8_t  pres     : 1;
	uint16_t off_high;
};
typedef struct idtdesc_t idtdesc_t;

struct interrupt_state_entry_t
{
	uint8_t enabled;

	struct interrupt_state_entry_t *prev;
};
typedef struct interrupt_state_entry_t interrupt_state_entry_t;

struct interrupt_ctx_t {
	uint8_t enabled;
	interrupt_state_entry_t *states;

	idtr_t idtr __attribute__((aligned(8)));
	idtdesc_t idt[256] __attribute__((aligned(sizeof(idtdesc_t))));

	void (*irq_handlers[16])(void);
};
typedef struct interrupt_ctx_t interrupt_ctx_t;


static interrupt_ctx_t interrupt_ctx;

void arch_disable_interrupt(void)
{
	interrupt_ctx.enabled = 0;
	__asm__ __volatile__("cli \n");
}

void arch_enable_interrupt(void)
{
	__asm__ __volatile__("sti \n");
	interrupt_ctx.enabled = 1;
}

void arch_disable_interrupt_push_state(void)
{
	interrupt_state_entry_t *entry;

	entry = kmalloc(sizeof(*entry));
	entry->prev = interrupt_ctx.states;
	interrupt_ctx.states = entry;

	interrupt_ctx.states->enabled = interrupt_ctx.enabled;

	arch_disable_interrupt();
}

void arch_enable_interrupt_push_state(void)
{
	interrupt_state_entry_t *entry;

	entry = kmalloc(sizeof(*entry));
	entry->prev = interrupt_ctx.states;
	interrupt_ctx.states = entry;

	interrupt_ctx.states->enabled = interrupt_ctx.enabled;

	arch_enable_interrupt();
}

void arch_interrupt_pop_state(void)
{
	interrupt_state_entry_t *entry;

	if(!interrupt_ctx.states)
		return;

	if(interrupt_ctx.states->enabled)
		arch_enable_interrupt();
	else
		arch_disable_interrupt();

	interrupt_ctx.enabled = interrupt_ctx.states->enabled;

	entry = interrupt_ctx.states->prev;
	kfree(interrupt_ctx.states);
	interrupt_ctx.states = entry;
}

void arch_irq_add_handler(int num, void (*handler)(void))
{
	interrupt_ctx.irq_handlers[num] = handler;
}

static void irq_run_handlers(int num)
{
	if(interrupt_ctx.irq_handlers[num])
		interrupt_ctx.irq_handlers[num]();
}

int arch_interrupt_setup(void)
{
	interrupt_ctx.states = NULL;

	for(int i = 0; i < sizeof(interrupt_ctx.idt) / sizeof(interrupt_ctx.idt[0]); i++) {
		if(!int_array[i])
			continue;
		interrupt_ctx.idt[i].res0 = 0;

		interrupt_ctx.idt[i].off_low = ((uint32_t)int_array[i]) & 0xFFFF;
		interrupt_ctx.idt[i].seg_sel = GDT_CODESEG32 & 0xFFFF;
		interrupt_ctx.idt[i].off_high = ((uint32_t)int_array[i]) >> 16;

		interrupt_ctx.idt[i].dpl = 0;
		interrupt_ctx.idt[i].stor_seg = 0;
		interrupt_ctx.idt[i].type = INT_GATE;
		interrupt_ctx.idt[i].pres = 1;
	}

	interrupt_ctx.idtr.limit = sizeof(interrupt_ctx.idt) - 1;
	interrupt_ctx.idtr.addr = &interrupt_ctx.idt;

	for(int i = 0; i < sizeof(interrupt_ctx.irq_handlers) / sizeof(interrupt_ctx.irq_handlers[0]); i++)
		interrupt_ctx.irq_handlers[i] = NULL;

	__asm__ __volatile__("lidtl %0" :: "m"(interrupt_ctx.idtr));
	arch_enable_interrupt();

	return 0;
}
