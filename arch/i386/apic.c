/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "asm/cpuid.h"
#include "asm/msr.h"
#include "common/printk.h"
#include "arch/mmu.h"
#include "arch/interrupt.h"
#include "arch/pic8259a.h"

#define IA32_APIC_BASE 0x0000001B
#define APIC_ENABLE 0x800

#define APIC_REG_SVR (0xF0 / 8)
#define APIC_SOFT_ENABLE_MASK (1 << 8)

struct apic_ctx_t {
	uint8_t  is_avail;
	uint8_t enabled;

	void    *phys_addr;
	uint64_t registers[512] __attribute__((aligned(0x1000)));
};
typedef struct apic_ctx_t apic_ctx_t;


static apic_ctx_t apic_ctx;

int arch_apic_enable(void)
{
	if(apic_ctx.enabled)
		return 0;
	arch_disable_interrupt_push_state();
	apic_ctx.registers[APIC_REG_SVR] |= APIC_SOFT_ENABLE_MASK;
	arch_interrupt_pop_state();

	apic_ctx.enabled = 1;
	printk("apic: APIC enabled.");

	return 0;
}

int arch_apic_disable(void)
{
	if(!apic_ctx.enabled)
		return 0;
	apic_ctx.enabled = 0;

	arch_disable_interrupt_push_state();
	apic_ctx.registers[APIC_REG_SVR] &= ~APIC_SOFT_ENABLE_MASK;
	arch_interrupt_pop_state();

	printk("apic: APIC disabled.");

	return 0;
}

int arch_apic_setup(void)
{
	apic_ctx.is_avail = _cpuid_apic_support();
	apic_ctx.is_avail = 0; /* Not impl for now */
	if(!apic_ctx.is_avail) {
		printk("apic: Not supported.");
		return -1;
	}

	apic_ctx.phys_addr = arch_get_phys_addr(&apic_ctx.registers);

	arch_disable_interrupt_push_state();

	/* Hardware enable and remap */
	_wrmsr(IA32_APIC_BASE, (uintptr_t)apic_ctx.phys_addr | APIC_ENABLE);
	printk("apic: Remapped to %p => %p.", apic_ctx.phys_addr, &apic_ctx.registers);

	printk("apic: Disabling the legacy PIC.");
	arch_pic8259a_disable();

	printk("apic: Enabling the APIC.");
	arch_apic_enable();

	arch_interrupt_pop_state();

	return 0;
}
