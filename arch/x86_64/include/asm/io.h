/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef ASM_IO_H
#define ASM_IO_H

#include "os/types.h"

static inline void _outb(uint16_t port, uint8_t data)
{
	__asm__ __volatile__ ("outb %%al, %%dx\n" ::"d"(port), "a"(data));
}

static inline void _outw(uint16_t port, uint16_t data)
{
	__asm__ __volatile__ ("outw %%ax, %%dx\n" ::"d"(port), "a"(data));
}

static inline void _outl(uint16_t port, uint32_t data)
{
	__asm__ __volatile__ ("outl %%eax, %%dx\n" ::"d"(port), "a"(data));
}

static inline uint8_t _inb(uint16_t port)
{
	uint8_t ret;
	__asm__ __volatile__ ("inb %%dx, %%al\n" :"=a"(ret) :"d"(port));
	return ret;
}

static inline uint16_t _inw(uint16_t port)
{
	uint16_t ret;
	__asm__ __volatile__ ("inw %%dx, %%ax\n" :"=a"(ret) :"d"(port));
	return ret;
}

static inline uint32_t _inl(uint16_t port)
{
	uint32_t ret;
	__asm__ __volatile__ ("inl %%dx, %%eax\n" :"=a"(ret) :"d"(port));
	return ret;
}

#endif
