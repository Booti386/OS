CFLAGS_ARCH-y := -m64 -fno-PIC -mcmodel=kernel -mno-sse -mno-mmx -fstack-protector -mstack-protector-guard=global -mno-red-zone -I$(TOP)/arch/i386/include/common
LDFLAGS_ARCH-y := -melf_x86_64
