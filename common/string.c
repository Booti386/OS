/*
 * Copyright (C) 2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"

#include "common/string.h"

static void *noarch_memchr(const void *buf, int c, size_t len)
{
	const char *s = buf;
	const char *ends = &s[len];

	while (s != ends && *s != c)
		s++;

	if (s == ends)
		return NULL;

	return (void *)s;
}

static int noarch_memcmp(const void *p1, const void *p2, size_t len)
{
	const char *s1 = p1;
	const char *ends1 = &s1[len];
	const char *s2 = p2;
	int result = 0;

	while (s1 != ends1 && (result = *s1++ - *s2++) == 0)
		;

	return result;
}

static void *noarch_memcpy_up(void *dst, const void *src, size_t len)
{
	const char *s = src;
	const char *ends = &s[len];
	char *d = dst;

	if (dst == src)
		return dst;

	while (s != ends)
		*d++ = *s++;

	return dst;
}

static void *noarch_memcpy_down(void *dst, const void *src, size_t len)
{
	const char *s = src;
	char *d = dst;

	if (dst == src)
		return dst;

	s = &s[len];
	d = &d[len];

	while (s != src)
		*--d = *--s;

	return dst;
}

static void *noarch_memmem(const void *buf, size_t buf_len, const void *expr, size_t expr_len)
{
	const char *s = buf;
	const char *xpr = expr;
	int c0;

 	if (!expr_len)
		return (void *)buf;

	c0 = xpr[0];

	while (1)
	{
		const char *ss;

		ss = memchr(s, c0, buf_len);
		if (!ss)
			return NULL;

		buf_len -= ss - s;
		if (buf_len < expr_len)
			return NULL;

		if (memcmp(ss, expr, expr_len) == 0)
 			return (void *)ss;

		s = &ss[1];
		buf_len--;
	}

	return NULL;
}

static void *noarch_memmove(void *dst, const void *src, size_t len)
{
	if (!len)
		return dst;

	if (dst < src)
		noarch_memcpy_up(dst, src, len);
	else if (dst > src)
		noarch_memcpy_down(dst, src, len);

	return dst;
}

static void *noarch_memset(void *dst, int c, size_t len)
{
	char *d = dst;

	while (len--)
		*d++ = c;

	return dst;
}

static char *noarch_strchrnul(const char *str, int c)
{
	while (*str != c && *str)
		str++;

	return (char *)str;
}

static char *noarch_strchr(const char *str, int c)
{
	str = strchrnul(str, c);
	if (*str != c)
		return NULL;

	return (char *)str;
}

static char *noarch_strrchr(const char *str, int c)
{
	const char *s = NULL;

	while (1)
	{
		if (*str == c)
			s = str;

		/* Check for \0 late in case c is \0 */
		if (!*str)
			break;

		str++;
	}

	return (char *)s;
}

static int noarch_strcmp(const char *s1, const char *s2)
{
	int ret;

	while ((ret = *s1++ - *s2++) == 0 && s1[-1])
		;

	return ret;
}

static int noarch_strcmp_lim_s2(const char *s1, const char *s2)
{
	int ret = 0;

	while (*s2 && (ret = *s1++ - *s2++) == 0)
		;

	return ret;
}

static int noarch_strncmp(const char *s1, const char *s2, size_t len)
{
	const char *ends1 = &s1[len];
	int ret = 0;

	while (s1 != ends1 && (ret = *s1++ - *s2++) == 0 && s1[-1])
		;

	return ret;
}

static size_t noarch_strlen(const char *str)
{
	const char *s = str;

	while (*s)
		s++;

	return (size_t)(s - str);
}

static size_t noarch_strnlen(const char *str, size_t max_len)
{
	size_t len = 0;

	while (len != max_len && *str++)
		len++;

	return len;
}

static char *noarch_strstr(const char *str, const char *expr)
{
	int c0 = expr[0];

	while (1)
	{
		str = strchr(str, c0);
		if (!str)
			return NULL;

		if (noarch_strcmp_lim_s2(str, expr) == 0)
 			return (char *)str;

		str++;
	}

	return NULL;
}

/* Overridable by arch */

__attribute__((weak)) void *memchr(const void *buf, int c, size_t len)
{
	return noarch_memchr(buf, c, len);
}

__attribute__((weak)) int memcmp(const void *p1, const void *p2, size_t len)
{
	return noarch_memcmp(p1, p2, len);
}

__attribute__((weak)) void *memcpy(void *dst, const void *src, size_t len)
{
	return noarch_memcpy_up(dst, src, len);
}

__attribute__((weak)) void *memmem(const void *buf, size_t buf_len, const void *expr, size_t expr_len)
{
	return noarch_memmem(buf, buf_len, expr, expr_len);
}

__attribute__((weak)) void *memmove(void *dst, const void *src, size_t len)
{
	return noarch_memmove(dst, src, len);
}

__attribute__((weak)) void *memset(void *dst, int c, size_t len)
{
	return noarch_memset(dst, c, len);
}

__attribute__((weak)) char *strchr(const char *str, int c)
{
	return noarch_strchr(str, c);
}

__attribute__((weak)) char *strchrnul(const char *str, int c)
{
	return noarch_strchrnul(str, c);
}

__attribute__((weak)) char *strrchr(const char *str, int c)
{
	return noarch_strrchr(str, c);
}

__attribute__((weak)) int strcmp(const char *s1, const char *s2)
{
	return noarch_strcmp(s1, s2);
}

__attribute__((weak)) int strncmp(const char *s1, const char *s2, size_t len)
{
	return noarch_strncmp(s1, s2, len);
}

__attribute__((weak)) size_t strlen(const char *str)
{
	return noarch_strlen(str);
}

__attribute__((weak)) size_t strnlen(const char *str, size_t max_len)
{
	return noarch_strnlen(str, max_len);
}

__attribute__((weak)) char *strstr(const char *str, const char *expr)
{
	return noarch_strstr(str, expr);
}
