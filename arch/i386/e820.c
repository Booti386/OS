/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <macros.h>
#include <arch/mmu.h>
#include <common/kmalloc.h>
#include <common/printk.h>
#include <common/string.h>
#include <os/types.h>

#include "drivers/vm/vm86/vm86.h"

#include "memory_map.h"

static memory_map_t *e820_map = NULL;
static int e820_map_nbentries = 0;

#define E820_SIG (('S' << 24) | ('M' << 16) | ('A' << 8) | 'P')

#define E820_TYPETOSTR(x) ( \
	(x == 1) ? "AVAILABLE" : \
	(x == 2) ? "RESERVED" : \
	(x == 3) ? "ACPI" : \
	(x == 4) ? "ACPI-NVS" : "RESERVED")

struct __attribute__((packed)) e820_desc
{
	struct __attribute__((packed))
	{
		uint64_t address;
		uint64_t length;
		uint32_t type;
	} v10;

	/* ACPI 3.0 */
	struct __attribute__((packed))
	{
		uint32_t ext_attrs;
	} v30;
};

int arch_memory_map_setup(void)
{
	memory_map_t *map;
	struct e820_desc buf;
	struct vm86_ctx *vm = NULL;
	struct vm86_regs regs;
	char instrs[] =
		"\xCD\x15"     /* INT 15h */
		VM86_COP_QUIT; /* VM86_QUIT */

	vm86_create(&vm, 1);
	vm86_map_bios_bda(vm);
	vm86_map_bios_ebda(vm);
	vm86_map_upper_mem(vm);
	vm86_write_segment(vm, VOIDPTR(0x7C00), instrs, sizeof(instrs));

	e820_map = kmalloc(sizeof(*e820_map));
	/* Alloc check is done in the loop */
	map = e820_map;
	e820_map_nbentries = 0;

	memset(&regs, 0, sizeof(regs));
	regs.mask = VM86_REG_EAX | VM86_REG_EBX | VM86_REG_ECX | VM86_REG_EDX | VM86_REG_ES | VM86_REG_EDI | VM86_REG_ESP | VM86_REG_EIP | VM86_REG_EFLAGS;
	vm86_get_regs(vm, &regs);
	/* First entry offset */
	regs.ebx.dw = 0;

	printk("e820: BIOS memory map:");
	do
	{
		char *map_addr;

		if (!map)
			return -1;

		memset(&buf, 0, sizeof(buf));
		vm86_write_segment(vm, VOIDPTR(0x20000), &buf, sizeof(buf));

		regs.eax.dw = 0xE820;
		/* Do not overwrite ebx: Next entry offset */
		regs.ecx.dw = sizeof(buf);
		regs.edx.dw = E820_SIG;
		regs.es = 0x2000;
		regs.edi.dw = 0;
		regs.esp.dw = 0x7C00;
		regs.eip = 0x7C00;
		regs.eflags &= ~VM86_EFLAGS_CF;

		vm86_set_regs(vm, &regs);
		vm86_exec(vm, 10);
		vm86_get_regs(vm, &regs);

		if (regs.eflags & VM86_EFLAGS_CF
				|| regs.eax.dw != E820_SIG)
			break;

		vm86_read_segment(vm, &buf, VOIDPTR(0x20000), sizeof(buf));

		if (regs.ecx.dw < SIZEOF_UNTIL_MEMBER(struct e820_desc, v10))
			continue;

		if (!buf.v10.length)
			continue;

		map->address = VOIDPTR(buf.v10.address);
		map->length = buf.v10.length;
		map->type = buf.v10.type;
		e820_map_nbentries++;

		map_addr = map->address;

		printk("e820:  %p - %p, 0x%x (%s)", map_addr, &map_addr[map->length - 1], map->type, E820_TYPETOSTR(map->type));

		if (regs.ebx.dw)
		{
			map->next = kmalloc(sizeof(*map->next));
			map = map->next;
		}
		else
			map->next = NULL;
	}
	while(regs.ebx.dw);

	vm86_destroy(&vm);

	return 0;
}

memory_map_t *arch_memory_map_get(void)
{
	return e820_map;
}

int arch_memory_map_update(void *address, uint64_t length, uint8_t type)
{
	char *addr = address;
	memory_map_t *map;

	if (!length)
		return -1;

	map = e820_map;
	for(int i = 0; i < e820_map_nbentries; i++)
	{
		char *map_addr = map->address;

		if(map_addr <= addr && addr < &map_addr[map->length] && &addr[length] < &map_addr[map->length])
		{

		}
		else if(map_addr <= addr && addr < &map_addr[map->length] && &map_addr[map->length] <= &addr[length])
		{
			map->length = (uintptr_t)addr - (uintptr_t)map->address;
		}
	}

	printk("e820: update %p - %p, 0x%x (%s)", addr, &addr[length - 1], type, E820_TYPETOSTR(type));

	return 0;
}

int arch_memory_map_get_length(void)
{
	return e820_map_nbentries;
}
