/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "asm/io.h"
#include "common/string.h"
#include "common/printk.h"
#include "arch/mmu.h"

#include "config.h"

#define WIDTH  80
#define HEIGHT 25
#define BPCHAR 2

static uint16_t screen_buf[HEIGHT][WIDTH];
static void *mapped_screen = NULL;

static int8_t x = 0;
static int8_t y = 0;

static void vga_update_cursor(int8_t x, int8_t y)
{
	uint16_t pos = x + (y * WIDTH);

	/* Allows the cursor to be visible */
	screen_buf[y][x] |= 0x0F00;

	_outb(0x3D4, 0x0F);
	_outb(0x3D5, (uint8_t)(pos & 0xFF));

	_outb(0x3D4, 0x0E);
	_outb(0x3D5, (uint8_t)(pos >> 8));
}

static void vga_flush(void)
{
	memcpy(mapped_screen, screen_buf, sizeof(screen_buf));
}

static void vga_blank(void)
{
	x = 0;
	y = 0;

	memset(screen_buf, 0, sizeof(screen_buf));
	vga_update_cursor(x, y);

	vga_flush();
}

static void vga_scrollup(int8_t n)
{
	if (!y)
		return;

	if (n >= HEIGHT)
	{
		vga_blank();
		return;
	}

	y--;
	memcpy(screen_buf, screen_buf[n], sizeof(screen_buf) - n * WIDTH * BPCHAR);
	memset(screen_buf[HEIGHT - n], 0, n * WIDTH * BPCHAR);

	vga_update_cursor(x, y);
}

static void vga_newline(void)
{
	x = 0;
	y++;

#if CONFIG_BOCHS_E9_HACK

	_outb(0xE9, '\n');

#endif

	if (y >= HEIGHT)
		vga_scrollup(1);

	vga_update_cursor(x, y);
}

static void vga_cr(void)
{
	x = 0;

#if CONFIG_BOCHS_E9_HACK

	_outb(0xE9, '\r');

#endif

	vga_update_cursor(x, y);
}

static void vga_print(const char *str)
{
	size_t len;

	len = strlen(str);

	if (!len)
		return;

	for (size_t i = 0; i < len; i++)
	{
		switch (str[i])
		{
			case '\n':
				vga_newline();
				continue;

			case '\r':
				vga_cr();
				continue;

			case '\t':
				vga_print("    ");
				continue;
		}

		if (x >= WIDTH)
		{
			x = 0;
			y++;
		}

		if (y >= HEIGHT)
			vga_scrollup(1);

		screen_buf[y][x] = str[i] | 0x0F00; /* Black & White */

#if CONFIG_BOCHS_E9_HACK

		_outb(0xE9, str[i]);

#endif

		x++;
	}
}

static void vga_print_flush(const char *str)
{
	vga_print(str);
	vga_newline();
	vga_flush();
}

int arch_vga_setup(void)
{
	mapped_screen = arch_map_phys((void *)0xB8000, 0x8000);
	vga_blank();
	printk_register_func(vga_print_flush);
	return 0;
}

