/*
 * Copyright (C) 2016-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <asm/io.h>
#include <common/kmalloc.h>
#include <common/printk.h>
#include <common/string.h>
#include <os/types.h>

#include "vm86.h"

#define SETF(d, f) do { (d) |= (f); } while (0)
#define UNSETF(d, f) do { (d) &= ~(f); } while (0)
#define SETUNSETF(cond, d, f) \
	do \
	{ \
		if (cond) \
			SETF((d), (f)); \
		else \
			UNSETF((d), (f)); \
	} while(0)

#define RL(reg) VM86_RL(reg)
#define RH(reg) VM86_RH(reg)
#define RX(reg) VM86_RX(reg)
#define RE(reg) VM86_RE(reg)

#define IVT_MAX_ENTRIES 0x100

struct vm86_seg
{
	enum vm86_prot_mask prot;
	volatile void *data;
};

struct vm86_ctx
{
	struct vm86_regs regs;
	struct vm86_seg segs[VM86_SEG_COUNT];
	void *mem;
	int shutdown;
	unsigned int trace : 1;
};

struct vm86_drv_ctx
{
	uint32_t ivt[IVT_MAX_ENTRIES];
	size_t ivt_nb_entries;
};

static struct vm86_drv_ctx drv_ctx;

int vm86_setup(void)
{
	drv_ctx.ivt_nb_entries = 0;
	return 0;
}

int vm86_import_native_ivt(void *ivt, size_t nb_entries)
{
	if (!nb_entries)
	{
		drv_ctx.ivt_nb_entries = 0;
		return 0;
	}

	if (nb_entries > IVT_MAX_ENTRIES)
		return -1;

	memcpy(drv_ctx.ivt, ivt, nb_entries * sizeof(drv_ctx.ivt[0]));
	drv_ctx.ivt_nb_entries = nb_entries;
	return 0;
}

int vm86_create(struct vm86_ctx **vm86, int use_native_ivt)
{
	struct vm86_ctx *lvm86;

	if (!vm86 || *vm86)
		return -1;

	if (use_native_ivt && !drv_ctx.ivt_nb_entries)
		return -1;

	*vm86 = kmalloc(sizeof(**vm86));
	if (!*vm86)
		return -1;
	lvm86 = *vm86;

	lvm86->mem = kmalloc(VM86_MEM_SIZE);
	lvm86->shutdown = 0;
	lvm86->trace = 0;

	vm86_reset_regs(lvm86);
	vm86_unmap_segment(lvm86, VOIDPTR(0), VM86_MEM_SIZE);

	if (use_native_ivt && drv_ctx.ivt_nb_entries)
		vm86_write_segment(lvm86, VOIDPTR(0), drv_ctx.ivt, drv_ctx.ivt_nb_entries * sizeof(drv_ctx.ivt[0]));

	return 0;
}

int vm86_destroy(struct vm86_ctx **vm86)
{
	struct vm86_ctx *lvm86;

	if (!vm86 || !*vm86)
		return -1;
	lvm86 = *vm86;

	kfree(lvm86->mem);

	kfree(lvm86);
	*vm86 = NULL;
	return 0;
}

int vm86_set_trace(struct vm86_ctx *vm86, int trace)
{
	vm86->trace = !!trace;
	return 0;
}

static inline uint32_t vm86_seg_idx(uint32_t phys_addr) {
	return phys_addr / VM86_SEG_SIZE;
}

static inline uint32_t vm86_seg_offset(uint32_t phys_addr) {
	return phys_addr % VM86_SEG_SIZE;
}

static inline uint32_t vm86_seg_count(uint32_t len) {
	return len / VM86_SEG_SIZE;
}

int vm86_map_segment(struct vm86_ctx *vm86, void *phys_addr, enum vm86_prot_mask prot, void *data, uint32_t len)
{
	uintptr_t p = (uintptr_t)phys_addr;
	uintptr_t d = (uintptr_t)data;

	if (!vm86 || !d || !len || p >= VM86_MEM_SIZE || len > VM86_MEM_SIZE || p + len > VM86_MEM_SIZE)
		return -1;

	if (p & ~VM86_SEG_MASK || len & ~VM86_SEG_MASK)
		return -1;

	uint32_t idx = vm86_seg_idx(p);
	uint32_t cnt = vm86_seg_count(len);

	struct vm86_seg *seg = &vm86->segs[idx];
	struct vm86_seg *seg_lim = &seg[cnt];

	while (seg < seg_lim)
	{
		seg->prot = prot;
		seg->data = VOIDPTR(d);

		seg++;
		d += VM86_SEG_SIZE;
	}

	return 0;
}

int vm86_map_bios_bda(struct vm86_ctx *vm86)
{
	return vm86_map_segment(vm86, VOIDPTR(0x400), VM86_PROT_READ | VM86_PROT_WRITE, VOIDPTR(0x400), 0x100);
}

int vm86_map_bios_ebda(struct vm86_ctx *vm86)
{
	return vm86_map_segment(vm86, VOIDPTR(0x80000), VM86_PROT_READ | VM86_PROT_WRITE, VOIDPTR(0x80000), 0x20000);
}

int vm86_map_upper_mem(struct vm86_ctx *vm86)
{
	return vm86_map_segment(vm86, VOIDPTR(0xA0000), VM86_PROT_READ | VM86_PROT_WRITE, VOIDPTR(0xA0000), 0x60000);
}

int vm86_unmap_segment(struct vm86_ctx *vm86, void *phys_addr, uint32_t len)
{
	uintptr_t mem = (uintptr_t)vm86->mem;
	uintptr_t p = (uintptr_t)phys_addr;

	return vm86_map_segment(vm86, phys_addr, VM86_PROT_READ | VM86_PROT_WRITE | VM86_PROT_EXEC, VOIDPTR(mem + p), len);
}

int vm86_write_segment(struct vm86_ctx *vm86, void *phys_addr, const void *data, uint32_t len)
{
	uintptr_t p = (uintptr_t)phys_addr;
	char *mem;

	if (!vm86 || !data || !len || p + len > VM86_MEM_SIZE)
		return -1;

	mem = vm86->mem;
	memcpy(&mem[p], data, len);
	return 0;
}

int vm86_read_segment(struct vm86_ctx *vm86, void *data, void *phys_addr, uint32_t len)
{
	uintptr_t p = (uintptr_t)phys_addr;
	char *mem;

	if (!vm86 || !data || !len || p + len > VM86_MEM_SIZE)
		return -1;

	mem = vm86->mem;
	memcpy(data, &mem[p], len);
	return 0;
}

int vm86_read_string(struct vm86_ctx *vm86, void *data, void *phys_addr, uint32_t max_size)
{
	uintptr_t p = (uintptr_t)phys_addr;
	char *mem;
	size_t len;

	if (!vm86 || !data || !max_size || p > VM86_MEM_SIZE - 1)
		return -1;

	if (p + max_size > VM86_MEM_SIZE)
		max_size = VM86_MEM_SIZE - p;

	mem = vm86->mem;
	len = strnlen(&mem[p], max_size - 1);
	memcpy(data, &mem[p], len);
	mem[p + len] = 0;
	return 0;
}

int vm86_reset_regs(struct vm86_ctx *vm86)
{
	if (!vm86)
		return -1;

	memset(&vm86->regs, 0, sizeof(vm86->regs));
	vm86_reset_eflags(vm86);
	return 0;
}

int vm86_reset_eflags(struct vm86_ctx *vm86)
{
	if (!vm86)
		return -1;

	vm86->regs.eflags = 0x02 | VM86_EFLAGS_IF;

	return 0;
}

int vm86_set_regs(struct vm86_ctx *vm86, struct vm86_regs *regs)
{
	if (!vm86 || !regs)
		return -1;

	if (regs->mask & VM86_REG_EAX)
		vm86->regs.eax = regs->eax;
	if (regs->mask & VM86_REG_EBX)
		vm86->regs.ebx = regs->ebx;
	if (regs->mask & VM86_REG_ECX)
		vm86->regs.ecx = regs->ecx;
	if (regs->mask & VM86_REG_EDX)
		vm86->regs.edx = regs->edx;
	if (regs->mask & VM86_REG_EDI)
		vm86->regs.edi = regs->edi;
	if (regs->mask & VM86_REG_ESI)
		vm86->regs.esi = regs->esi;
	if (regs->mask & VM86_REG_EBP)
		vm86->regs.ebp = regs->ebp;
	if (regs->mask & VM86_REG_ESP)
		vm86->regs.esp = regs->esp;
	if (regs->mask & VM86_REG_EIP)
		vm86->regs.eip = regs->eip;
	if (regs->mask & VM86_REG_EFLAGS)
		vm86->regs.eflags = regs->eflags;
	if (regs->mask & VM86_REG_CS)
		vm86->regs.cs = regs->cs;
	if (regs->mask & VM86_REG_DS)
		vm86->regs.ds = regs->ds;
	if (regs->mask & VM86_REG_ES)
		vm86->regs.es = regs->es;
	if (regs->mask & VM86_REG_FS)
		vm86->regs.fs = regs->fs;
	if (regs->mask & VM86_REG_GS)
		vm86->regs.gs = regs->gs;
	if (regs->mask & VM86_REG_SS)
		vm86->regs.ss = regs->ss;
	return 0;
}

int vm86_get_regs(struct vm86_ctx *vm86, struct vm86_regs *regs)
{
	if (!vm86 || !regs)
		return -1;

	if (regs->mask & VM86_REG_EAX)
		regs->eax = vm86->regs.eax;
	if (regs->mask & VM86_REG_EBX)
		regs->ebx = vm86->regs.ebx;
	if (regs->mask & VM86_REG_ECX)
		regs->ecx = vm86->regs.ecx;
	if (regs->mask & VM86_REG_EDX)
		regs->edx = vm86->regs.edx;
	if (regs->mask & VM86_REG_EDI)
		regs->edi = vm86->regs.edi;
	if (regs->mask & VM86_REG_ESI)
		regs->esi = vm86->regs.esi;
	if (regs->mask & VM86_REG_EBP)
		regs->ebp = vm86->regs.ebp;
	if (regs->mask & VM86_REG_ESP)
		regs->esp = vm86->regs.esp;
	if (regs->mask & VM86_REG_EIP)
		regs->eip = vm86->regs.eip;
	if (regs->mask & VM86_REG_EFLAGS)
		regs->eflags = vm86->regs.eflags;
	if (regs->mask & VM86_REG_CS)
		regs->cs = vm86->regs.cs;
	if (regs->mask & VM86_REG_DS)
		regs->ds = vm86->regs.ds;
	if (regs->mask & VM86_REG_ES)
		regs->es = vm86->regs.es;
	if (regs->mask & VM86_REG_FS)
		regs->fs = vm86->regs.fs;
	if (regs->mask & VM86_REG_GS)
		regs->gs = vm86->regs.gs;
	if (regs->mask & VM86_REG_SS)
		regs->ss = vm86->regs.ss;
	return 0;
}

int vm86_clear_regs(struct vm86_ctx *vm86)
{
	if (!vm86)
		return -1;

	memset(&vm86->regs, 0, sizeof(vm86->regs));
	return 0;
}

static int vm86_compute_segs(struct vm86_ctx *vm86)
{
	(void)vm86;
	return 0;
}

static inline uint32_t realaddr_seg_off_to_phys(uint32_t seg, uint32_t off) {
	return ((seg & 0xFFFF) << 4) + (off & 0xFFFF);
}

static inline uint32_t realaddr16_to_phys(uint32_t addr) {
	return realaddr_seg_off_to_phys(addr >> 16, addr & 0xFFFF);
}

static inline uint32_t realaddr32_to_phys(uint64_t addr) {
	return realaddr_seg_off_to_phys(addr >> 32, addr & 0xFFFFFFFF);
}

static inline uint32_t realaddr_seg_off_to_realaddr16(uint32_t seg, uint32_t off) {
	return (seg << 16) | (off & 0xFFFF);
}

static inline uint64_t realaddr_seg_off_to_realaddr32(uint32_t seg, uint32_t off) {
	return ((uint64_t)seg << 32) | off;
}

static inline uint32_t ctx_get_ip_off(struct vm86_ctx *vm86, uint32_t off) {
	struct vm86_regs *regs = &vm86->regs;
	return realaddr_seg_off_to_phys(regs->cs, regs->eip + off);
}

static inline uint32_t ctx_get_ip(struct vm86_ctx *vm86) {
	return ctx_get_ip_off(vm86, 0);
}

static inline uint32_t ctx_get_sp_off(struct vm86_ctx *vm86, uint32_t off) {
	struct vm86_regs *regs = &vm86->regs;
	return realaddr_seg_off_to_phys(regs->ss, RE(regs->esp) + off);
}

static inline uint32_t ctx_get_sp(struct vm86_ctx *vm86) {
	return ctx_get_sp_off(vm86, 0);
}

static inline uint8_t read_byte_unchecked(volatile void *data, uint32_t off) {
	volatile uint8_t *p = VOIDPTR((uintptr_t)data + off);
	return *p;
}

static inline uint16_t read_word_unchecked(volatile void *data, uint32_t off) {
	volatile uint16_t *p = VOIDPTR((uintptr_t)data + off);
	return *p;
}

static inline uint32_t read_dword_unchecked(volatile void *data, uint32_t off) {
	volatile uint32_t *p = VOIDPTR((uintptr_t)data + off);
	return *p;
}

static inline void write_byte_unchecked(volatile void *data, uint32_t off, uint8_t val) {
	volatile uint8_t *p = VOIDPTR((uintptr_t)data + off);
	*p = val;
}

static inline void write_word_unchecked(volatile void *data, uint32_t off, uint16_t val) {
	volatile uint16_t *p = VOIDPTR((uintptr_t)data + off);
	*p = val;
}

static inline void write_dword_unchecked(volatile void *data, uint32_t off, uint32_t val) {
	volatile uint32_t *p = VOIDPTR((uintptr_t)data + off);
	*p = val;
}

static inline uint8_t read_mem_byte(struct vm86_ctx *vm86, uint32_t idx) {
	if (vm86->trace)
		printk("READ8[0x%x]", idx);

	if (idx >= VM86_MEM_SIZE)
		return 0;

	uint16_t seg_idx = vm86_seg_idx(idx);
	uint16_t seg_off = vm86_seg_offset(idx);

	struct vm86_seg *seg = &vm86->segs[seg_idx];
	if (!(seg->prot & VM86_PROT_READ))
		return 0;

	return read_byte_unchecked(seg->data, seg_off);
}

static inline uint16_t read_mem_word(struct vm86_ctx *vm86, uint32_t idx) {
	if (vm86->trace)
		printk("READ16[0x%x]", idx);

	if (idx >= VM86_MEM_SIZE || idx + 1 >= VM86_MEM_SIZE)
		return 0;

	uint16_t seg0_idx = vm86_seg_idx(idx);
	uint16_t seg0_off = vm86_seg_offset(idx);

	struct vm86_seg *seg0 = &vm86->segs[seg0_idx];
	if (!(seg0->prot & VM86_PROT_READ))
		return 0;

	uint16_t seg1_idx = vm86_seg_idx(idx + 1);
	if (seg0_idx == seg1_idx)
		return read_word_unchecked(seg0->data, seg0_off);

	struct vm86_seg *seg1 = &vm86->segs[seg1_idx];
	if (!(seg1->prot & VM86_PROT_READ))
		return 0;

	/* If the two segments are contiguous */
	if ((uintptr_t)seg0->data + VM86_SEG_SIZE == (uintptr_t)seg1->data)
		return read_word_unchecked(seg0->data, seg0_off);

	uint16_t seg1_off = vm86_seg_offset(idx + 1);

	return read_byte_unchecked(seg0->data, seg0_off)
		| ((uint16_t)read_byte_unchecked(seg1->data, seg1_off) << 8);
}

static inline uint32_t read_mem_dword(struct vm86_ctx *vm86, uint32_t idx) {
	if (vm86->trace)
		printk("READ32[0x%x]", idx);

	if (idx >= VM86_MEM_SIZE || idx + 3 >= VM86_MEM_SIZE)
		return 0;

	uint16_t seg0_idx = vm86_seg_idx(idx);
	uint16_t seg0_off = vm86_seg_offset(idx);

	struct vm86_seg *seg0 = &vm86->segs[seg0_idx];
	if (!(seg0->prot & VM86_PROT_READ))
		return 0;

	uint16_t seg3_idx = vm86_seg_idx(idx + 3);
	if (seg0_idx == seg3_idx)
		return read_dword_unchecked(seg0->data, seg0_off);

	struct vm86_seg *seg3 = &vm86->segs[seg3_idx];
	if (!(seg3->prot & VM86_PROT_READ))
		return 0;

	/* If the two segments are contiguous */
	if ((uintptr_t)seg0->data + VM86_SEG_SIZE == (uintptr_t)seg3->data)
		return read_dword_unchecked(seg0->data, seg0_off);

	uint16_t seg1_idx = vm86_seg_idx(idx + 1);
	uint16_t seg1_off = vm86_seg_offset(idx + 1);
	uint16_t seg2_idx = vm86_seg_idx(idx + 2);
	uint16_t seg2_off = vm86_seg_offset(idx + 2);
	uint16_t seg3_off = vm86_seg_offset(idx + 3);

	struct vm86_seg *seg1 = &vm86->segs[seg1_idx];
	struct vm86_seg *seg2 = &vm86->segs[seg2_idx];

	return read_byte_unchecked(seg0->data, seg0_off)
		| ((uint32_t)read_byte_unchecked(seg1->data, seg1_off) << 8)
		| ((uint32_t)read_byte_unchecked(seg2->data, seg2_off) << 16)
		| ((uint32_t)read_byte_unchecked(seg3->data, seg3_off) << 24);
}

static inline union vm86_gpr read_mem(struct vm86_ctx *vm86, uint32_t idx, int op_size) {
	union vm86_gpr result;

	RE(result) = 0;

	if (op_size == 4)
		RE(result) = read_mem_dword(vm86, idx);
	else
		RX(result) = read_mem_word(vm86, idx);

	return result;
}

static inline uint8_t read_mem_byte_silent(struct vm86_ctx *vm86, uint32_t idx) {
	unsigned int trace = vm86->trace;
	vm86->trace = 0;
	uint8_t val = read_mem_byte(vm86, idx);
	vm86->trace = trace;
	return val;
}

static inline uint32_t read_mem_dword_silent(struct vm86_ctx *vm86, uint32_t idx) {
	unsigned int trace = vm86->trace;
	vm86->trace = 0;
	uint32_t val = read_mem_dword(vm86, idx);
	vm86->trace = trace;
	return val;
}

static inline void write_mem_byte(struct vm86_ctx *vm86, uint32_t idx, uint8_t val) {
	if (vm86->trace)
		printk("WRITE8[0x%x] = 0x%x", idx, val);

	if (idx >= VM86_MEM_SIZE)
		return;

	uint16_t seg_idx = vm86_seg_idx(idx);
	uint16_t seg_off = vm86_seg_offset(idx);

	struct vm86_seg *seg = &vm86->segs[seg_idx];
	if (!(seg->prot & VM86_PROT_WRITE))
		return;

	write_byte_unchecked(seg->data, seg_off, val);
}

static inline void write_mem_word(struct vm86_ctx *vm86, uint32_t idx, uint16_t val) {
	if (vm86->trace)
		printk("WRITE16[0x%x] = 0x%x", idx, val);

	if (idx >= VM86_MEM_SIZE || idx + 1 >= VM86_MEM_SIZE)
		return;

	uint16_t seg0_idx = vm86_seg_idx(idx);
	uint16_t seg0_off = vm86_seg_offset(idx);

	struct vm86_seg *seg0 = &vm86->segs[seg0_idx];
	if (!(seg0->prot & VM86_PROT_WRITE))
		return;

	uint16_t seg1_idx = vm86_seg_idx(idx + 1);
	if (seg0_idx == seg1_idx)
	{
		write_word_unchecked(seg0->data, seg0_off, val);
		return;
	}

	struct vm86_seg *seg1 = &vm86->segs[seg1_idx];
	if (!(seg1->prot & VM86_PROT_WRITE))
		return;

	/* If the two segments are contiguous */
	if ((uintptr_t)seg0->data + VM86_SEG_SIZE == (uintptr_t)seg1->data)
	{
		write_word_unchecked(seg0->data, seg0_off, val);
		return;
	}

	uint16_t seg1_off = vm86_seg_offset(idx + 1);

	write_byte_unchecked(seg0->data, seg0_off, val);
	write_byte_unchecked(seg1->data, seg1_off, val >> 8);
}

static inline void write_mem_dword(struct vm86_ctx *vm86, uint32_t idx, uint32_t val) {
	if (vm86->trace)
		printk("WRITE32[0x%x] = 0x%x", idx, val);

	if (idx >= VM86_MEM_SIZE || idx + 3 >= VM86_MEM_SIZE)
		return;

	uint16_t seg0_idx = vm86_seg_idx(idx);
	uint16_t seg0_off = vm86_seg_offset(idx);

	struct vm86_seg *seg0 = &vm86->segs[seg0_idx];
	if (!(seg0->prot & VM86_PROT_WRITE))
		return;

	uint16_t seg3_idx = vm86_seg_idx(idx + 3);
	if (seg0_idx == seg3_idx)
	{
		write_dword_unchecked(seg0->data, seg0_off, val);
		return;
	}

	struct vm86_seg *seg3 = &vm86->segs[seg3_idx];
	if (!(seg3->prot & VM86_PROT_WRITE))
		return;

	/* If the two segments are contiguous */
	if ((uintptr_t)seg0->data + VM86_SEG_SIZE == (uintptr_t)seg3->data)
	{
		write_dword_unchecked(seg0->data, seg0_off, val);
		return;
	}

	uint16_t seg1_idx = vm86_seg_idx(idx + 1);
	uint16_t seg1_off = vm86_seg_offset(idx + 1);
	uint16_t seg2_idx = vm86_seg_idx(idx + 2);
	uint16_t seg2_off = vm86_seg_offset(idx + 2);
	uint16_t seg3_off = vm86_seg_offset(idx + 3);

	struct vm86_seg *seg1 = &vm86->segs[seg1_idx];
	struct vm86_seg *seg2 = &vm86->segs[seg2_idx];

	write_byte_unchecked(seg0->data, seg0_off, val);
	write_byte_unchecked(seg1->data, seg1_off, val >> 8);
	write_byte_unchecked(seg2->data, seg2_off, val >> 16);
	write_byte_unchecked(seg3->data, seg3_off, val >> 24);
}

static inline void write_mem(struct vm86_ctx *vm86, uint32_t idx, int op_size, union vm86_gpr val) {
	if (op_size == 4)
		write_mem_dword(vm86, idx, RE(val));
	else
		write_mem_word(vm86, idx, RX(val));
}

static inline void push_word(struct vm86_ctx *vm86, uint16_t val) {
	RE(vm86->regs.esp) -= 2;
	write_mem_word(vm86, ctx_get_sp(vm86), val);
}

static inline void push_dword(struct vm86_ctx *vm86, uint32_t val) {
	RE(vm86->regs.esp) -= 4;
	write_mem_dword(vm86, ctx_get_sp(vm86), val);
}

static inline void push(struct vm86_ctx *vm86, int op_size, union vm86_gpr val) {
	if (op_size == 4)
		push_dword(vm86, RE(val));
	else
		push_word(vm86, RX(val));
}

static inline uint16_t pop_word(struct vm86_ctx *vm86) {
	const uint16_t val = read_mem_word(vm86, ctx_get_sp(vm86));
	RE(vm86->regs.esp) += 2;
	return val;
}

static inline uint32_t pop_dword(struct vm86_ctx *vm86) {
	const uint32_t val = read_mem_dword(vm86, ctx_get_sp(vm86));
	RE(vm86->regs.esp) += 4;
	return val;
}

static inline union vm86_gpr pop(struct vm86_ctx *vm86, int op_size) {
	union vm86_gpr result;

	RE(result) = 0;

	if (op_size == 4)
		RE(result) = pop_dword(vm86);
	else
		RX(result) = pop_word(vm86);

	return result;
}

static inline uint16_t mb8_to_word(uint8_t high, uint8_t low) {
	return ((uint16_t)high << 8) | low;
}

static inline uint32_t mb16_to_dword(uint16_t high, uint16_t low) {
	return ((uint32_t)high << 16) | low;
}

static inline uint64_t mb32_to_qword(uint32_t high, uint32_t low) {
	return ((uint64_t)high << 32) | low;
}

static void dump_regs(struct vm86_ctx *vm86)
{
	struct vm86_regs *regs = &vm86->regs;

	printk("vm86: [BEGIN REGS DUMP]\n"
		"\teax = %x\n"
		"\tebx = %x\n"
		"\tecx = %x\n"
		"\tedx = %x\n"
		"\tedi = %x\n"
		"\tesi = %x\n"
		"\tebp = %x\n"
		"\tesp = %x\n"
		"\teip = %x\n"
		"\teflags = %x\n"
		"\tcs = %x\n"
		"\tds = %x\n"
		"\tes = %x\n"
		"\tfs = %x\n"
		"\tgs = %x\n"
		"\tss = %x\n"
		"[END REGS DUMP]",
		RE(regs->eax), RE(regs->ebx), RE(regs->ecx), RE(regs->edx),
		RE(regs->edi), RE(regs->esi), RE(regs->ebp), RE(regs->esp),
		regs->eip, regs->eflags, regs->cs, regs->ds,
		regs->es, regs->fs, regs->gs, regs->ss);
}

static inline void faulty_shutdown(struct vm86_ctx *vm86, const char *msg) {
	printk("vm86: VM %p crashed: %s, shutting down.", vm86, msg);
	dump_regs(vm86);
	vm86->shutdown = 1;
}

static inline void throw_int(struct vm86_ctx *vm86, uint8_t num) {
	push_word(vm86, vm86->regs.eflags & 0xFFFF);
	push_word(vm86, vm86->regs.cs);
	push_word(vm86, vm86->regs.eip & 0xFFFF);

	vm86->regs.cs = drv_ctx.ivt[num] >> 16;
	vm86->regs.eip = drv_ctx.ivt[num] & 0xFFFF;
	UNSETF(vm86->regs.eflags, VM86_EFLAGS_TF | VM86_EFLAGS_IF | VM86_EFLAGS_AC);
}

static void trigger_excp(struct vm86_ctx *vm86, uint8_t num, uint16_t err_code);

static void trigger_int(struct vm86_ctx *vm86, uint8_t num)
{
	/* 1) Sanity checks */

	if (num >= drv_ctx.ivt_nb_entries)
	{
		trigger_excp(vm86, VM86_EXCP_GP, 0);
		return;
	}

	if (RE(vm86->regs.esp) < 2 + 2 + 2)
	{
		trigger_excp(vm86, VM86_EXCP_SS, 0);
		return;
	}

	/* 2) Throw the interrupt */

	throw_int(vm86, num);
}

static void trigger_excp(struct vm86_ctx *vm86, uint8_t num, uint16_t err_code)
{
	int write_err_code;

	/* It was not an exception ?! */
	if (num > VM86_EXCP_RES31)
	{
		trigger_int(vm86, num);
		return;
	}

	/* 1) Sanity checks */

	write_err_code = 0;

	switch (num)
	{
		case VM86_EXCP_DF:
		case VM86_EXCP_TS:
		case VM86_EXCP_NP:
		case VM86_EXCP_SS:
		case VM86_EXCP_GP:
		case VM86_EXCP_PF:
		case VM86_EXCP_AC:
		case VM86_EXCP_SX:
			write_err_code = 1;
	}

	if (num >= drv_ctx.ivt_nb_entries)
	{
		if (num == VM86_EXCP_GP)
		{
			faulty_shutdown(vm86, "vm86: The GPF vector is out of the IVT");
			return;
		}
		trigger_excp(vm86, VM86_EXCP_GP, 0);
		return;
	}

	if (RE(vm86->regs.esp) < 2 + 2 + 2 + (write_err_code ? 2 : 0))
	{
		faulty_shutdown(vm86, "vm86: Not enough stack");
		return;
	}

	/* 2) Throw the interrupt */

	printk("vm86: VM %p: Exception %d thrown.", num);
	dump_regs(vm86);
	printk("Guilty instruction was at [cs:eip]: 0x%x 0x%x 0x%x 0x%x",
			read_mem_dword(vm86, ctx_get_ip_off(vm86, 0)),
			read_mem_dword(vm86, ctx_get_ip_off(vm86, 4)),
			read_mem_dword(vm86, ctx_get_ip_off(vm86, 8)),
			read_mem_dword(vm86, ctx_get_ip_off(vm86, 12)));

	throw_int(vm86, num);

	if (write_err_code)
		push_word(vm86, err_code);
}

static inline int chk_throw_excp_de(struct vm86_ctx *vm86, uint32_t val) {
	if (!val)
	{
		trigger_excp(vm86, VM86_EXCP_DE, 0);
		return 1;
	}
	return 0;
}

static inline void update_eflags_szp_u8(struct vm86_ctx *vm86, uint8_t val) {
	SETUNSETF(val & 0x80, vm86->regs.eflags, VM86_EFLAGS_SF);
	SETUNSETF(!val, vm86->regs.eflags, VM86_EFLAGS_ZF);
	val ^= (val >> 4);
	val ^= (val >> 2);
	val ^= (val >> 1);
	SETUNSETF(!(val & 1), vm86->regs.eflags, VM86_EFLAGS_PF);
}

static inline void update_eflags_szp_u16(struct vm86_ctx *vm86, uint16_t val) {
	SETUNSETF(val & 0x8000, vm86->regs.eflags, VM86_EFLAGS_SF);
	SETUNSETF(!val, vm86->regs.eflags, VM86_EFLAGS_ZF);
	val ^= (val >> 4);
	val ^= (val >> 2);
	val ^= (val >> 1);
	SETUNSETF(!(val & 1), vm86->regs.eflags, VM86_EFLAGS_PF);
}

static inline void update_eflags_szp_u32(struct vm86_ctx *vm86, uint32_t val) {
	SETUNSETF(val & 0x80000000, vm86->regs.eflags, VM86_EFLAGS_SF);
	SETUNSETF(!val, vm86->regs.eflags, VM86_EFLAGS_ZF);
	val ^= (val >> 4);
	val ^= (val >> 2);
	val ^= (val >> 1);
	SETUNSETF(!(val & 1), vm86->regs.eflags, VM86_EFLAGS_PF);
}

static inline void update_eflags_szp(struct vm86_ctx *vm86, int op_size, union vm86_gpr val) {
	if (op_size == 4)
		update_eflags_szp_u32(vm86, RE(val));
	else
		update_eflags_szp_u16(vm86, RX(val));
}

static inline void update_eflags_ac_add(struct vm86_ctx *vm86, uint32_t val, uint32_t a1, uint32_t a2) {
	uint8_t af_a1 = a1 & 0xF;
	uint8_t af_a2 = a2 & 0xF;
	uint8_t af_val = af_a1 + af_a2;

	SETUNSETF(af_val > 0xF, vm86->regs.eflags, VM86_EFLAGS_AF);
	SETUNSETF(val < a1 || val < a2, vm86->regs.eflags, VM86_EFLAGS_CF);
}

/* Overflow if we get a sign inversion:
 *    - from below: - add - => +
 *    - from above: + add + => -
 */
static inline void update_eflags_oac_add_u8(struct vm86_ctx *vm86, uint8_t val, uint8_t a1, uint8_t a2) {
	SETUNSETF(((a1 ^ val) & (a2 ^ val)) & 0x80u, vm86->regs.eflags, VM86_EFLAGS_OF);
	update_eflags_ac_add(vm86, val, a1, a2);
}

static inline void update_eflags_oac_add_u16(struct vm86_ctx *vm86, uint16_t val, uint16_t a1, uint16_t a2) {
	SETUNSETF(((a1 ^ val) & (a2 ^ val)) & 0x8000u, vm86->regs.eflags, VM86_EFLAGS_OF);
	update_eflags_ac_add(vm86, val, a1, a2);
}

static inline void update_eflags_oac_add_u32(struct vm86_ctx *vm86, uint32_t val, uint32_t a1, uint32_t a2) {
	SETUNSETF(((a1 ^ val) & (a2 ^ val)) & 0x80000000u, vm86->regs.eflags, VM86_EFLAGS_OF);
	update_eflags_ac_add(vm86, val, a1, a2);
}

static inline void update_eflags_oac_add(struct vm86_ctx *vm86, int op_size, union vm86_gpr val, union vm86_gpr a1, union vm86_gpr a2) {
	if (op_size == 4)
		update_eflags_oac_add_u32(vm86, RE(val), RE(a1), RE(a2));
	else
		update_eflags_oac_add_u16(vm86, RX(val), RX(a1), RX(a2));
}

static inline void update_eflags_ac_sub(struct vm86_ctx *vm86, uint32_t val, uint32_t a1, uint32_t a2) {
	uint8_t af_a1 = a1 & 0xF;
	uint8_t af_a2 = a2 & 0xF;

	(void)val;
	SETUNSETF(a1 < a2, vm86->regs.eflags, VM86_EFLAGS_CF);
	SETUNSETF(af_a1 < af_a2, vm86->regs.eflags, VM86_EFLAGS_AF);
}

/* Overflow if we get a sign inversion:
 *    - from below: - sub + => +
 *    - from above: + sub - => -
 */
static inline void update_eflags_oac_sub_u8(struct vm86_ctx *vm86, uint8_t val, uint8_t a1, uint8_t a2) {
	SETUNSETF((((a1 ^ a2) & (a1 ^ val)) >> 7) & 1, vm86->regs.eflags, VM86_EFLAGS_OF);
	update_eflags_ac_sub(vm86, val, a1, a2);
}

static inline void update_eflags_oac_sub_u16(struct vm86_ctx *vm86, uint16_t val, uint16_t a1, uint16_t a2) {
	SETUNSETF((((a1 ^ a2) & (a1 ^ val)) >> 15) & 1, vm86->regs.eflags, VM86_EFLAGS_OF);
	update_eflags_ac_sub(vm86, val, a1, a2);
}

static inline void update_eflags_oac_sub_u32(struct vm86_ctx *vm86, uint32_t val, uint32_t a1, uint32_t a2) {
	SETUNSETF((((a1 ^ a2) & (a1 ^ val)) >> 31) & 1, vm86->regs.eflags, VM86_EFLAGS_OF);
	update_eflags_ac_sub(vm86, val, a1, a2);
}

static inline void update_eflags_oac_sub(struct vm86_ctx *vm86, int op_size, union vm86_gpr val, union vm86_gpr a1, union vm86_gpr a2) {
	if (op_size == 4)
		update_eflags_oac_sub_u32(vm86, RE(val), RE(a1), RE(a2));
	else
		update_eflags_oac_sub_u16(vm86, RX(val), RX(a1), RX(a2));
}

static int check_cc(struct vm86_ctx *vm86, uint8_t cc)
{
	uint32_t eflags = vm86->regs.eflags;

	switch (cc & VM86_CC_MASK)
	{
	 	case VM86_CC_O:   return !!(eflags & VM86_EFLAGS_OF);
		case VM86_CC_NO:  return !(eflags & VM86_EFLAGS_OF);
		case VM86_CC_B:   return !!(eflags & VM86_EFLAGS_CF);
		case VM86_CC_NB:  return !(eflags & VM86_EFLAGS_CF);
		case VM86_CC_E:   return !!(eflags & VM86_EFLAGS_ZF);
		case VM86_CC_NE:  return !(eflags & VM86_EFLAGS_ZF);
		case VM86_CC_BE:  return !!(eflags & VM86_EFLAGS_CF) || !!(eflags & VM86_EFLAGS_ZF);
		case VM86_CC_NBE: return !(eflags & VM86_EFLAGS_CF) && !(eflags & VM86_EFLAGS_ZF);
		case VM86_CC_S:   return !!(eflags & VM86_EFLAGS_SF);
		case VM86_CC_NS:  return !(eflags & VM86_EFLAGS_SF);
		case VM86_CC_P:   return !!(eflags & VM86_EFLAGS_PF);
		case VM86_CC_NP:  return !(eflags & VM86_EFLAGS_PF);
		case VM86_CC_L:   return !(eflags & VM86_EFLAGS_SF) ^ !(eflags & VM86_EFLAGS_OF);
		case VM86_CC_NL:  return !(!(eflags & VM86_EFLAGS_SF) ^ !(eflags & VM86_EFLAGS_OF));
		case VM86_CC_LE:  return !!(eflags & VM86_EFLAGS_ZF) || (!(eflags & VM86_EFLAGS_SF) ^ !(eflags & VM86_EFLAGS_OF));
		case VM86_CC_NLE: return !(eflags & VM86_EFLAGS_ZF) && !(!(eflags & VM86_EFLAGS_SF) ^ !(eflags & VM86_EFLAGS_OF));
	}
	return 0;
}

static uint32_t get_arg_realaddr16_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *s, uint32_t idx, int reg_or_rm, int *bytes_read)
{
	struct vm86_regs *regs = &vm86->regs;
	uint16_t seg_ds = s ? *s : regs->ds;
	uint16_t seg_ss = s ? *s : regs->ss;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint16_t disp = 0;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
		return 0;

	switch (mod_reg_rm & VM86_OP_MOD_MASK)
	{
		case VM86_OP_MOD_NODISP_OR_PABS:
			if ((mod_reg_rm & VM86_OP_RM_MASK) == VM86_OP_RM_PBP_OR_PABS16)
			{
				if (bytes_read)
					*bytes_read += 2;
				return realaddr_seg_off_to_realaddr16(seg_ds, read_mem_word(vm86, idx + 1));
			}
			disp = 0;
			break;

		case VM86_OP_MOD_REL8:
			if (bytes_read)
				*bytes_read += 1;
			/* disp8 must be sign-extended to an int16_t */
			disp = (uint16_t)(int16_t)(int8_t)read_mem_byte(vm86, idx + 1);
			break;

		case VM86_OP_MOD_ABS:
			if (bytes_read)
				*bytes_read += 2;
			disp = read_mem_word(vm86, idx + 1);
			break;

		case VM86_OP_MOD_REG:
			return 0;
	}

	switch (mod_reg_rm & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_PBX_SI:
			return realaddr_seg_off_to_realaddr16(seg_ds, RX(regs->ebx) + RX(regs->esi) + disp);
		case VM86_OP_RM_PBX_DI:
			return realaddr_seg_off_to_realaddr16(seg_ds, RX(regs->ebx) + RX(regs->edi) + disp);
		case VM86_OP_RM_PBP_SI:
			return realaddr_seg_off_to_realaddr16(seg_ss, RX(regs->ebp) + RX(regs->esi) + disp);
		case VM86_OP_RM_PBP_DI:
			return realaddr_seg_off_to_realaddr16(seg_ss, RX(regs->ebp) + RX(regs->edi) + disp);
		case VM86_OP_RM_PSI:
			return realaddr_seg_off_to_realaddr16(seg_ds, RX(regs->esi) + disp);
		case VM86_OP_RM_PDI:
			return realaddr_seg_off_to_realaddr16(seg_ds, RX(regs->edi) + disp);
		case VM86_OP_RM_PBP_OR_PABS16:
			return realaddr_seg_off_to_realaddr16(seg_ss, RX(regs->ebp) + disp);
		case VM86_OP_RM_PBX:
			return realaddr_seg_off_to_realaddr16(seg_ds, RX(regs->ebx) + disp);
	}
	return 0;
}

static inline uint32_t get_arg_addr16_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *s, uint32_t idx, int reg_or_rm, int *bytes_read) {
	const uint32_t addr = get_arg_realaddr16_from_mod_reg_rm(vm86, s, idx, reg_or_rm, bytes_read);
	const uint16_t seg = addr >> 16;
	const uint16_t off = addr & 0xFFFF;

	return realaddr_seg_off_to_phys(seg, off);
}

static uint64_t get_arg_realaddr32_from_sib(struct vm86_ctx *vm86, uint16_t *s, uint32_t idx, uint8_t mod, int *bytes_read)
{
	struct vm86_regs *regs = &vm86->regs;
	uint16_t seg_ds = s ? *s : regs->ds;
	uint16_t seg_ss = s ? *s : regs->ss;
	uint8_t sib = read_mem_byte(vm86, idx);
	uint32_t disp = 0;
	uint8_t scale = 0;

	if (bytes_read)
		*bytes_read = 1;

	switch (mod)
	{
		case VM86_OP_MOD_NODISP_OR_PABS:
			disp = 0;
			break;

		case VM86_OP_MOD_REL8:
			if (bytes_read)
				*bytes_read += 1;
			/* disp8 must be sign-extended to an int32_t */
			disp = (uint32_t)(int32_t)(int8_t)read_mem_byte(vm86, idx + 1);
			break;

		case VM86_OP_MOD_ABS:
			if (bytes_read)
				*bytes_read += 4;
			disp = read_mem_dword(vm86, idx + 1);
			break;

		case VM86_OP_MOD_REG:
			return 0;
	}

	scale = (sib & VM86_OP_SIB_SCALE_MASK) >> VM86_OP_SIB_SCALE_SHIFT;

	switch (sib & VM86_OP_SIB_IDX_MASK)
	{
		case VM86_OP_SIB_IDX_EAX: disp += RE(regs->eax) << scale; break;
		case VM86_OP_SIB_IDX_ECX: disp += RE(regs->ecx) << scale; break;
		case VM86_OP_SIB_IDX_EDX: disp += RE(regs->edx) << scale; break;
		case VM86_OP_SIB_IDX_EBX: disp += RE(regs->ebx) << scale; break;
		case VM86_OP_SIB_IDX_NONE: break;
		case VM86_OP_SIB_IDX_EBP: disp += RE(regs->ebp) << scale; break;
		case VM86_OP_SIB_IDX_ESI: disp += RE(regs->esi) << scale; break;
		case VM86_OP_SIB_IDX_EDI: disp += RE(regs->edi) << scale; break;
	}

	switch (sib & VM86_OP_SIB_BASE_MASK)
	{
		case VM86_OP_SIB_BASE_EAX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->eax) + disp);

		case VM86_OP_SIB_BASE_ECX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->ecx) + disp);

		case VM86_OP_SIB_BASE_EDX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->edx) + disp);

		case VM86_OP_SIB_BASE_EBX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->ebx) + disp);

		case VM86_OP_SIB_BASE_ESP:
			return realaddr_seg_off_to_realaddr32(seg_ss, RE(regs->esp) + disp);

		case VM86_OP_SIB_BASE_EBP_OR_ABS32:
			if (mod == VM86_OP_MOD_NODISP_OR_PABS)
			{
				if (bytes_read)
					*bytes_read += 4;
				return realaddr_seg_off_to_realaddr32(seg_ds, read_mem_dword(vm86, idx + 1) + disp);
			}
			return realaddr_seg_off_to_realaddr32(seg_ss, RE(regs->ebp) + disp);

		case VM86_OP_SIB_BASE_ESI:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->esi) + disp);

		case VM86_OP_SIB_BASE_EDI:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->edi) + disp);
	}
	return 0;
}

static uint64_t get_arg_realaddr32_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *s, uint32_t idx, int reg_or_rm, int *bytes_read)
{
	struct vm86_regs *regs = &vm86->regs;
	uint16_t seg_ds = s ? *s : regs->ds;
	uint16_t seg_ss = s ? *s : regs->ss;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint32_t disp = 0;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
		return 0;

	if ((mod_reg_rm & VM86_OP_RM_MASK) == VM86_OP_RM_PSIB)
	{
		int br = 0;
		uint64_t result;

		result = get_arg_realaddr32_from_sib(vm86, s, idx + 1, mod_reg_rm & VM86_OP_MOD_MASK, &br);
		if (bytes_read)
			*bytes_read += br;
		return result;
	}

	switch(mod_reg_rm & VM86_OP_MOD_MASK)
	{
		case VM86_OP_MOD_NODISP_OR_PABS:
			if ((mod_reg_rm & VM86_OP_RM_MASK) == VM86_OP_RM_PEBP_OR_PABS32)
			{
				if (bytes_read)
					*bytes_read += 4;
				return realaddr_seg_off_to_realaddr32(seg_ds, read_mem_dword(vm86, idx + 1));
			}
			disp = 0;
			break;

		case VM86_OP_MOD_REL8:
			if (bytes_read)
				*bytes_read += 1;
			/* disp8 must be sign-extended */
			disp = (uint32_t)(int32_t)(int8_t)read_mem_byte(vm86, idx + 1);
			break;

		case VM86_OP_MOD_ABS:
			if (bytes_read)
				*bytes_read += 4;
			disp = read_mem_dword(vm86, idx + 1);
			break;

		case VM86_OP_MOD_REG:
			return 0;
	}

	switch (mod_reg_rm & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_PEAX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->eax) + disp);
		case VM86_OP_RM_PECX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->ecx) + disp);
		case VM86_OP_RM_PEDX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->edx) + disp);
		case VM86_OP_RM_PEBX:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->ebx) + disp);
		/* case VM86_OP_RM_PSIB: */ /* Already handled earlier */
		case VM86_OP_RM_PEBP_OR_PABS32:
			return realaddr_seg_off_to_realaddr32(seg_ss, RE(regs->ebp) + disp);
		case VM86_OP_RM_PESI:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->esi) + disp);
		case VM86_OP_RM_PEDI:
			return realaddr_seg_off_to_realaddr32(seg_ds, RE(regs->edi) + disp);
	}
	return 0;
}

static inline uint32_t get_arg_addr32_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *s, uint32_t idx, int reg_or_rm, int *bytes_read) {
	const uint64_t addr = get_arg_realaddr32_from_mod_reg_rm(vm86, s, idx, reg_or_rm, bytes_read);
	const uint16_t seg = (addr >> 32) & 0xFFFF;
	const uint32_t off = addr & 0xFFFFFFFF;

	return realaddr_seg_off_to_phys(seg, off);
}

static uint8_t get_arg8_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int *bytes_read)
{
	struct vm86_regs *regs = &vm86->regs;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint32_t addr;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
	{
		switch (mod_reg_rm & VM86_OP_REG_MASK)
		{
			case VM86_OP_REG_AL: return RL(regs->eax);
			case VM86_OP_REG_CL: return RL(regs->ecx);
			case VM86_OP_REG_DL: return RL(regs->edx);
			case VM86_OP_REG_BL: return RL(regs->ebx);
			case VM86_OP_REG_AH: return RH(regs->eax);
			case VM86_OP_REG_CH: return RH(regs->ecx);
			case VM86_OP_REG_DH: return RH(regs->edx);
			case VM86_OP_REG_BH: return RH(regs->ebx);
		}
		return 0;
	}

	if ((mod_reg_rm & VM86_OP_MOD_MASK) == VM86_OP_MOD_REG)
	{
		switch (mod_reg_rm & VM86_OP_RM_MASK)
		{
			case VM86_OP_RM_AL: return RL(regs->eax);
			case VM86_OP_RM_CL: return RL(regs->ecx);
			case VM86_OP_RM_DL: return RL(regs->edx);
			case VM86_OP_RM_BL: return RL(regs->ebx);
			case VM86_OP_RM_AH: return RH(regs->eax);
			case VM86_OP_RM_CH: return RH(regs->ecx);
			case VM86_OP_RM_DH: return RH(regs->edx);
			case VM86_OP_RM_BH: return RH(regs->ebx);
		}
		return 0;
	}

	if (bytes_read)
		*bytes_read = 0;

	if (addr_size == 4)
		addr = get_arg_addr32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	else
		addr = get_arg_addr16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	return read_mem_byte(vm86, addr);
}

static uint16_t get_arg16_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int *bytes_read)
{
	struct vm86_regs *regs = &vm86->regs;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint32_t addr;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
	{
		switch (mod_reg_rm & VM86_OP_REG_MASK)
		{
			case VM86_OP_REG_AX: return RX(regs->eax);
			case VM86_OP_REG_CX: return RX(regs->ecx);
			case VM86_OP_REG_DX: return RX(regs->edx);
			case VM86_OP_REG_BX: return RX(regs->ebx);
			case VM86_OP_REG_SP: return RX(regs->esp);
			case VM86_OP_REG_BP: return RX(regs->ebp);
			case VM86_OP_REG_SI: return RX(regs->esi);
			case VM86_OP_REG_DI: return RX(regs->edi);
		}
		return 0;
	}

	if ((mod_reg_rm & VM86_OP_MOD_MASK) == VM86_OP_MOD_REG)
	{
		switch (mod_reg_rm & VM86_OP_RM_MASK)
		{
			case VM86_OP_RM_AX: return RX(regs->eax);
			case VM86_OP_RM_CX: return RX(regs->ecx);
			case VM86_OP_RM_DX: return RX(regs->edx);
			case VM86_OP_RM_BX: return RX(regs->ebx);
			case VM86_OP_RM_SP: return RX(regs->esp);
			case VM86_OP_RM_BP: return RX(regs->ebp);
			case VM86_OP_RM_SI: return RX(regs->esi);
			case VM86_OP_RM_DI: return RX(regs->edi);
		}
		return 0;
	}

	if (bytes_read)
		*bytes_read = 0;

	if (addr_size == 4)
		addr = get_arg_addr32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	else
		addr = get_arg_addr16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	return read_mem_word(vm86, addr);
}

static uint32_t get_arg32_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int *bytes_read)
{
	struct vm86_regs *regs = &vm86->regs;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint32_t addr;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
	{
		switch (mod_reg_rm & VM86_OP_REG_MASK)
		{
			case VM86_OP_REG_EAX: return RE(regs->eax);
			case VM86_OP_REG_ECX: return RE(regs->ecx);
			case VM86_OP_REG_EDX: return RE(regs->edx);
			case VM86_OP_REG_EBX: return RE(regs->ebx);
			case VM86_OP_REG_ESP: return RE(regs->esp);
			case VM86_OP_REG_EBP: return RE(regs->ebp);
			case VM86_OP_REG_ESI: return RE(regs->esi);
			case VM86_OP_REG_EDI: return RE(regs->edi);
		}
		return 0;
	}

	if ((mod_reg_rm & VM86_OP_MOD_MASK) == VM86_OP_MOD_REG)
	{
		switch (mod_reg_rm & VM86_OP_RM_MASK)
		{
			case VM86_OP_RM_EAX: return RE(regs->eax);
			case VM86_OP_RM_ECX: return RE(regs->ecx);
			case VM86_OP_RM_EDX: return RE(regs->edx);
			case VM86_OP_RM_EBX: return RE(regs->ebx);
			case VM86_OP_RM_ESP: return RE(regs->esp);
			case VM86_OP_RM_EBP: return RE(regs->ebp);
			case VM86_OP_RM_ESI: return RE(regs->esi);
			case VM86_OP_RM_EDI: return RE(regs->edi);
		}
		return 0;
	}

	if (bytes_read)
		*bytes_read = 0;

	if (addr_size == 4)
		addr = get_arg_addr32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	else
		addr = get_arg_addr16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	return read_mem_dword(vm86, addr);
}

static inline union vm86_gpr get_arg_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int op_size, int *bytes_read) {
	union vm86_gpr result;

	if (op_size == 4)
		RE(result) = get_arg32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, addr_size, bytes_read);
	else
		RX(result) = get_arg16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, addr_size, bytes_read);
	return result;
}

static void set_arg8_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int *bytes_read, uint8_t val)
{
	struct vm86_regs *regs = &vm86->regs;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint32_t addr;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
	{
		switch (mod_reg_rm & VM86_OP_REG_MASK)
		{
			case VM86_OP_REG_AL: RL(regs->eax) = val; break;
			case VM86_OP_REG_CL: RL(regs->ecx) = val; break;
			case VM86_OP_REG_DL: RL(regs->edx) = val; break;
			case VM86_OP_REG_BL: RL(regs->ebx) = val; break;
			case VM86_OP_REG_AH: RH(regs->eax) = val; break;
			case VM86_OP_REG_CH: RH(regs->ecx) = val; break;
			case VM86_OP_REG_DH: RH(regs->edx) = val; break;
			case VM86_OP_REG_BH: RH(regs->ebx) = val; break;
		}
		return;
	}

	if ((mod_reg_rm & VM86_OP_MOD_MASK) == VM86_OP_MOD_REG)
	{
		switch (mod_reg_rm & VM86_OP_RM_MASK)
		{
			case VM86_OP_RM_AL: RL(regs->eax) = val; break;
			case VM86_OP_RM_CL: RL(regs->ecx) = val; break;
			case VM86_OP_RM_DL: RL(regs->edx) = val; break;
			case VM86_OP_RM_BL: RL(regs->ebx) = val; break;
			case VM86_OP_RM_AH: RH(regs->eax) = val; break;
			case VM86_OP_RM_CH: RH(regs->ecx) = val; break;
			case VM86_OP_RM_DH: RH(regs->edx) = val; break;
			case VM86_OP_RM_BH: RH(regs->ebx) = val; break;
		}
		return;
	}

	if (bytes_read)
		*bytes_read = 0;

	if (addr_size == 4)
		addr = get_arg_addr32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	else
		addr = get_arg_addr16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	write_mem_byte(vm86, addr, val);
}

static void set_arg16_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int *bytes_read, uint16_t val)
{
	struct vm86_regs *regs = &vm86->regs;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint32_t addr;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
	{
		switch (mod_reg_rm & VM86_OP_REG_MASK)
		{
			case VM86_OP_REG_AX: RX(regs->eax) = val; break;
			case VM86_OP_REG_CX: RX(regs->ecx) = val; break;
			case VM86_OP_REG_DX: RX(regs->edx) = val; break;
			case VM86_OP_REG_BX: RX(regs->ebx) = val; break;
			case VM86_OP_REG_SP: RX(regs->esp) = val; break;
			case VM86_OP_REG_BP: RX(regs->ebp) = val; break;
			case VM86_OP_REG_SI: RX(regs->esi) = val; break;
			case VM86_OP_REG_DI: RX(regs->edi) = val; break;
		}
		return;
	}

	if ((mod_reg_rm & VM86_OP_MOD_MASK) == VM86_OP_MOD_REG)
	{
		switch (mod_reg_rm & VM86_OP_RM_MASK)
		{
			case VM86_OP_RM_AX: RX(regs->eax) = val; break;
			case VM86_OP_RM_CX: RX(regs->ecx) = val; break;
			case VM86_OP_RM_DX: RX(regs->edx) = val; break;
			case VM86_OP_RM_BX: RX(regs->ebx) = val; break;
			case VM86_OP_RM_SP: RX(regs->esp) = val; break;
			case VM86_OP_RM_BP: RX(regs->ebp) = val; break;
			case VM86_OP_RM_SI: RX(regs->esi) = val; break;
			case VM86_OP_RM_DI: RX(regs->edi) = val; break;
		}
		return;
	}

	if (bytes_read)
		*bytes_read = 0;

	if (addr_size == 4)
		addr = get_arg_addr32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	else
		addr = get_arg_addr16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	write_mem_word(vm86, addr, val);
}

static void set_arg32_from_mod_reg_rm(struct vm86_ctx *vm86, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int *bytes_read, uint32_t val)
{
	struct vm86_regs *regs = &vm86->regs;
	uint8_t mod_reg_rm = read_mem_byte(vm86, idx);
	uint32_t addr;

	if (bytes_read)
		*bytes_read = 1;

	if (reg_or_rm)
	{
		switch (mod_reg_rm & VM86_OP_REG_MASK)
		{
			case VM86_OP_REG_EAX: RE(regs->eax) = val; break;
			case VM86_OP_REG_ECX: RE(regs->ecx) = val; break;
			case VM86_OP_REG_EDX: RE(regs->edx) = val; break;
			case VM86_OP_REG_EBX: RE(regs->ebx) = val; break;
			case VM86_OP_REG_ESP: RE(regs->esp) = val; break;
			case VM86_OP_REG_EBP: RE(regs->ebp) = val; break;
			case VM86_OP_REG_ESI: RE(regs->esi) = val; break;
			case VM86_OP_REG_EDI: RE(regs->edi) = val; break;
		}
		return;
	}

	if ((mod_reg_rm & VM86_OP_MOD_MASK) == VM86_OP_MOD_REG)
	{
		switch (mod_reg_rm & VM86_OP_RM_MASK)
		{
			case VM86_OP_RM_EAX: RE(regs->eax) = val; break;
			case VM86_OP_RM_ECX: RE(regs->ecx) = val; break;
			case VM86_OP_RM_EDX: RE(regs->edx) = val; break;
			case VM86_OP_RM_EBX: RE(regs->ebx) = val; break;
			case VM86_OP_RM_ESP: RE(regs->esp) = val; break;
			case VM86_OP_RM_EBP: RE(regs->ebp) = val; break;
			case VM86_OP_RM_ESI: RE(regs->esi) = val; break;
			case VM86_OP_RM_EDI: RE(regs->edi) = val; break;
		}
		return;
	}

	if (bytes_read)
		*bytes_read = 0;

	if (addr_size == 4)
		addr = get_arg_addr32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	else
		addr = get_arg_addr16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, bytes_read);
	write_mem_dword(vm86, addr, val);
}

static inline void set_arg_from_mod_reg_rm(struct vm86_ctx *vm86, union vm86_gpr src, uint16_t *seg, uint32_t idx, int reg_or_rm, int addr_size, int op_size, int *bytes_read) {
	if (op_size == 4)
		set_arg32_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, addr_size, bytes_read, RE(src));
	else
		set_arg16_from_mod_reg_rm(vm86, seg, idx, reg_or_rm, addr_size, bytes_read, RX(src));
}

#if 0
static uint8_t get_arg8_from_reg(struct vm86_ctx *vm86, uint8_t reg)
{
	struct vm86_regs *regs = &vm86->regs;

	switch (reg & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_AL: return RL(regs->eax);
		case VM86_OP_RM_CL: return RL(regs->ecx);
		case VM86_OP_RM_DL: return RL(regs->edx);
		case VM86_OP_RM_BL: return RL(regs->ebx);
		case VM86_OP_RM_AH: return RH(regs->eax);
		case VM86_OP_RM_CH: return RH(regs->ecx);
		case VM86_OP_RM_DH: return RH(regs->edx);
		case VM86_OP_RM_BH: return RH(regs->ebx);
	}

	return 0;
}
#endif

static uint16_t get_arg16_from_reg(struct vm86_ctx *vm86, uint8_t reg)
{
	struct vm86_regs *regs = &vm86->regs;

	switch (reg & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_AX: return RX(regs->eax);
		case VM86_OP_RM_CX: return RX(regs->ecx);
		case VM86_OP_RM_DX: return RX(regs->edx);
		case VM86_OP_RM_BX: return RX(regs->ebx);
		case VM86_OP_RM_SP: return RX(regs->esp);
		case VM86_OP_RM_BP: return RX(regs->ebp);
		case VM86_OP_RM_SI: return RX(regs->esi);
		case VM86_OP_RM_DI: return RX(regs->edi);
	}

	return 0;
}

static uint32_t get_arg32_from_reg(struct vm86_ctx *vm86, uint8_t reg)
{
	struct vm86_regs *regs = &vm86->regs;

	switch (reg & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_EAX: return RE(regs->eax);
		case VM86_OP_RM_ECX: return RE(regs->ecx);
		case VM86_OP_RM_EDX: return RE(regs->edx);
		case VM86_OP_RM_EBX: return RE(regs->ebx);
		case VM86_OP_RM_ESP: return RE(regs->esp);
		case VM86_OP_RM_EBP: return RE(regs->ebp);
		case VM86_OP_RM_ESI: return RE(regs->esi);
		case VM86_OP_RM_EDI: return RE(regs->edi);
	}

	return 0;
}

static inline union vm86_gpr get_arg_from_reg(struct vm86_ctx *vm86, uint8_t reg, int op_size) {
	union vm86_gpr result;

	if (op_size == 4)
		RE(result) = get_arg32_from_reg(vm86, reg);
	else
		RX(result) = get_arg16_from_reg(vm86, reg);

	return result;
}

static void set_arg8_from_reg(struct vm86_ctx *vm86, uint8_t reg, uint8_t val)
{
	struct vm86_regs *regs = &vm86->regs;

	switch (reg & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_AL: RL(regs->eax) = val; break;
		case VM86_OP_RM_CL: RL(regs->ecx) = val; break;
		case VM86_OP_RM_DL: RL(regs->edx) = val; break;
		case VM86_OP_RM_BL: RL(regs->ebx) = val; break;
		case VM86_OP_RM_AH: RH(regs->eax) = val; break;
		case VM86_OP_RM_CH: RH(regs->ecx) = val; break;
		case VM86_OP_RM_DH: RH(regs->edx) = val; break;
		case VM86_OP_RM_BH: RH(regs->ebx) = val; break;
	}
}

static void set_arg16_from_reg(struct vm86_ctx *vm86, uint8_t reg, uint16_t val)
{
	struct vm86_regs *regs = &vm86->regs;

	switch (reg & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_AX: RX(regs->eax) = val; break;
		case VM86_OP_RM_CX: RX(regs->ecx) = val; break;
		case VM86_OP_RM_DX: RX(regs->edx) = val; break;
		case VM86_OP_RM_BX: RX(regs->ebx) = val; break;
		case VM86_OP_RM_SP: RX(regs->esp) = val; break;
		case VM86_OP_RM_BP: RX(regs->ebp) = val; break;
		case VM86_OP_RM_SI: RX(regs->esi) = val; break;
		case VM86_OP_RM_DI: RX(regs->edi) = val; break;
	}
}

static void set_arg32_from_reg(struct vm86_ctx *vm86, uint8_t reg, uint32_t val)
{
	struct vm86_regs *regs = &vm86->regs;

	switch (reg & VM86_OP_RM_MASK)
	{
		case VM86_OP_RM_EAX: RE(regs->eax) = val; break;
		case VM86_OP_RM_ECX: RE(regs->ecx) = val; break;
		case VM86_OP_RM_EDX: RE(regs->edx) = val; break;
		case VM86_OP_RM_EBX: RE(regs->ebx) = val; break;
		case VM86_OP_RM_ESP: RE(regs->esp) = val; break;
		case VM86_OP_RM_EBP: RE(regs->ebp) = val; break;
		case VM86_OP_RM_ESI: RE(regs->esi) = val; break;
		case VM86_OP_RM_EDI: RE(regs->edi) = val; break;
	}
}

static inline void set_arg_from_reg(struct vm86_ctx *vm86, uint8_t reg, int op_size, union vm86_gpr src) {
	if (op_size == 4)
		set_arg32_from_reg(vm86, reg, RE(src));
	else
		set_arg16_from_reg(vm86, reg, RX(src));
}

static int vm86_run(struct vm86_ctx *vm86, uint16_t timeout_ms)
{
	struct vm86_regs *regs = &vm86->regs;
	uint32_t phys_addr;
	uint8_t instr[3];
	uint32_t cur_instr_eip;
	int last_was_prefix;
	int has_rep_prefix;
	int has_repnz_prefix;
	int has_rep_any_prefix;
	int op_size;
	int addr_size;
	uint16_t *segment;
	int leave = 0;

	(void)timeout_ms;

	last_was_prefix = 0;

	while (!leave && !vm86->shutdown)
	{
		phys_addr = ctx_get_ip(vm86);
		instr[0] = read_mem_byte_silent(vm86, ctx_get_ip_off(vm86, 0));
		instr[1] = read_mem_byte_silent(vm86, ctx_get_ip_off(vm86, 1));
		instr[2] = read_mem_byte_silent(vm86, ctx_get_ip_off(vm86, 2));

		if (vm86->trace)
		{
			uint32_t stack = realaddr_seg_off_to_phys(regs->ss, RX(regs->esp));
			printk("Stack dump at [0x%x]: %x %x %x %x", stack,
					read_mem_dword_silent(vm86, stack),
					read_mem_dword_silent(vm86, stack + 4),
					read_mem_dword_silent(vm86, stack + 8),
					read_mem_dword_silent(vm86, stack + 12));

			printk("Mem dump at [0x%x]: %x %x %x %x", phys_addr,
					read_mem_dword_silent(vm86, ctx_get_ip_off(vm86, 0)),
					read_mem_dword_silent(vm86, ctx_get_ip_off(vm86, 4)),
					read_mem_dword_silent(vm86, ctx_get_ip_off(vm86, 8)),
					read_mem_dword_silent(vm86, ctx_get_ip_off(vm86, 12)));

			dump_regs(vm86);
		}

		if (!last_was_prefix)
		{
			cur_instr_eip = regs->eip;
			has_rep_prefix = 0;
			has_repnz_prefix = 0;
			op_size = 2;
			addr_size = 2;
			segment = NULL;
		}

		last_was_prefix = 0;

		has_rep_any_prefix = has_rep_prefix || has_repnz_prefix;

		switch (instr[0])
		{
			/* Prefixes */

			case 0xF0: /* LOCK */
			{
				/* Ignored */
				last_was_prefix = 1;
				/* TODO: Throw #UD on unsupported following instruction */
				regs->eip++;
				break;
			}
			case 0xF3: /* REP/REPE/REPZ */
			{
				last_was_prefix = 1;
				/* TODO: Throw #UD on unsupported following instruction */
				has_rep_prefix = 1;
				regs->eip++;
				break;
			}
			case 0xF2: /* REPNE/REPNZ */
			{
				last_was_prefix = 1;
				/* TODO: Throw #UD on unsupported following instruction */
				has_repnz_prefix = 1;
				regs->eip++;
				break;
			}

			/* Overrides */

			case 0x2E: /* CS */
			{
				last_was_prefix = 1;
				segment = &regs->cs;
				regs->eip++;
				break;
			}
			case 0x3E: /* DS */
			{
				last_was_prefix = 1;
				segment = &regs->ds;
				regs->eip++;
				break;
			}
			case 0x26: /* ES */
			{
				last_was_prefix = 1;
				segment = &regs->es;
				regs->eip++;
				break;
			}
			case 0x64: /* FS */
			{
				last_was_prefix = 1;
				segment = &regs->fs;
				regs->eip++;
				break;
			}
			case 0x65: /* GS */
			{
				last_was_prefix = 1;
				segment = &regs->gs;
				regs->eip++;
				break;
			}
			case 0x36: /* SS */
			{
				last_was_prefix = 1;
				segment = &regs->ss;
				regs->eip++;
				break;
			}
			case 0x66: /* Operand size */
			{
				last_was_prefix = 1;
				op_size = 4;
				regs->eip++;
				break;
			}
			case 0x67: /* Address size */
			{
				last_was_prefix = 1;
				addr_size = 4;
				regs->eip++;
				break;
			}

			/* 8086 */

			case 0x37: /* AAA */
			{
				if ((RL(regs->eax) & 0xF) > 9 || regs->eflags & VM86_EFLAGS_AF)
				{
					RX(regs->eax) += 0x106; /* CPU >= 386 */
					SETF(regs->eflags, VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				}
				else
					UNSETF(regs->eflags, VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				RL(regs->eax) &= 0xF;
				update_eflags_szp_u8(vm86, RL(regs->eax));
				UNSETF(regs->eflags, VM86_EFLAGS_OF);
				regs->eip++;
				break;
			}
			case 0xD5: /* AAD imm8 */
			{
				RL(regs->eax) += RH(regs->eax) * instr[1];
				RH(regs->eax) = 0;
				update_eflags_szp_u8(vm86, RL(regs->eax));
				regs->eip += 2;
				break;
			}
			case 0xD4: /* AAM imm8 */
			{
				uint8_t al = RL(regs->eax);

				if (chk_throw_excp_de(vm86, instr[1]))
					break;

				RL(regs->eax) = al % instr[1];
				RH(regs->eax) = al / instr[1];

				update_eflags_szp_u8(vm86, RL(regs->eax));
				UNSETF(regs->eflags, VM86_EFLAGS_CF | VM86_EFLAGS_OF);

				regs->eip += 2;
				break;
			}
			case 0x3F: /* AAS */
			{
				if ((RL(regs->eax) & 0xF) > 9 || regs->eflags & VM86_EFLAGS_AF)
				{
					RX(regs->eax) -= 0x106; /* CPU >= 386 */
					SETF(regs->eflags, VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				}
				else
					UNSETF(regs->eflags, VM86_EFLAGS_AF | VM86_EFLAGS_CF);

				RL(regs->eax) &= 0xF;
				update_eflags_szp_u8(vm86, RL(regs->eax));
				UNSETF(regs->eflags, VM86_EFLAGS_OF);
				regs->eip++;
				break;
			}
			case 0x14: /* ADC al, imm8 */
			{
				const uint8_t a1 = RL(regs->eax);
				const uint8_t a2 = instr[1];
				uint8_t r = a1 + a2 + !!(regs->eflags & VM86_EFLAGS_CF);

				update_eflags_oac_add_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				RL(regs->eax) = r;
				regs->eip += 2;
				break;
			}
			case 0x15: /* ADC ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r;

				r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) + RX(a2) + !!(regs->eflags & VM86_EFLAGS_CF);
				else
					RE(r) = RE(a1) + RE(a2) + !!(regs->eflags & VM86_EFLAGS_CF);

				update_eflags_oac_add(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eax = r;
				regs->eip += 1 + op_size;
				break;
			}
			case 0x80: /* ADC/ADD/AND/CMP/OR/SBB/SUB/XOR r/m8, imm8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = read_mem_byte(vm86, phys_addr + 1 + rm_len);
				uint8_t r = 0;
				int write_result = 1;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					case VM86_OP_REG_2: /* ADC */
						r = !!(regs->eflags & VM86_EFLAGS_CF);
						/* Fall through. */

					case VM86_OP_REG_0: /* ADD */
						r += a1 + a2;
						update_eflags_oac_add_u8(vm86, r, a1, a2);
						break;

					case VM86_OP_REG_4: /* AND */
						r = a1 & a2;
						UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
						break;

					case VM86_OP_REG_7: /* CMP */
						r = a1 - a2;
						update_eflags_oac_sub_u8(vm86, r, a1, a2);
						write_result = 0;
						break;

					case VM86_OP_REG_1: /* OR */
						r = a1 | a2;
						UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
						break;

					case VM86_OP_REG_3: /* SBB */
						r = -!!(regs->eflags & VM86_EFLAGS_CF);
						/* Fall through. */

					case VM86_OP_REG_5: /* SUB */
						r += a1 - a2;
						update_eflags_oac_sub_u8(vm86, r, a1, a2);
						break;

					case VM86_OP_REG_6: /* XOR */
						r = a1 ^ a2;
						UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
						break;
				}

				if (write_result)
					set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len + 1;
				break;
			}
			case 0x81: /* ADC/ADD/AND/CMP/OR/SBB/SUB/XOR r/m16/32, imm16/32 */
			case 0x83: /* ADC/ADD/AND/CMP/OR/SBB/SUB/XOR r/m16/32, imm8 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = instr[0] == 0x81
						? read_mem(vm86, phys_addr + 1 + rm_len, op_size)
						/* a2 is sign-extended if imm8 */
						: VM86_GPR((int32_t)(int8_t)read_mem_byte(vm86, phys_addr + 1 + rm_len));
				union vm86_gpr r;
				int write_result = 1;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					case VM86_OP_REG_2: /* ADC */
						if (op_size == 2)
							RX(r) = RX(a1) + RX(a2) + !!(regs->eflags & VM86_EFLAGS_CF);
						else
							RE(r) = RE(a1) + RE(a2) + !!(regs->eflags & VM86_EFLAGS_CF);
						update_eflags_oac_add(vm86, op_size, r, a1, a2);
						break;

					case VM86_OP_REG_0: /* ADD */
						if (op_size == 2)
							RX(r) = RX(a1) + RX(a2);
						else
							RE(r) = RE(a1) + RE(a2);
						update_eflags_oac_add(vm86, op_size, r, a1, a2);
						break;

					case VM86_OP_REG_4: /* AND */
						if (op_size == 2)
							RX(r) = RX(a1) & RX(a2);
						else
							RE(r) = RE(a1) & RE(a2);
						UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
						break;

					case VM86_OP_REG_7: /* CMP */
						if (op_size == 2)
							RX(r) = RX(a1) - RX(a2);
						else
							RE(r) = RE(a1) - RE(a2);
						update_eflags_oac_sub(vm86, op_size, r, a1, a2);
						write_result = 0;
						break;

					case VM86_OP_REG_1: /* OR */
						if (op_size == 2)
							RX(r) = RX(a1) | RX(a2);
						else
							RE(r) = RE(a1) | RE(a2);
						UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
						break;

					case VM86_OP_REG_3: /* SBB */
						if (op_size == 2)
							RX(r) = RX(a1) - RX(a2) - !!(regs->eflags & VM86_EFLAGS_CF);
						else
							RE(r) = RE(a1) - RE(a2) - !!(regs->eflags & VM86_EFLAGS_CF);
						update_eflags_oac_sub(vm86, op_size, r, a1, a2);
						break;

					case VM86_OP_REG_5: /* SUB */
						if (op_size == 2)
							RX(r) = RX(a1) - RX(a2);
						else
							RE(r) = RE(a1) - RE(a2);
						update_eflags_oac_sub(vm86, op_size, r, a1, a2);
						break;

					case VM86_OP_REG_6: /* XOR */
						if (op_size == 2)
							RX(r) = RX(a1) ^ RX(a2);
						else
							RE(r) = RE(a1) ^ RE(a2);
						UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
						break;
				}

				if (write_result)
					set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len + (instr[0] == 0x81 ? op_size : 1);
				break;
			}
			case 0x10: /* ADC r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint8_t r = a1 + a2 + !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				update_eflags_oac_add_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x11: /* ADC r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) + RX(a2) + !!(regs->eflags & VM86_EFLAGS_CF);
				else
					RE(r) = RE(a1) + RE(a2) + !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				update_eflags_oac_add(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x12: /* ADC r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint8_t r = a1 + a2 + !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, r);
				update_eflags_oac_add_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x13: /* ADC r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) + RX(a2) + !!(regs->eflags & VM86_EFLAGS_CF);
				else
					RE(r) = RE(a1) + RE(a2) + !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				update_eflags_oac_add(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x04: /* ADD al, imm8 */
			{
				const uint8_t a1 = RL(regs->eax);
				const uint8_t a2 = instr[1];
				uint8_t r = a1 + a2;

				RL(regs->eax) = r;
				update_eflags_oac_add_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 2;
				break;
			}
			case 0x05: /* ADD ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r;

				r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) + RX(a2);
				else
					RE(r) = RE(a1) + RE(a2);

				update_eflags_oac_add(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eax = r;
				regs->eip += 1 + op_size;
				break;
			}
			case 0x00: /* ADD r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint16_t r = a1 + a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				update_eflags_oac_add_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x01: /* ADD r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) + RX(a2);
				else
					RE(r) = RE(a1) + RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				update_eflags_oac_add(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x02: /* ADD r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint16_t r = a1 + a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, r);
				update_eflags_oac_add_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x03: /* ADD r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) + RX(a2);
				else
					RE(r) = RE(a1) + RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				update_eflags_oac_add(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x24: /* AND al, imm8 */
			case 0xA8: /* TEST al, imm8 */
			{
				uint8_t r = RL(regs->eax) & instr[1];
				int write_result = (instr[0] == 0x24);

				if (write_result)
					RL(regs->eax) = r;

				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + 1;
				break;
			}
			case 0x25: /* AND ax/eax, imm16/32 */
			case 0xA9: /* TEST ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r;
				int write_result = (instr[0] == 0x25);

				r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) & RX(a2);
				else
					RE(r) = RE(a1) & RE(a2);

				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				if (write_result)
					regs->eax = r;
				regs->eip += 1 + op_size;
				break;
			}
			case 0x20: /* AND r/m8, r8 */
			case 0x84: /* TEST r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint8_t r = a1 & a2;
				int write_result = (instr[0] == 0x20);

				if (write_result)
					set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x21: /* AND r/m16/32, r16/32 */
			case 0x85: /* TEST r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;
				int write_result = (instr[0] == 0x21);

				if (op_size == 2)
					RX(r) = RX(a1) & RX(a2);
				else
					RE(r) = RE(a1) & RE(a2);

				if (write_result)
					set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x22: /* AND r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint8_t r = a1 & a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, r);
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x23: /* AND r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) & RX(a2);
				else
					RE(r) = RE(a1) & RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xE8: /* CALL near rel16/32 */
			{
				const union vm86_gpr a1 = read_mem(vm86, phys_addr + 1, op_size);

				regs->eip += 1 + op_size;
				if (op_size == 2)
				{
					push_word(vm86, regs->eip);
					regs->eip += (int16_t)RX(a1);
				}
				else
				{
					push_dword(vm86, regs->eip);
					regs->eip += (int32_t)RE(a1);
				}
				break;
			}
			case 0xFF: /* CALL near/CALL far/DEC/INC/JMP near/JMP far/PUSH r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;
				int is_conventional = 1;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					case VM86_OP_REG_2: /* CALL near */
					{
						is_conventional = 0;

						regs->eip += 1 + rm_len;
						if (op_size == 2)
						{
							push_word(vm86, regs->eip);
							regs->eip = RX(a1);
						}
						else
						{
							push_dword(vm86, regs->eip);
							regs->eip = RE(a1);
						}
						break;
					}
					case VM86_OP_REG_3: /* CALL far */
					{
						const uint32_t addr = realaddr_seg_off_to_phys(segment ? *segment : regs->ds, op_size == 4 ? RE(a1) : RX(a1));
						uint16_t seg;
						union vm86_gpr off;

						is_conventional = 0;

						regs->eip += 1 + rm_len;
						if (op_size == 2)
						{
							seg = read_mem_word(vm86, addr + 2);
							RX(off) = read_mem_word(vm86, addr);
							push_word(vm86, regs->cs);
							push_word(vm86, regs->eip);
							regs->cs = seg;
							regs->eip = RX(off);
						}
						else
						{
							seg = read_mem_word(vm86, addr + 4);
							RE(off) = read_mem_dword(vm86, addr);
							push_word(vm86, regs->cs);
							push_dword(vm86, regs->eip);
							regs->cs = seg;
							regs->eip = RE(off);
						}
						break;
					}
					case VM86_OP_REG_1: /* DEC */
					{
						if (op_size == 2)
							RX(r) = RX(a1) - 1;
						else
							RE(r) = RE(a1) - 1;
						break;
					}
					case VM86_OP_REG_0: /* INC */
					{
						if (op_size == 2)
							RX(r) = RX(a1) + 1;
						else
							RE(r) = RE(a1) + 1;
						break;
					}
					case VM86_OP_REG_4: /* JMP near */
					{
						is_conventional = 0;

						regs->eip += 1 + rm_len;
						if (op_size == 2)
							regs->eip = RX(a1);
						else
							regs->eip = RE(a1);
						break;
					}
					case VM86_OP_REG_5: /* JMP far */
					{
						const uint32_t addr = realaddr_seg_off_to_phys(segment ? *segment : regs->ds, op_size == 4 ? RE(a1) : RX(a1));
						uint16_t seg;
						union vm86_gpr off;

						is_conventional = 0;

						regs->eip += 1 + rm_len;
						if (op_size == 2)
						{
							seg = read_mem_word(vm86, addr + 2);
							RX(off) = read_mem_word(vm86, addr);
							regs->cs = seg;
							regs->eip = RX(off);
						}
						else
						{
							seg = read_mem_word(vm86, addr + 4);
							RE(off) = read_mem_dword(vm86, addr);
							regs->cs = seg;
							regs->eip = RE(off);
						}
						break;
					}
					case VM86_OP_REG_6: /* PUSH */
					{
						is_conventional = 0;

						if (op_size == 2)
							push_word(vm86, RX(a1));
						else
							push_dword(vm86, RE(a1));
						regs->eip += 1 + rm_len;
						break;
					}
					case VM86_OP_REG_7:
					{
						is_conventional = 0;

						trigger_excp(vm86, VM86_EXCP_UD, 0);
						break;
					}
				}

				if (!is_conventional)
					break;

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x9A: /* CALL far ptr16:16/32 */
			{
				const uint16_t seg = read_mem_word(vm86, phys_addr + 1 + op_size);
				const union vm86_gpr off = read_mem(vm86, phys_addr + 1, op_size);

				regs->eip += 1 + 2 + op_size;
				if (op_size == 2)
				{
					push_word(vm86, regs->cs);
					push_word(vm86, regs->eip);
					regs->cs = seg;
					regs->eip = RX(off);
				}
				else
				{
					push_word(vm86, regs->cs);
					push_dword(vm86, regs->eip);
					regs->cs = seg;
					regs->eip = RE(off);
				}
				break;
			}
			case 0x98: /* CBW */
			{
				/* AL must be sign-extended to AX */
				RX(regs->eax) = (uint16_t)(int16_t)(int8_t)RL(regs->eax);
				regs->eip++;
				break;
			}
			case 0xF8: /* CLC */
			{
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_CF);
				regs->eip++;
				break;
			}
			case 0xFC: /* CLD */
			{
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_DF);
				regs->eip++;
				break;
			}
			case 0xFA: /* CLI */
			{
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_IF);
				regs->eip++;
				break;
			}
			case 0xF5: /* CMC */
			{
				vm86->regs.eflags = vm86->regs.eflags ^ VM86_EFLAGS_CF;
				regs->eip++;
				break;
			}
			case 0x3C: /* CMP al, imm8 */
			{
				const uint8_t a1 = RL(regs->eax);
				const uint8_t a2 = instr[1];
				uint8_t r = a1 - a2;

				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 2;
				break;
			}
			case 0x3D: /* CMP ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2);
				else
					RE(r) = RE(a1) - RE(a2);

				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + op_size;
				break;
			}
			case 0x38: /* CMP r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint16_t r = a1 - a2;

				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x39: /* CMP r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2);
				else
					RE(r) = RE(a1) - RE(a2);

				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x3A: /* CMP r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint16_t r = a1 - a2;

				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x3B: /* CMP r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2);
				else
					RE(r) = RE(a1) - RE(a2);

				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xA6: /* CMPS m8, m8 */
			{
				printk("TODO: CMPSB");
				trigger_excp(vm86, VM86_EXCP_UD, 0);
				regs->eip += 0;
				break;
			}
			case 0xA7: /* CMPS m16/32, m16/32 */
			{
				printk("TODO: CMPSW/CMPSD");
				trigger_excp(vm86, VM86_EXCP_UD, 0);
				regs->eip += 0;
				break;
			}
			case 0x99: /* CWD/CDQ */
			{
				if (op_size == 2)
					RX(vm86->regs.edx) = (RX(vm86->regs.eax) & 0x8000u) ? 0xFFFFu : 0;
				else
					RE(vm86->regs.edx) = (RE(vm86->regs.eax) & 0x80000000u) ? 0xFFFFFFFFu : 0;
				regs->eip++;
				break;
			}
			case 0x27: /* DAA */
			{
				printk("TODO: DAA");
				trigger_excp(vm86, VM86_EXCP_UD, 0);
				regs->eip += 0;
				break;
			}
			case 0x2F: /* DAS */
			{
				printk("TODO: DAS");
				trigger_excp(vm86, VM86_EXCP_UD, 0);
				regs->eip += 0;
				break;
			}
			case 0xFE: /* DEC/INC r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint8_t r;
				int leave = 0;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					case VM86_OP_REG_1: /* DEC */
						r = a1 - 1;
						break;

					case VM86_OP_REG_0: /* INC */
						r = a1 + 1;
						break;

					case VM86_OP_REG_2:
					case VM86_OP_REG_3:
					case VM86_OP_REG_4:
					case VM86_OP_REG_5:
					case VM86_OP_REG_6:
					case VM86_OP_REG_7:
						trigger_excp(vm86, VM86_EXCP_UD, 0);
						leave = 1;
						break;
				}

				if (leave)
					break;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x48: /* DEC reg16/32 */
			case 0x49:
			case 0x4A:
			case 0x4B:
			case 0x4C:
			case 0x4D:
			case 0x4E:
			case 0x4F:
			case 0x40: /* INC reg16/32 */
			case 0x41:
			case 0x42:
			case 0x43:
			case 0x44:
			case 0x45:
			case 0x46:
			case 0x47: /* DEC/INC reg16/32 */
			{
				const union vm86_gpr a1 = get_arg_from_reg(vm86, instr[0], op_size);
				union vm86_gpr r;

				if (instr[0] >= 0x48)
				{
					/* DEC */
					if (op_size == 2)
						RX(r) = RX(a1) - 1;
					else
						RE(r) = RE(a1) - 1;
				}
				else
				{
					/* INC */
					if (op_size == 2)
						RX(r) = RX(a1) + 1;
					else
						RE(r) = RE(a1) + 1;
				}

				set_arg_from_reg(vm86, instr[0], op_size, r);
				update_eflags_szp(vm86, op_size, r);
				regs->eip++;
				break;
			}
			case 0xF6: /* DIV/IDIV/IMUL/MUL al, r/m8 or NEG/NOT r/m8 or TEST r/m8, imm8 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = vm86->regs.eax;
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint8_t r, r1;

				if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_6) {
					/* DIV */
					r = RX(a1) / a2;
					r1 = RX(a1) % a2;
				} else if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_7) {
					/* IDIV */
					r = (int16_t)RX(a1) / (int8_t)a2;
					r1 = (int16_t)RX(a1) % (int8_t)a2;
				} else if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_5) {
					/* IMUL */
					uint16_t mul_r = (int16_t)(int8_t)RL(a1) * (int8_t)a2;

					r = mul_r & 0xFFu;
					r1 = mul_r >> 8;
					SETUNSETF((int8_t)r != (int16_t)mul_r, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
				} else if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_4) {
					/* MUL */
					uint16_t mul_r = (uint16_t)RL(a1) * a2;

					r = mul_r & 0xFFu;
					r1 = mul_r >> 8;
					SETUNSETF(r1, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
				} else if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_3) {
					/* NEG r/m8 */
					const uint8_t a_1 = a2;
					uint8_t r;
					int cond;

					cond = a_1 != 0;
					r = -a_1;

					set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
					update_eflags_szp_u8(vm86, r);
					SETUNSETF(cond, regs->eflags, VM86_EFLAGS_CF);
					UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF);
					update_eflags_szp_u8(vm86, r);
					regs->eip += 1 + rm_len;
					break;
				} else if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_2) {
					/* NOT r/m8 */
					const uint8_t a_1 = a2;

					r = ~a_1;

					set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
					regs->eip += 1 + rm_len;
					break;
				} else if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_0) {
					/* TEST r/m8, imm8 */
					const uint8_t a_1 = a2;
					const uint8_t a_2 = read_mem_byte(vm86, phys_addr + 1 + rm_len);

					r = a_1 & a_2;

					UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
					update_eflags_szp_u8(vm86, r);
					regs->eip += 1 + rm_len + 1;
					break;
				}
				else
				{
					trigger_excp(vm86, VM86_EXCP_UD, 0);
					break;
				}

				RL(vm86->regs.eax) = r;
				RH(vm86->regs.eax) = r1;
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xF7: /* DIV/IDIV/IMUL/MUL ax/eax, r/m16/32 or NEG/NOT r/m16/32 or TEST r/m16/32, imm16/32 */
			{
				int rm_len = 0;
				const uint64_t div_a1 = op_size == 4 ? mb32_to_qword(RE(vm86->regs.edx), RE(vm86->regs.eax)) : mb16_to_dword(RX(vm86->regs.edx), RX(vm86->regs.eax));
				const uint32_t mul_a1 = op_size == 4 ? RE(vm86->regs.eax) : RX(vm86->regs.eax);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r, r1;
				int leave = 0;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					case VM86_OP_REG_6: /* DIV */
					{
						if (op_size == 2)
						{
							RX(r) = (uint32_t)div_a1 / RX(a2);
							RX(r1) = (uint32_t)div_a1 % RX(a2);
						}
						else
						{
							RE(r) = div_a1 / RE(a2);
							RE(r1) = div_a1 % RE(a2);
						}
						break;
					}
					case VM86_OP_REG_7: /* IDIV */
					{
						if (op_size == 2)
						{
							RX(r) = (int32_t)div_a1 / (int16_t)RX(a2);
							RX(r1) = (int32_t)div_a1 % (int16_t)RX(a2);
						}
						else
						{
							RE(r) = (int64_t)div_a1 / (int32_t)RE(a2);
							RE(r1) = (int64_t)div_a1 % (int32_t)RE(a2);
						}
						break;
					}
					case VM86_OP_REG_5: /* IMUL */
					{
						if (op_size == 2)
						{
							uint32_t mul_r = (int32_t)mul_a1 * (int16_t)RX(a2);

							RX(r) = mul_r & 0xFFFFu;
							RX(r1) = mul_r >> 16;
							SETUNSETF((int16_t)RX(r) != (int32_t)mul_r, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
						}
						else
						{
							uint64_t mul_r = (int64_t)mul_a1 * (int32_t)RE(a2);

							RE(r) = mul_r & 0xFFFFFFFFu;
							RE(r1) = mul_r >> 32;
							SETUNSETF((int32_t)RE(r) != (int64_t)mul_r, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
						}
						break;
					}
					case VM86_OP_REG_4: /* MUL */
					{
						if (op_size == 2)
						{
							uint32_t mul_r = mul_a1 * RX(a2);

							RX(r) = mul_r & 0xFFFFu;
							RX(r1) = mul_r >> 16;
							SETUNSETF(RX(r1), regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
						}
						else
						{
							uint64_t mul_r = mul_a1 * RE(a2);

							RE(r) = mul_r & 0xFFFFFFFFu;
							RE(r1) = mul_r >> 32;
							SETUNSETF(RE(r1), regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
						}
						break;
					}
					case VM86_OP_REG_3: /* NEG r/m16/32 */
					{
						const union vm86_gpr a_1 = a2;
						union vm86_gpr r;
						int cond;

						if (op_size == 2)
						{
							cond = RX(a_1) != 0;
							RX(r) = -RX(a_1);
						}
						else
						{
							cond = RE(a_1) != 0;
							RE(r) = -RE(a_1);
						}

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
						UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF);
						SETUNSETF(cond, regs->eflags, VM86_EFLAGS_CF);
						update_eflags_szp(vm86, op_size, r);
						regs->eip += 1 + rm_len;
						leave = 1;
						break;
					}
					case VM86_OP_REG_2: /* NOT r/m16/32 */
					{
						const union vm86_gpr a_1 = a2;
						union vm86_gpr r;

						if (op_size == 2)
							RX(r) = ~RX(a_1);
						else
							RE(r) = ~RE(a_1);

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
						regs->eip += 1 + rm_len;
						leave = 1;
						break;
					}
					case VM86_OP_REG_0: /* TEST r/m16/32, imm16/32 */
					{
						const union vm86_gpr a_1 = a2;
						const union vm86_gpr a_2 = read_mem(vm86, phys_addr + 1 + rm_len, op_size);
						union vm86_gpr r;

						if (op_size == 2)
							RX(r) = RX(a_1) & RX(a_2);
						else
							RE(r) = RE(a_1) & RE(a_2);

						UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
						update_eflags_szp(vm86, op_size, r);
						regs->eip += 1 + rm_len + op_size;
						leave = 1;
						break;
					}
					case VM86_OP_REG_1:
					{
						trigger_excp(vm86, VM86_EXCP_UD, 0);
						leave = 1;
						break;
					}
				}

				if (leave)
					break;

				if (op_size == 2)
				{
					RX(vm86->regs.eax) = RX(r);
					RX(vm86->regs.edx) = RX(r1);
				}
				else
				{
					RE(vm86->regs.eax) = RE(r);
					RE(vm86->regs.edx) = RE(r1);
				}

				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xF4: /* HLT */
			{
				/* Ignore. */
				regs->eip++;
				break;
			}
			case 0x69: /* IMUL r16/32, r/m16/32, imm16/32 */
			case 0x6B: /* IMUL r16/32, r/m16/32, imm8 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = (instr[0] == 0x69)
						? read_mem(vm86, phys_addr + 1 + rm_len, op_size)
						/* a2 is sign-extended if imm8 */
						: VM86_GPR((int32_t)(int8_t)read_mem_byte(vm86, phys_addr + 1 + rm_len));
				union vm86_gpr r;

				if (op_size == 2)
				{
					int32_t tmp_r = (int32_t)(int16_t)RX(a1) * (int16_t)RX(a2);

					RX(r) = (uint16_t)tmp_r;
					SETUNSETF((int16_t)RX(r) != tmp_r, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
				}
				else
				{
					int64_t tmp_r = (int64_t)(int32_t)RE(a1) * (int32_t)RE(a2);

					RE(r) = (uint32_t)tmp_r;
					SETUNSETF((int32_t)RE(r) != tmp_r, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
				}

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len + (instr[0] == 0x69 ? op_size : 1);
				break;
			}
			case 0xE4: /* IN al, imm8 */
			case 0xE5: /* IN ax/eax, imm8 */
			case 0xEC: /* IN al, dx */
			case 0xED: /* IN ax/eax, dx */
			{
				const uint16_t a1 = (instr[0] == 0xE4 || instr[0] == 0xE5) ? read_mem_byte(vm86, phys_addr + 1) : RX(regs->edx);

				if (instr[0] == 0xE4 || instr[0] == 0xEC)
					RL(regs->eax) = _inb(a1);
				else if (op_size == 2)
					RX(regs->eax) = _inw(a1);
				else
					RE(regs->eax) = _inl(a1);

				regs->eip += 1 + ((instr[0] == 0xE4 || instr[0] == 0xE5) ? 1 : 0);
				break;
			}
			case 0xCC: /* INT3 */
			{
				regs->eip++;
				trigger_int(vm86, VM86_EXCP_BP);
				break;
			}
			case 0xCD: /* INT x */
			{
				regs->eip += 1 + 1;
				trigger_int(vm86, instr[1]);
				break;
			}
			case 0xCE: /* INTO */
			{
				regs->eip++;
				if (vm86->regs.eflags & VM86_EFLAGS_OF)
					trigger_int(vm86, VM86_EXCP_OF);
				break;
			}
			case 0xCF: /* IRET/IRETD */
			{
				if (op_size == 2)
				{
					vm86->regs.eip = pop_word(vm86);
					vm86->regs.cs = pop_word(vm86);
					vm86->regs.eflags = pop_word(vm86);
				}
				else
				{
					vm86->regs.eip = pop_dword(vm86);
					vm86->regs.cs = pop_dword(vm86) & 0xFFFF;
					vm86->regs.eflags = pop_dword(vm86);
				}
				break;
			}
			case 0x70: /* JO imm8 */
			case 0x71: /* JNO imm8 */
			case 0x72: /* JB/JC/JNAE imm8 */
			case 0x73: /* JNB/JNC/JAE imm8 */
			case 0x74: /* JE/JZ imm8 */
			case 0x75: /* JNE/JNZ imm8 */
			case 0x76: /* JBE/JNA imm8 */
			case 0x77: /* JNBE/JA imm8 */
			case 0x78: /* JS imm8 */
			case 0x79: /* JNS imm8 */
			case 0x7A: /* JP/JPE imm8 */
			case 0x7B: /* JNP/JPO imm8 */
			case 0x7C: /* JL/JNGE imm8 */
			case 0x7D: /* JNL/JGE imm8 */
			case 0x7E: /* JLE/JNG imm8 */
			case 0x7F: /* JNLE/JG imm8 */
			case 0xE3: /* JCXZ/JECXZ imm8 */
			{
				const int8_t a1 = (int8_t)instr[1];
				int cond;

				if (instr[0] == 0xE3)
					cond = op_size == 4 ? !RE(regs->ecx) : !RX(regs->ecx);
				else
					cond = check_cc(vm86, instr[0]);

				regs->eip += 1 + 1;
				if (cond)
					regs->eip += (int32_t)a1;
				break;
			}
			case 0xEB: /* JMP near rel8 */
			case 0xE9: /* JMP near rel16/32 */
			{
				const union vm86_gpr a1 = (instr[0] == 0xE9) ? read_mem(vm86, phys_addr + 1, op_size) : VM86_GPR((int32_t)(int8_t)instr[1]);

				regs->eip += 1 + (instr[0] == 0xE9 ? op_size : 1);
				if (op_size == 2)
					regs->eip += (int16_t)RX(a1);
				else
					regs->eip += (int32_t)RE(a1);
				break;
			}
			case 0xEA: /* JMP far ptr16:16/32 */
			{
				const uint16_t seg = read_mem_word(vm86, phys_addr + 1 + op_size);
				const union vm86_gpr off = read_mem(vm86, phys_addr + 1, op_size);

				regs->eip += 1 + op_size + 2;
				if (op_size == 2)
				{
					regs->cs = seg;
					regs->eip = RX(off);
				}
				else
				{
					regs->cs = seg;
					regs->eip = RE(off);
				}
				break;
			}
			case 0x9F: /* LAHF */
			{
				RH(regs->eax) = regs->eflags & 0xFFu;
				regs->eip++;
				break;
			}
			case 0x8D: /* LEA r16/32, m16/32 */
			case 0xC5: /* LDS r16/32, m16:16/32 */
			case 0xC4: /* LES r16/32, m16:16/32 */
			{
				int rm_len = 0;
				uint16_t seg;
				union vm86_gpr r;

				if (addr_size == 4)
				{
					uint64_t addr = get_arg_realaddr32_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, &rm_len);

					seg = (addr >> 32) & 0xFFFF;
					RE(r) = addr & 0xFFFFFFFF;
				}
				else
				{
					uint32_t addr = get_arg_realaddr16_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, &rm_len);

					seg = addr >> 16;
					RE(r) = addr & 0xFFFF;
				}

				/* No segment for LEA */
				if (instr[0] == 0xC5)
					regs->ds = seg;
				else if (instr[0] == 0xC4)
					regs->es = seg;

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xAC: /* LODSB */
			case 0xAD: /* LODSW/D */
			{
				uint32_t src_addr = realaddr_seg_off_to_phys(segment ? *segment : regs->ds, addr_size == 4 ? RE(regs->esi) : RX(regs->esi));
				int incdec = (instr[0] == 0xAD) ? op_size : 1;

				if (has_rep_prefix)
				{
					printk("TODO: REP LODSB/W/D");
					trigger_excp(vm86, VM86_EXCP_UD, 0);
					break;
				}

				if (regs->eflags & VM86_EFLAGS_DF)
					incdec = -incdec;

				if (instr[0] == 0xAC)
					RL(regs->eax) = read_mem_byte(vm86, src_addr);
				else if (op_size == 2)
					RX(regs->eax) = read_mem_word(vm86, src_addr);
				else
					RE(regs->eax) = read_mem_dword(vm86, src_addr);

				if (addr_size == 2)
					RX(regs->esi) += incdec;
				else
					RE(regs->esi) += incdec;

				regs->eip++;
				break;
			}
			case 0xE2: /* LOOP rel8 */
			case 0xE1: /* LOOPE/LOOPZ rel8 */
			case 0xE0: /* LOOPNE/LOOPNZ rel8 */
			{
				int cond;

				if (addr_size == 4)
					cond = --RE(regs->ecx) != 0;
				else
					cond = --RX(regs->ecx) != 0;

				if (instr[0] == 0xE1)
					cond = cond && (regs->eflags & VM86_EFLAGS_ZF);
				else if (instr[0] == 0xE0)
					cond = cond && !(regs->eflags & VM86_EFLAGS_ZF);

				regs->eip += 1 + 1;
				if (cond)
					regs->eip += (int8_t)instr[1];
				break;
			}
			case 0x88: /* MOV r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len, a2);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x89: /* MOV r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);

				set_arg_from_mod_reg_rm(vm86, a2, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x8A: /* MOV r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, a2);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x8B: /* MOV r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);

				set_arg_from_mod_reg_rm(vm86, a2, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x8C: /* MOV r/m16, sreg */
			{
				int rm_len = 0;
				uint16_t a2;
				int excp = 0;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					/* MOV cs, x is allowed only in real mode */
					case VM86_OP_REG_CS: a2 = regs->cs; break;
					case VM86_OP_REG_DS: a2 = regs->ds; break;
					case VM86_OP_REG_ES: a2 = regs->es; break;
					case VM86_OP_REG_FS: a2 = regs->fs; break;
					case VM86_OP_REG_GS: a2 = regs->gs; break;
					case VM86_OP_REG_SS: a2 = regs->ss; break;
					case VM86_OP_REG_RES0:
					case VM86_OP_REG_RES1:
						trigger_excp(vm86, VM86_EXCP_UD, 0);
						excp = 1;
						break;
				}

				if (!excp)
				{
					set_arg16_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len, a2);
					regs->eip += 1 + rm_len;
				}
				break;
			}
			case 0x8E: /* MOV sreg, r/m16 */
			{
				int rm_len = 0;
				uint16_t a2 = get_arg16_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				int excp = 0;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					/* MOV cs, x is allowed only in real mode */
					case VM86_OP_REG_CS: regs->cs = a2; break;
					case VM86_OP_REG_DS: regs->ds = a2; break;
					case VM86_OP_REG_ES: regs->es = a2; break;
					case VM86_OP_REG_FS: regs->fs = a2; break;
					case VM86_OP_REG_GS: regs->gs = a2; break;
					case VM86_OP_REG_SS: regs->ss = a2; break;
					case VM86_OP_REG_RES0:
					case VM86_OP_REG_RES1:
						trigger_excp(vm86, VM86_EXCP_UD, 0);
						excp = 1;
						break;
				}

				if (!excp)
					regs->eip += 1 + rm_len;
				break;
			}
			case 0xA0: /* MOV al, moffs8 */
			case 0xA1: /* MOV ax/eax, moffs16/32 */
			{
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, addr_size);
				const uint32_t off = addr_size == 4 ? RE(a2) : (uint32_t)RX(a2);
				const uint32_t addr = realaddr_seg_off_to_phys(segment ? *segment : regs->ds, off);

				if (instr[0] == 0xA0)
					RL(regs->eax) = read_mem_byte(vm86, addr);
				else if (op_size == 2)
					RX(regs->eax) = read_mem_word(vm86, addr);
				else
					RE(regs->eax) = read_mem_dword(vm86, addr);
				regs->eip += 1 + addr_size;
				break;
			}
			case 0xA2: /* MOV moffs8, al */
			case 0xA3: /* MOV moffs16/32, ax/eax */
			{
				const union vm86_gpr a1 = read_mem(vm86, phys_addr + 1, addr_size);
				const uint32_t off = addr_size == 4 ? RE(a1) : (uint32_t)RX(a1);
				const uint32_t addr = realaddr_seg_off_to_phys(segment ? *segment : regs->ds, off);

				if (instr[0] == 0xA2)
					write_mem_byte(vm86, addr, RL(regs->eax));
				else if (op_size == 2)
					write_mem_word(vm86, addr, RX(regs->eax));
				else
					write_mem_dword(vm86, addr, RE(regs->eax));
				regs->eip += 1 + addr_size;
				break;
			}
			case 0xB0:
			case 0xB1:
			case 0xB2:
			case 0xB3:
			case 0xB4:
			case 0xB5:
			case 0xB6:
			case 0xB7: /* MOV reg8, imm8 */
			{
				const uint8_t a2 = instr[1];

				set_arg8_from_reg(vm86, instr[0], a2);
				regs->eip += 1 + 1;
				break;
			}
			case 0xB8:
			case 0xB9:
			case 0xBA:
			case 0xBB:
			case 0xBC:
			case 0xBD:
			case 0xBE:
			case 0xBF: /* MOV reg16/32, imm16/32 */
			{
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);

				set_arg_from_reg(vm86, instr[0], op_size, a2);
				regs->eip += 1 + op_size;
				break;
			}
			case 0xC6: /* MOV r/m8, imm8 */
			{
				int rm_len = 0;
				/* Get rm_len */
				get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = read_mem_byte(vm86, phys_addr + 1 + rm_len);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, a2);
				regs->eip += 1 + rm_len + 1;
				break;
			}
			case 0xC7: /* MOV r/m16/32, imm16/32 */
			{
				int rm_len = 0;
				/* Get rm_len */
				get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1 + rm_len, op_size);

				set_arg_from_mod_reg_rm(vm86, a2, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				regs->eip += 1 + rm_len + op_size;
				break;
			}
			case 0xA4: /* MOVSB */
			case 0xA5: /* MOVSW/D */
			{
				uint32_t counter = addr_size == 4 ? RE(regs->ecx) : (uint32_t)RX(regs->ecx);
				uint32_t dst_addr = realaddr_seg_off_to_phys(regs->es, addr_size == 4 ? RE(regs->edi) : RX(regs->edi));
				uint32_t src_addr = realaddr_seg_off_to_phys(segment ? *segment : regs->ds, addr_size == 4 ? RE(regs->esi) : RX(regs->esi));
				int incdec = (instr[0] == 0xA5) ? op_size : 1;

				if (has_rep_prefix && counter == 0)
				{
					regs->eip++;
					break;
				}

				if (regs->eflags & VM86_EFLAGS_DF)
					incdec = -incdec;

				if (instr[0] == 0xA4)
					write_mem_byte(vm86, dst_addr, read_mem_byte(vm86, src_addr));
				else if (op_size == 2)
					write_mem_word(vm86, dst_addr, read_mem_word(vm86, src_addr));
				else
					write_mem_dword(vm86, dst_addr, read_mem_dword(vm86, src_addr));

				if (addr_size == 2)
				{
					if (has_rep_prefix)
						RX(regs->ecx) = RX(regs->ecx) - 1;
					RX(regs->esi) += incdec;
					RX(regs->edi) += incdec;
				}
				else
				{
					if (has_rep_prefix)
						RE(regs->ecx) = RE(regs->ecx) - 1;
					RE(regs->esi) += incdec;
					RE(regs->edi) += incdec;
				}

				if (has_rep_prefix)
					regs->eip = cur_instr_eip;
				else
					regs->eip++;
				break;
			}
			case 0x90: /* NOP/PAUSE (SSE2) */
			{
				/* PAUSE == REP NOP */
				regs->eip++;
				break;
			}
			case 0x0C: /* OR al, imm8 */
			{
				RL(regs->eax) |= instr[1];
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, RL(regs->eax));
				regs->eip += 2;
				break;
			}
			case 0x0D: /* OR ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r;

				r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) | RX(a2);
				else
					RE(r) = RE(a1) | RE(a2);

				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eax = r;
				regs->eip += 1 + op_size;
				break;
			}
			case 0x08: /* OR r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint8_t r = a1 | a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x09: /* OR r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) | RX(a2);
				else
					RE(r) = RE(a1) | RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x0A: /* OR r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint8_t r = a1 | a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, r);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x0B: /* OR r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) | RX(a2);
				else
					RE(r) = RE(a1) | RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xE6: /* OUT imm8, al */
			case 0xE7: /* OUT imm8, ax/eax */
			case 0xEE: /* OUT dx, al */
			case 0xEF: /* OUT dx, ax/eax */
			{
				const uint16_t a1 = (instr[0] == 0xE6 || instr[0] == 0xE7) ? read_mem_byte(vm86, phys_addr + 1) : RX(regs->edx);

				if (instr[0] == 0xE6 || instr[0] == 0xEE)
					_outb(a1, RL(regs->eax));
				else if (op_size == 2)
					_outw(a1, RX(regs->eax));
				else
					_outl(a1, RE(regs->eax));
				regs->eip += 1 + ((instr[0] == 0xE6 || instr[0] == 0xE7) ? 1 : 0);
				break;
			}
			case 0x8F: /* POP m16/32 */
			{
				int rm_len = 0;
				union vm86_gpr r;

				if ((instr[1] & VM86_OP_REG_MASK) == VM86_OP_REG_0)
				{
					/* POP */
					if (op_size == 2)
						RX(r) = pop_word(vm86);
					else
						RE(r) = pop_dword(vm86);
				}
				else
				{
					trigger_excp(vm86, VM86_EXCP_UD, 0);
					break;
				}

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x58:
			case 0x59:
			case 0x5A:
			case 0x5B:
			case 0x5C:
			case 0x5D:
			case 0x5E:
			case 0x5F: /* POP reg16/32 */
			{
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = pop_word(vm86);
				else
					RE(r) = pop_dword(vm86);

				set_arg_from_reg(vm86, instr[0], op_size, r);
				regs->eip++;
				break;
			}
			case 0x1F: /* POP ds */
			{
				regs->ds = pop_word(vm86);
				regs->eip++;
				break;
			}
			case 0x07: /* POP es */
			{
				regs->es = pop_word(vm86);
				regs->eip++;
				break;
			}
			case 0x17: /* POP ss */
			{
				regs->ss = pop_word(vm86);
				regs->eip++;
				break;
			}
			case 0x9D: /* POPF/POPFD */
			{
				if (op_size == 2)
					regs->eflags = pop_word(vm86);
				else
					regs->eflags = pop_dword(vm86);
				regs->eip++;
				break;
			}
			case 0x50:
			case 0x51:
			case 0x52:
			case 0x53:
			case 0x54:
			case 0x55:
			case 0x56:
			case 0x57: /* PUSH reg16/32 */
			{
				const union vm86_gpr a1 = get_arg_from_reg(vm86, instr[0], op_size);

				if (op_size == 2)
					push_word(vm86, RX(a1));
				else
					push_dword(vm86, RE(a1));

				regs->eip++;
				break;
			}
			case 0x0E: /* PUSH cs */
			{
				push_word(vm86, regs->cs);
				regs->eip++;
				break;
			}
			case 0x16: /* PUSH ss */
			{
				push_word(vm86, regs->ss);
				regs->eip++;
				break;
			}
			case 0x1E: /* PUSH ds */
			{
				push_word(vm86, regs->ds);
				regs->eip++;
				break;
			}
			case 0x06: /* PUSH es */
			{
				push_word(vm86, regs->es);
				regs->eip++;
				break;
			}
			case 0x9C: /* PUSHF/PUSHFD */
			{
				if (op_size == 2)
					push_word(vm86, regs->eflags);
				else
					push_dword(vm86, regs->eflags);
				regs->eip++;
				break;
			}
			case 0xD0: /* RCL/RCR/ROL/ROR/SAL/SHL/SAR/SHR r/m8, 1 */
			case 0xD2: /* RCL/RCR/ROL/ROR/SAL/SHL/SAR/SHR r/m8, cl */
			case 0xC0: /* RCL/RCR/ROL/ROR/SAL/SHL/SAR/SHR r/m8, imm8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = (instr[0] == 0xD0) ? 1 : (instr[0] == 0xD2 ? RL(regs->ecx) : read_mem_byte(vm86, phys_addr + 1 + rm_len));
				const uint8_t shift = a2 & 0x1F;
				const uint8_t rot9 = shift % 9;
				uint8_t r;
				int condc = 0;
				int condo = 0;
				int upd_szp = 0;
				int leave = 0;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					case VM86_OP_REG_2: /* RCL */
					{
						uint16_t val = a1 | (!!(regs->eflags & VM86_EFLAGS_CF) << 8);

						val = (val << rot9) | (val >> (9 - rot9));
						r = val & 0xFF;
						condc = (val >> 8) & 1;
						condo = (r >> 7) ^ condc;
						break;
					}
					case VM86_OP_REG_3: /* RCR */
					{
						uint16_t val = a1 | (!!(regs->eflags & VM86_EFLAGS_CF) << 8);

						condo = (a1 >> 7) ^ (val >> 8);
						val = (val >> rot9) | (val << (9 - rot9));
						r = val & 0xFF;
						condc = (val >> 8) & 1;
						break;
					}
					case VM86_OP_REG_0: /* ROL */
					{
						r = (a1 << shift) | (a1 >> (8 - shift));
						condc = r & 1;
						condo = (r >> 7) ^ condc;
						break;
					}
					case VM86_OP_REG_1: /* ROR */
					{
						r = (a1 >> shift) | (a1 << (8 - shift));
						condc = r >> 7;
						condo = ((a1 >> 6) & 1) ^ condc;
						break;
					}
					case VM86_OP_REG_4: /* SAL/SHL */
					{
						r = a1 << (shift - 1);
						condc = r >> 7;
						r <<= 1;
						condo = (r >> 7) ^ condc;
						upd_szp = 1;
						break;
					}
					case VM86_OP_REG_7: /* SAR */
					{
						int8_t sa1 = (int8_t)a1;

						sa1 >>= shift - 1;
						condc = a1 & 1;
						r = sa1 >> 1;
						upd_szp = 1;
						break;
					}
					case VM86_OP_REG_5: /* SHR */
					{
						r = a1 >> (shift - 1);
						condc = r & 1;
						r >>= 1;
						condo = a1 >> 7;
						upd_szp = 1;
						break;
					}
					case VM86_OP_REG_6:
					{
						trigger_excp(vm86, VM86_EXCP_UD, 0);
						leave = 1;
						break;
					}
				}

				if (leave)
					break;

				if (shift)
				{
					SETUNSETF(condo, regs->eflags, VM86_EFLAGS_OF);
					SETUNSETF(condc, regs->eflags, VM86_EFLAGS_CF);
					if (upd_szp)
						update_eflags_szp_u8(vm86, r);
				}

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				regs->eip += 1 + rm_len + (instr[0] == 0xC0);
				break;
			}
			case 0xD1: /* RCL/RCR/ROL/ROR/SAL/SHL/SAR/SHR r/m16/32, 1 */
			case 0xD3: /* RCL/RCR/ROL/ROR/SAL/SHL/SAR/SHR r/m16/32, cl */
			case 0xC1: /* RCL/RCR/ROL/ROR/SAL/SHL/SAR/SHR r/m16/32, imm8 */
			{

				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const uint8_t a2 = (instr[0] == 0xD1) ? 1 : (instr[0] == 0xD3 ? RL(regs->ecx) : read_mem_byte(vm86, phys_addr + 1 + rm_len));
				const uint8_t shift = a2 & 0x1F;
				const uint8_t rot17 = shift % 17;
				union vm86_gpr r;
				int condc = 0;
				int condo = 0;
				int upd_szp = 0;
				int leave = 0;

				switch (instr[1] & VM86_OP_REG_MASK)
				{
					case VM86_OP_REG_2: /* RCL */
					{
						if (op_size == 2)
						{
							uint32_t val = RX(a1) | (!!(regs->eflags & VM86_EFLAGS_CF) << 16);

							val = (val << rot17) | (val >> (17 - rot17));
							RX(r) = val & 0xFFFF;
							condc = (val >> 16) & 1;
							condo = (RX(r) >> 15) ^ condc;
						}
						else
						{
							uint64_t val = RE(a1) | ((uint64_t)!!(regs->eflags & VM86_EFLAGS_CF) << 32);

							val = (val << shift) | (val >> (33 - shift));
							RE(r) = val & 0xFFFFFFFF;
							condc = (val >> 32) & 1;
							condo = (RE(r) >> 31) ^ condc;
						}
						break;
					}
					case VM86_OP_REG_3: /* RCR */
					{
						if (op_size == 2)
						{
							uint32_t val = RX(a1) | (!!(regs->eflags & VM86_EFLAGS_CF) << 16);

							condo = (RX(a1) >> 15) ^ (val >> 16);
							val = (val >> rot17) | (val << (17 - rot17));
							RX(r) = val & 0xFFFF;
							condc = (val >> 16) & 1;
						}
						else
						{
							uint64_t val = RE(a1) | ((uint64_t)!!(regs->eflags & VM86_EFLAGS_CF) << 32);

							condo = (RE(a1) >> 31) ^ (val >> 32);
							val = (val >> shift) | (val << (33 - shift));
							RE(r) = val & 0xFFFFFFFF;
							condc = (val >> 32) & 1;
						}
						break;
					}
					case VM86_OP_REG_0: /* ROL */
					{
						if (op_size == 2)
						{
							RX(r) = (RX(a1) << shift) | (RX(a1) >> (16 - shift));
							condc = RX(r) & 1;
							condo = (RX(r) >> 15) ^ condc;
						}
						else
						{
							RE(r) = (RE(a1) << shift) | (RE(a1) >> (32 - shift));
							condc = RE(r) & 1;
							condo = (RE(r) >> 31) ^ condc;
						}
						break;
					}
					case VM86_OP_REG_1: /* ROR */
					{
						if (op_size == 2)
						{
							RX(r) = (RX(a1) >> shift) | (RX(a1) << (16 - shift));
							condc = RX(r) >> 15;
							condo = ((RX(a1) >> 14) & 1) ^ condc;
						}
						else
						{
							RE(r) = (RE(a1) >> shift) | (RE(a1) << (32 - shift));
							condc = RE(r) >> 31;
							condo = ((RE(a1) >> 30) & 1) ^ condc;
						}
						break;
					}
					case VM86_OP_REG_4: /* SAL/SHL */
					{
						if (op_size == 2)
						{
							RX(r) = RX(a1) << (shift - 1);
							condc = RX(r) >> 15;
							RX(r) <<= 1;
							condo = (RX(r) >> 15) ^ condc;
						}
						else
						{
							RE(r) = RE(a1) << (shift - 1);
							condc = RE(r) >> 31;
							RE(r) <<= 1;
							condo = (RE(r) >> 31) ^ condc;
						}
						upd_szp = 1;
						break;
					}
					case VM86_OP_REG_7: /* SAR */
					{
						if (op_size == 2)
						{
							int16_t sa1 = (int8_t)RX(a1);

							condc = RX(a1) & 1;
							sa1 >>= shift - 1;
							RX(r) = sa1 >> 1;
						}
						else
						{
							int32_t sa1 = (int32_t)RE(a1);

							condc = RX(a1) & 1;
							sa1 >>= shift - 1;
							RE(r) = sa1 >> 1;
						}
						upd_szp = 1;
						break;
					}
					case VM86_OP_REG_5: /* SHR */
					{
						if (op_size == 2)
						{
							RX(r) = RX(a1) >> (shift - 1);
							condc = RX(r) & 1;
							RX(r) >>= 1;
							condo = RX(a1) >> 7;
						}
						else
						{
							RE(r) = RE(a1) >> (shift - 1);
							condc = RE(r) & 1;
							RE(r) >>= 1;
							condo = RE(a1) >> 7;
						}
						upd_szp = 1;
						break;
					}
					case VM86_OP_REG_6:
					{
						trigger_excp(vm86, VM86_EXCP_UD, 0);
						leave = 1;
						break;
					}
				}

				if (leave)
					break;

				if (shift)
				{
					SETUNSETF(condo, regs->eflags, VM86_EFLAGS_OF);
					SETUNSETF(condc, regs->eflags, VM86_EFLAGS_CF);
					if (upd_szp)
						update_eflags_szp(vm86, op_size, r);
				}

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				regs->eip += 1 + rm_len + (instr[0] == 0xC1);
				break;
			}
			case 0xC3: /* RETN */
			case 0xC2: /* RETN imm16 */
			{
				if (op_size == 2)
					regs->eip = pop_word(vm86);
				else
					regs->eip = pop_dword(vm86);

				if (instr[0] == 0xC2)
					RE(regs->esp) += read_mem_word(vm86, phys_addr + 1);
				break;
			}
			case 0xCB: /* RETF */
			case 0xCA: /* RETF imm16 */
			{
				if (op_size == 2)
				{
					regs->eip = pop_word(vm86);
					regs->cs = pop_word(vm86);
				}
				else
				{
					regs->eip = pop_dword(vm86);
					regs->cs = pop_word(vm86);
				}

				if (instr[0] == 0xCA)
					RE(regs->esp) += read_mem_word(vm86, phys_addr + 1);
				break;
			}
			case 0x9E: /* SAHF */
			{
				regs->eflags = (regs->eflags & 0xFFFFFF00u) | RH(regs->eax);
				regs->eip++;
				break;
			}
			case 0x1C: /* SBB al, imm8 */
			{
				const uint8_t a1 = RL(regs->eax);
				const uint8_t a2 = instr[1];
				uint8_t r = a1 - a2 - !!(regs->eflags & VM86_EFLAGS_CF);

				RL(regs->eax) = r;
				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 2;
				break;
			}
			case 0x1D: /* SBB ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2) - !!(regs->eflags & VM86_EFLAGS_CF);
				else
					RE(r) = RE(a1) - RE(a2) - !!(regs->eflags & VM86_EFLAGS_CF);

				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eax = r;
				regs->eip += 1 + op_size;
				break;
			}
			case 0x18: /* SBB r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint8_t r = a1 - a2 - !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x19: /* SBB r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2) - !!(regs->eflags & VM86_EFLAGS_CF);
				else
					RE(r) = RE(a1) - RE(a2) - !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x1A: /* SBB r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint8_t r = a1 - a2 - !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, r);
				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x1B: /* SBB r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2) - !!(regs->eflags & VM86_EFLAGS_CF);
				else
					RE(r) = RE(a1) - RE(a2) - !!(regs->eflags & VM86_EFLAGS_CF);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xAE: /* SCASB */
			case 0xAF: /* SCASW/D */
			{
				uint32_t counter = addr_size == 4 ? RE(regs->ecx) : (uint32_t)RX(regs->ecx);
				uint32_t src_addr = realaddr_seg_off_to_phys(regs->es, addr_size == 4 ? RE(regs->edi) : RX(regs->edi));
				int incdec = (instr[0] == 0xAF) ? op_size : 1;

				if (has_rep_any_prefix && counter == 0)
				{
					regs->eip++;
					break;
				}

				if (regs->eflags & VM86_EFLAGS_DF)
					incdec = -incdec;

				if (instr[0] == 0xAE)
				{
					uint8_t a1 = RL(regs->eax);
					uint8_t a2 = read_mem_byte(vm86, src_addr);
					uint8_t r = a1 - a2;

					update_eflags_oac_sub_u8(vm86, r, a1, a2);
					update_eflags_szp_u8(vm86, r);
				}
				else
				{
					const union vm86_gpr a1 = regs->eax;
					const union vm86_gpr a2 = read_mem(vm86, src_addr, op_size);
					union vm86_gpr r;

					if (op_size == 2)
						RX(r) = RX(a1) - RX(a2);
					else
						RE(r) = RE(a1) - RE(a2);

					update_eflags_oac_sub(vm86, op_size, r, a1, a2);
					update_eflags_szp(vm86, op_size, r);
				}

				if (addr_size == 2)
				{
					if (has_rep_any_prefix)
						RX(regs->ecx) = RX(regs->ecx) - 1;
					RX(regs->edi) += incdec;
				}
				else
				{
					if (has_rep_any_prefix)
						RE(regs->ecx) = RE(regs->ecx) - 1;
					RE(regs->edi) += incdec;
				}

				if ((has_rep_prefix && (regs->eflags & VM86_EFLAGS_ZF))
						|| (has_repnz_prefix && !(regs->eflags & VM86_EFLAGS_ZF)))
					regs->eip = cur_instr_eip;
				else
					regs->eip++;
				break;
			}
			case 0xF9: /* STC */
			{
				SETF(vm86->regs.eflags, VM86_EFLAGS_CF);
				regs->eip++;
				break;
			}
			case 0xFD: /* STD */
			{
				SETF(vm86->regs.eflags, VM86_EFLAGS_DF);
				regs->eip++;
				break;
			}
			case 0xFB: /* STI */
			{
				SETF(vm86->regs.eflags, VM86_EFLAGS_IF);
				regs->eip++;
				break;
			}
			case 0xAA: /* STOSB */
			case 0xAB: /* STOSW/D */
			{
				uint32_t counter = addr_size == 4 ? RE(regs->ecx) : (uint32_t)RX(regs->ecx);
				uint32_t dst_addr = realaddr_seg_off_to_phys(regs->es, addr_size == 4 ? RE(regs->edi) : RX(regs->edi));
				int incdec = (instr[0] == 0xAB) ? op_size : 1;

				if (has_rep_prefix && counter == 0)
				{
					regs->eip++;
					break;
				}

				if (regs->eflags & VM86_EFLAGS_DF)
					incdec = -incdec;

				if (instr[0] == 0xAA)
					write_mem_byte(vm86, dst_addr, RL(regs->eax));
				else if (op_size == 2)
					write_mem_word(vm86, dst_addr, RX(regs->eax));
				else
					write_mem_dword(vm86, dst_addr, RE(regs->eax));

				if (addr_size == 2)
				{
					if (has_rep_prefix)
						RX(regs->ecx) = RX(regs->ecx) - 1;
					RX(regs->edi) += incdec;
				}
				else
				{
					if (has_rep_prefix)
						RE(regs->ecx) = RE(regs->ecx) - 1;
					RE(regs->edi) += incdec;
				}

				if (has_rep_prefix)
					regs->eip = cur_instr_eip;
				else
					regs->eip++;
				break;
			}
			case 0x2C: /* SUB al, imm8 */
			{
				const uint8_t a1 = RL(regs->eax);
				const uint8_t a2 = instr[1];
				uint8_t r = a1 - a2;

				RL(regs->eax) = r;
				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 2;
				break;
			}
			case 0x2D: /* SUB ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2);
				else
					RE(r) = RE(a1) - RE(a2);

				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eax = r;
				regs->eip += 1 + op_size;
				break;
			}
			case 0x28: /* SUB r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint16_t r = a1 - a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x29: /* SUB r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2);
				else
					RE(r) = RE(a1) - RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x2A: /* SUB r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint16_t r = a1 - a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, r);
				update_eflags_oac_sub_u8(vm86, r, a1, a2);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x2B: /* SUB r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) - RX(a2);
				else
					RE(r) = RE(a1) - RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				update_eflags_oac_sub(vm86, op_size, r, a1, a2);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x9B: /* WAIT */
			{
				/* Ignore. */
				regs->eip++;
				break;
			}
			/* case 0x90: */ /* XCHG ax/eax, ax/eax == NOP */
			case 0x91:
			case 0x92:
			case 0x93:
			case 0x94:
			case 0x95:
			case 0x96:
			case 0x97: /* XCHG ax/eax, reg16/32 */
			{
				if (op_size == 2)
				{
					uint16_t a1 = get_arg16_from_reg(vm86, instr[0]);
					set_arg16_from_reg(vm86, instr[0], RX(regs->eax));
					RX(regs->eax) = a1;
				}
				else
				{
					uint32_t a1 = get_arg32_from_reg(vm86, instr[0]);
					set_arg32_from_reg(vm86, instr[0], RE(regs->eax));
					RE(regs->eax) = a1;
				}

				regs->eip++;
				break;
			}
			case 0x86: /* XCHG r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, a2);
				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, a1);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x87: /* XCHG r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);

				set_arg_from_mod_reg_rm(vm86, a2, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				set_arg_from_mod_reg_rm(vm86, a1, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0xD7: /* XLATB */
			{
				RL(regs->eax) = read_mem_byte(vm86, realaddr_seg_off_to_phys(segment ? *segment : regs->ds, (addr_size == 4 ? RE(regs->ebx) : RX(regs->ebx)) + RL(regs->eax)));
				regs->eip++;
				break;
			}
			case 0x34: /* XOR al, imm8 */
			{
				RL(regs->eax) ^= instr[1];
				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, RL(regs->eax));
				regs->eip += 1 + 1;
				break;
			}
			case 0x35: /* XOR ax/eax, imm16/32 */
			{
				const union vm86_gpr a1 = regs->eax;
				const union vm86_gpr a2 = read_mem(vm86, phys_addr + 1, op_size);
				union vm86_gpr r;

				r = regs->eax;

				if (op_size == 2)
					RX(r) = RX(a1) ^ RX(a2);
				else
					RE(r) = RE(a1) ^ RE(a2);

				UNSETF(vm86->regs.eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eax = r;
				regs->eip += 1 + op_size;
				break;
			}
			case 0x30: /* XOR r/m8, r8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				uint8_t r = a1 ^ a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, NULL, r);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x31: /* XOR r/m16/32, r16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) ^ RX(a2);
				else
					RE(r) = RE(a1) ^ RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 0, addr_size, op_size, NULL);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x32: /* XOR r8, r/m8 */
			{
				int rm_len = 0;
				const uint8_t a1 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL);
				const uint8_t a2 = get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, &rm_len);
				uint8_t r = a1 ^ a2;

				set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, NULL, r);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp_u8(vm86, r);
				regs->eip += 1 + rm_len;
				break;
			}
			case 0x33: /* XOR r16/32, r/m16/32 */
			{
				int rm_len = 0;
				const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 1, 0, addr_size, op_size, &rm_len);
				union vm86_gpr r;

				if (op_size == 2)
					RX(r) = RX(a1) ^ RX(a2);
				else
					RE(r) = RE(a1) ^ RE(a2);

				set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 1, 1, addr_size, op_size, NULL);
				UNSETF(regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_AF | VM86_EFLAGS_CF);
				update_eflags_szp(vm86, op_size, r);
				regs->eip += 1 + rm_len;
				break;
			}

			/* 80186 */

			case 0x62: /* BOUND xxxx */
			{
				printk("TODO: BOUND");
				trigger_excp(vm86, VM86_EXCP_UD, 0);
				regs->eip += 0;
				break;
			}
			case 0xC8: /* ENTER imm16, imm8 */
			{
				uint16_t a1 = read_mem_word(vm86, phys_addr + 1);
				uint8_t a2 = read_mem_byte(vm86, phys_addr + 1 + 2);
				union vm86_gpr first_frame;

				a2 &= 0x1F;

				push(vm86, op_size, regs->ebp);
				first_frame = regs->esp;

				for (uint8_t nest_lvl = 1; nest_lvl < a2; nest_lvl++)
				{
					RX(regs->ebp) -= op_size;
					push(vm86, op_size, read_mem(vm86, realaddr_seg_off_to_phys(regs->ss, RX(regs->ebp)), op_size));
				}

				if (a2 >= 1)
					push(vm86, op_size, first_frame);

				if (op_size == 2)
				{
					RX(regs->ebp) = RX(first_frame);
					RX(regs->esp) -= a1;
				}
				else
				{
					RE(regs->ebp) = RE(first_frame);
					RE(regs->esp) -= a1;
				}

				regs->eip += 1 + 2 + 1;
				break;
			}
			case 0x6C: /* INSB */
			case 0x6D: /* INSW/D */
			{
				printk("TODO: INSB/W/D");
				trigger_excp(vm86, VM86_EXCP_UD, 0);
				regs->eip += 0;
				break;
			}
			case 0xC9: /* LEAVE */
			{
				RX(regs->esp) = RX(regs->ebp);

				if (op_size == 4)
					RE(regs->ebp) = pop_dword(vm86);
				else
					RX(regs->ebp) = pop_word(vm86);

				regs->eip++;
				break;
			}
			case 0x6E: /* OUTSB */
			case 0x6F: /* OUTSW/D */
			{
				printk("TODO: OUTSB/W/D");
				trigger_excp(vm86, VM86_EXCP_UD, 0);
				regs->eip += 0;
				break;
			}
			case 0x61: /* POPA/POPAD */
			{
				if (op_size == 2)
				{
					RX(regs->edi) = pop_word(vm86);
					RX(regs->esi) = pop_word(vm86);
					RX(regs->ebp) = pop_word(vm86);
					pop_word(vm86);
					RX(regs->ebx) = pop_word(vm86);
					RX(regs->edx) = pop_word(vm86);
					RX(regs->ecx) = pop_word(vm86);
					RX(regs->eax) = pop_word(vm86);
				}
				else
				{
					RE(regs->edi) = pop_dword(vm86);
					RE(regs->esi) = pop_dword(vm86);
					RE(regs->ebp) = pop_dword(vm86);
					pop_dword(vm86);
					RE(regs->ebx) = pop_dword(vm86);
					RE(regs->edx) = pop_dword(vm86);
					RE(regs->ecx) = pop_dword(vm86);
					RE(regs->eax) = pop_dword(vm86);
				}
				regs->eip++;
				break;
			}
			case 0x60: /* PUSHA/PUSHAD */
			{
				if (op_size == 2)
				{
					uint16_t sp_backup = RX(regs->esp);

					push_word(vm86, RX(regs->eax));
					push_word(vm86, RX(regs->ecx));
					push_word(vm86, RX(regs->edx));
					push_word(vm86, RX(regs->ebx));
					push_word(vm86, sp_backup);
					push_word(vm86, RX(regs->ebp));
					push_word(vm86, RX(regs->esi));
					push_word(vm86, RX(regs->edi));
				}
				else
				{
					uint32_t esp_backup = RE(regs->esp);

					push_dword(vm86, RE(regs->eax));
					push_dword(vm86, RE(regs->ecx));
					push_dword(vm86, RE(regs->edx));
					push_dword(vm86, RE(regs->ebx));
					push_dword(vm86, esp_backup);
					push_dword(vm86, RE(regs->ebp));
					push_dword(vm86, RE(regs->esi));
					push_dword(vm86, RE(regs->edi));
				}
				regs->eip++;
				break;
			}
			case 0x6A: /* PUSH imm8 */
			{
				const uint8_t a1 = instr[1];

				if (op_size == 2)
					push_word(vm86, a1);
				else
					push_dword(vm86, a1);

				regs->eip += 1 + 1;
				break;
			}
			case 0x68: /* PUSH imm16/32 */
			{
				union vm86_gpr a1 = read_mem(vm86, phys_addr + 1, op_size);

				push(vm86, op_size, a1);

				regs->eip += 1 + op_size;
				break;
			}

			/* 80286 */

			/* Mostly protected mode/task switching stuff. */

			/* Extended opcodes */

			case 0x0F:
			{
				switch (instr[1])
				{
					/* 8086 */

					case 0xAF: /* IMUL r16/32, r/m16/32 */
					{
						int rm_len = 0;
						const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, op_size, &rm_len);
						union vm86_gpr r;

						if (op_size == 2)
						{
							int32_t tmp_r = (int32_t)(int16_t)RX(a1) * (int16_t)RX(a2);

							RX(r) = (uint16_t)tmp_r;
							SETUNSETF((int16_t)RX(r) != tmp_r, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
						}
						else
						{
							int64_t tmp_r = (int64_t)(int32_t)RE(a1) * (int32_t)RE(a2);

							RE(r) = (uint32_t)tmp_r;
							SETUNSETF((int32_t)RE(r) != tmp_r, regs->eflags, VM86_EFLAGS_OF | VM86_EFLAGS_CF);
						}

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						update_eflags_szp(vm86, op_size, r);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0x80: /* JO imm16/32 */
					case 0x81: /* JNO imm16/32 */
					case 0x82: /* JB/JC/JNAE imm16/32 */
					case 0x83: /* JNB/JNC/JAE imm16/32 */
					case 0x84: /* JE/JZ imm16/32 */
					case 0x85: /* JNE/JNZ imm16/32 */
					case 0x86: /* JBE/JNA imm16/32 */
					case 0x87: /* JNBE/JA imm16/32 */
					case 0x88: /* JS imm16/32 */
					case 0x89: /* JNS imm16/32 */
					case 0x8A: /* JP/JPE imm16/32 */
					case 0x8B: /* JNP/JPO imm16/32 */
					case 0x8C: /* JL/JNGE imm16/32 */
					case 0x8D: /* JNL/JGE imm16/32 */
					case 0x8E: /* JLE/JNG imm16/32 */
					case 0x8F: /* JNLE/JG imm16/32 */
					{
						const union vm86_gpr a1_gpr = read_mem(vm86, phys_addr + 2, op_size);
						const int32_t a1 = op_size == 4 ? (int32_t)RE(a1_gpr) : (int32_t)(int16_t)RX(a1_gpr);

						regs->eip += 1 + 1 + op_size;
						if (check_cc(vm86, instr[1]))
							regs->eip += a1;
						break;
					}
					case 0xA1: /* POP fs */
					{
						regs->fs = pop_word(vm86);
						regs->eip += 1 + 1;
						break;
					}
					case 0xA9: /* POP gs */
					{
						regs->gs = pop_word(vm86);
						regs->eip += 1 + 1;
						break;
					}
					case 0xA0: /* PUSH fs */
					{
						push_word(vm86, regs->fs);
						regs->eip += 1 + 1;
						break;
					}
					case 0xA8: /* PUSH gs */
					{
						push_word(vm86, regs->gs);
						regs->eip += 1 + 1;
						break;
					}

					/* 80386 */

					case 0xBC: /* BSF r16/32, r/m16/32 */
					{
						int rm_len = 0;
						const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, op_size, &rm_len);
						uint32_t val = op_size == 4 ? RE(a2) : RX(a2);
						union vm86_gpr r;
						int condz = 1;

						RE(r) = 0;

						if (val)
						{
							while (!(val & 1))
							{
								RE(r)++;
								val >>= 1;
							}

							condz = 0;
						}

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						SETUNSETF(condz, regs->eflags, VM86_EFLAGS_ZF);
						regs->eip += 1 + 1 + rm_len;
						break;
					}

					case 0xBD: /* BSR r16/32, r/m16/32 */
					{
						int rm_len = 0;
						const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, op_size, &rm_len);
						uint32_t val = RE(a2);
						uint32_t mask = 0x80000000u;
						union vm86_gpr r;
						int condz = 1;

						RE(r) = 31;

						if (op_size == 2)
						{
							val = RX(a2);
							mask = 0x8000u;
							RE(r) = 15;
						}

						if (val)
						{
							while (!(val & mask))
							{
								RE(r)--;
								val <<= 1;
							}

							condz = 0;
						}

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						SETUNSETF(condz, regs->eflags, VM86_EFLAGS_ZF);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0xA3: /* BT r/m16/32, r16/32 */
					{
						int rm_len = 0;
						const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, op_size, &rm_len);
						const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						int cond = 0;

						if (op_size == 2)
							cond = (RX(a1) >> RX(a2)) & 1;
						else
							cond = (RE(a1) >> RE(a2)) & 1;

						SETUNSETF(cond, regs->eflags, VM86_EFLAGS_CF);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0xBA: /* BT/BTC/BTR/BTS r/m16/32, imm8 */
					{
						int rm_len = 0;
						const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, op_size, &rm_len);
						const uint8_t a2 = read_mem_byte(vm86, phys_addr + 2 + rm_len);
						union vm86_gpr r;
						int condc = 0;
						int cond = 0;
						int leave = 0;

						if (op_size == 2)
							condc = (RX(a1) >> a2) & 1;
						else
							condc = (RE(a1) >> a2) & 1;

						switch (instr[2] & VM86_OP_REG_MASK)
						{
							case VM86_OP_REG_4: /* BT */
								cond = condc;
								break;

							case VM86_OP_REG_7: /* BTC */
								cond = !condc;
								break;

							case VM86_OP_REG_6: /* BTR */
								cond = 0;
								break;

							case VM86_OP_REG_5: /* BTS */
								cond = 1;
								break;

							case VM86_OP_REG_0:
							case VM86_OP_REG_1:
							case VM86_OP_REG_2:
							case VM86_OP_REG_3:
								trigger_excp(vm86, VM86_EXCP_UD, 0);
								leave = 1;
								break;
						}

						if (leave)
							break;

						SETUNSETF(condc, regs->eflags, VM86_EFLAGS_CF);

						r = a1;
						if (op_size == 2)
							SETUNSETF(cond, RX(r), 1u << a2);
						else
							SETUNSETF(cond, RE(r), 1u << a2);

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 2, 0, addr_size, op_size, NULL);

						regs->eip += 1 + 1 + rm_len + 1;
						break;
					}
					case 0xBE: /* MOVSX r16/32, r/m8 */
					{
						int rm_len = 0;
						const union vm86_gpr a2 = VM86_GPR((int32_t)(int8_t)get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, &rm_len));

						set_arg_from_mod_reg_rm(vm86, a2, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0xBF: /* MOVSX r32, r/m16 */
					{
						int rm_len = 0;
						const uint32_t a2 = (uint32_t)(int32_t)(int16_t)get_arg16_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, &rm_len);

						set_arg32_from_mod_reg_rm(vm86, segment, phys_addr + 2, 1, addr_size, NULL, a2);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0xB6: /* MOVZX r16/32, r/m8 */
					{
						int rm_len = 0;
						const union vm86_gpr a2 = VM86_GPR(get_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, &rm_len));

						set_arg_from_mod_reg_rm(vm86, a2, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0xB7: /* MOVZX r32, r/m16 */
					{
						int rm_len = 0;
						const uint32_t a2 = (uint32_t)get_arg16_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, &rm_len);

						set_arg32_from_mod_reg_rm(vm86, segment, phys_addr + 2, 1, addr_size, NULL, a2);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0x90: /* SETO r/m8 */
					case 0x91: /* SETNO r/m8 */
					case 0x92: /* SETB/SETC/SETNAE r/m8 */
					case 0x93: /* SETNB/SETNC/SETAE r/m8 */
					case 0x94: /* SETE/SETZ r/m8 */
					case 0x95: /* SETNE/SETNZ r/m8 */
					case 0x96: /* SETBE/SETNA r/m8 */
					case 0x97: /* SETNBE/SETA r/m8 */
					case 0x98: /* SETS r/m8 */
					case 0x99: /* SETNS r/m8 */
					case 0x9A: /* SETP/SETPE r/m8 */
					case 0x9B: /* SETNP/SETPO r/m8 */
					case 0x9C: /* SETL/SETNGE r/m8 */
					case 0x9D: /* SETNL/SETGE r/m8 */
					case 0x9E: /* SETLE/SETNG r/m8 */
					case 0x9F: /* SETNLE/SETG r/m8 */
					{
						int rm_len = 0;
						uint8_t r = !!check_cc(vm86, instr[1]);

						set_arg8_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, &rm_len, r);
						regs->eip += 1 + 1 + rm_len;
						break;
					}
					case 0xA5: /* SHLD r/m16/32, r16/32, cl */
					case 0xA4: /* SHLD r/m16/32, r16/32, imm8 */
					{
						int rm_len = 0;
						const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, op_size, &rm_len);
						const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						const uint8_t a3 = (instr[0] == 0xA5) ? RL(regs->ecx) : read_mem_byte(vm86, phys_addr + 2 + rm_len);
						const uint8_t shift = a3 & 0x1F;
						union vm86_gpr r;
						int condc = 0;
						int condo = 0;

						if (op_size == 2)
						{
							RX(r) = (RX(a1) << shift) | (RX(a2) >> (16 - shift));
							condc = (RX(a1) >> (16 - shift)) & 1;
							condo = (RX(a1) ^ RX(r)) >> 15;
						}
						else
						{
							RE(r) = (RE(a1) << shift) | (RE(a2) >> (32 - shift));
							condc = (RE(a1) >> (32 - shift)) & 1;
							condo = (RE(a1) ^ RE(r)) >> 31;
						}

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						SETUNSETF(condc, regs->eflags, VM86_EFLAGS_CF);
						SETUNSETF(condo, regs->eflags, VM86_EFLAGS_OF);
						regs->eip += 1 + 1 + rm_len + (instr[0] == 0xA4);
						break;
					}
					case 0xAD: /* SHRD r/m16/32, r16/32, cl */
					case 0xAC: /* SHRD r/m16/32, r16/32, imm8 */
					{
						int rm_len = 0;
						const union vm86_gpr a1 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 0, addr_size, op_size, &rm_len);
						const union vm86_gpr a2 = get_arg_from_mod_reg_rm(vm86, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						const uint8_t a3 = (instr[0] == 0xAD) ? RL(regs->ecx) : read_mem_byte(vm86, phys_addr + 2 + rm_len);
						const uint8_t shift = a3 & 0x1F;
						union vm86_gpr r;
						int condc = 0;
						int condo = 0;

						if (op_size == 2)
						{
							RX(r) = (RX(a1) >> shift) | (RX(a2) << (16 - shift));
							condc = (RX(a1) >> (shift - 1)) & 1;
							condo = (RX(a1) ^ RX(r)) >> 15;
						}
						else
						{
							RE(r) = (RE(a1) >> shift) | (RE(a2) << (32 - shift));
							condc = (RE(a1) >> (shift - 1)) & 1;
							condo = (RE(a1) ^ RE(r)) >> 31;
						}

						set_arg_from_mod_reg_rm(vm86, r, segment, phys_addr + 2, 1, addr_size, op_size, NULL);
						SETUNSETF(condc, regs->eflags, VM86_EFLAGS_CF);
						SETUNSETF(condo, regs->eflags, VM86_EFLAGS_OF);
						regs->eip += 1 + 1 + rm_len + (instr[0] == 0xAC);
						break;
					}

					/* i686 (aka Pentium Pro) */

					case 0x0B: /* UD2 */
					{
						char buf[sizeof(VM86_COP_QUIT)];
						int result;

						result = vm86_read_segment(vm86, buf, VOIDPTR(phys_addr), sizeof(buf));

						if (result == 0)
							result = memcmp(buf, VM86_COP_QUIT, sizeof(buf));

						if (result == 0)
							leave = 1;
						else
							trigger_excp(vm86, VM86_EXCP_UD, 0);

						/* Don't increment EIP. */
						break;
					}

					default:
						trigger_excp(vm86, VM86_EXCP_UD, 0);
				}
				break;
			}

			default:
				trigger_excp(vm86, VM86_EXCP_UD, 0);
		}
	}

	if (vm86->shutdown)
	{
		printk("vm86: VM %p was shut down unexpectedly.", vm86);
		return -1;
	}
	return 0;
}

/* timeout_ms max = 65 sec => don't lockup forever if we get stuck */
int vm86_exec(struct vm86_ctx *vm86, uint16_t timeout_ms)
{
	/* Reset to clean state */
	vm86->shutdown = 0;

	vm86_compute_segs(vm86);
	/* Lock resources (mem/ports/devices/...) claimed by the vm */
	vm86_run(vm86, timeout_ms);
	/* Unlock */
	return 0;
}
