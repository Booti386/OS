SUBDIRS-y :=

BINS-y := builtin.o
LIBS-builtin.o-y :=
OBJS-builtin.o-y := \
	apmdrv.c.o

CFLAGS-y += -I$(TOP)/drivers
LDFLAGS-y += $(LDFLAGS_RELOC-y)
