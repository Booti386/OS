SUBDIRS-y := apm acpi

BINS-y := builtin.o
LIBS-builtin.o-y := \
	apm/builtin.o \
	acpi/builtin.o
OBJS-builtin.o-y :=

LDFLAGS-y += $(LDFLAGS_RELOC-y)
