/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef VBEDRV_H
#define VBEDRV_H 1

#include <os/types.h>

#include "info.h"
#include "modes.h"

#define VBE_MAX_MODES 1024

struct vbe_ctx
{
	struct vm86_ctx *vm;

	union vbe_ver version;
	union vbe_ver oem_version;

	char oem_name[64];
	char vendor_name[64];
	char product_name[64];
	char product_rev[64];

	union vbe_caps caps_flag;
	uint32_t mem_len;

	uint16_t nb_modes;
	uint16_t mode_nums[VBE_MAX_MODES];
	struct vbe_mode_info *mode_infos;

	uint16_t cur_mode_idx;
	struct vbe_mode_info *cur_mode;
	void *fb;
	uint32_t fb_len;
};

#endif /* VBEDRV_H */
