/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <os/types.h>

struct task_context_type_t
{
	uint16_t env      : 2;
	uint16_t exec_ctx : 2;
	uint16_t reserved : 12;
	uint16_t arch;
};
typedef struct task_context_type_t task_context_type_t;

struct __attribute__((packed)) task_context_t
{
	uint32_t             pid;
	uint32_t             gid, uid;
	uint8_t              priority;
	task_context_type_t  type;
	/*mm_context_t        *mm_ctx;
	cpu_context_t       *cpu_ctx;
	debug_context_t     *dbg_ctx;
	io_context_t        *io_ctx;
	thread_context_t    *thread_ctx;
	net_context_t       *net_ctx;*/
};
typedef struct task_context_t task_context_t;
