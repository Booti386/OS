SUBDIRS-y :=

BINS-y := builtin.o
LIBS-builtin.o-y :=
OBJS-builtin.o-y := \
	gcc/stack_protector.c.o \
	kmalloc.c.o \
	lib.c.o \
	string.c.o \
	printk.c.o \
	trace.c.o

LDFLAGS-y += $(LDFLAGS_RELOC-y)
