/*
 * Copyright (C) 2016-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef MACROS_H
#define MACROS_H

#if __ASSEMBLER__
# pragma GCC system_header
#endif

#define offsetof(type, member) __builtin_offsetof(type, member)

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#define SIZEOF_UNTIL_MEMBER(type, member) (offsetof(type, member) + sizeof(((type *)NULL)->member))

#define STRINGIFY_(...) #__VA_ARGS__
#define STRINGIFY(...) STRINGIFY_(__VA_ARGS__)

#endif
