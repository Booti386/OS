/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "memory_map.h"
#include "arch/arch.h"
#include "common/printk.h"
#include <fs/kmicrofs.h>
#include "drivers/fd/fd.h"
#include "drivers/fs/corefs.h"
#include "drivers/input/kbd/ps2/ps2kbd.h"
#include "drivers/video/vbe/vbe.h"
#include "drivers/bus/pci/pci.h"
#include "drivers/bus/pci/pcie.h"
#include "drivers/power/acpi/acpi.h"
#include "arch/mmu.h"
#include "common/trace.h"

void _start(void *kmicrofs)
{
	static char b[1024 * 1024];
	(void)kmicrofs;
	(void)b;

	arch_setup();

	printk("Booti's booting...");

	fs_kmicrofs_setup();
	fs_kmicrofs_load(kmicrofs);

	int fd_ksyms = fs_kmicrofs_open("kernel.syms");
	fs_kmicrofs_read(fd_ksyms, 0, b, sizeof(b));

	printk("%s", b);

	/* ... */

	//ps2kbd_setup();

	vbe_setup();

	pci_setup();
	pcie_setup();

	acpi_setup();

	arch_halt();
}
