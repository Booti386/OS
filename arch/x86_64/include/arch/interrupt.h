/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H 1

typedef void (*arch_interrupt_irq_handler_t)(void);

extern void arch_disable_interrupt(void);
extern void arch_enable_interrupt(void);
extern void arch_disable_interrupt_push_state(void);
extern void arch_enable_interrupt_push_state(void);
extern void arch_interrupt_pop_state(void);
extern void arch_irq_add_handler(unsigned int num, arch_interrupt_irq_handler_t handler);
extern void arch_interrupt_irq_run_handlers(unsigned int num);
extern int arch_interrupt_setup(void);

#endif /* INTERRUPT_H */
