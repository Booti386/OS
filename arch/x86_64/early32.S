/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <gas_intrin.h>
#include <asm/gdt.h>

#define CR0_PG          (1 << 31)
#define CR4_PAE         (1 << 5)

#define IA32_EFER       (0xC0000080)
#define IA32_EFER_LME   (1 << 8)

#define PML4E_PRESENT   (1 << 0)
#define PML4E_RW        (1 << 1)
#define PML4E_PDPTADDR  (0xFFFFFFFF << 12)

#define PDPTE_PRESENT   (1 << 0)
#define PDPTE_RW        (1 << 1)
#define PDPTE_PDADDR    (0xFFFFFFFF << 12)

#define PDE_PRESENT     (1 << 0)
#define PDE_RW          (1 << 1)
#define PDE_PGADDR      (0xFFFFFFFF << 12)

#define PTE_PRESENT     (1 << 0)
#define PTE_RW          (1 << 1)
#define PTE_PGADDR      (0xFFFFFFFF << 12)

#define PG_SIZE         (0x1000)

.text
.code32

_early_code_start:

FUNC_DECL_NOPRE(_early_start)
	cli

	/* Disable the PIC, we do not really need to be disturbed by IRQs at this time. */
	movb $0xFF, %al
	outb %al, $0xA1
	outb %al, $0x21

	xorl %ebx, %ebx
	xorl %ecx, %ecx
	xorl %edx, %edx
	xorl %edi, %edi
	xorl %esi, %esi
	xorl %ebp, %ebp
	/* Don't clear esp: we need a stack. */

	calll .Laddr
.Laddr:
	/* Save eip in ebx for relative addressing. */
	popl %ebx

	/* Clear the bss section. */
	xorl %eax, %eax
	leal (__bss_start - .Laddr)(%ebx), %edi
	leal (__bss_end - .Laddr)(%ebx), %ecx
	subl %edi, %ecx
	shrl $2, %ecx
	cld
	rep stosl

	/* Fetch & save kmicrofs ptr. */
	popl %eax
	movl %eax, (__early_kmicrofs_ptr - .Laddr)(%ebx)

	/* Setup a temporary stack. */
	leal (early_stack_top - .Laddr)(%ebx), %esp

	/* Save kernel physical address. */
	leal (__kernel_start - .Laddr)(%ebx), %eax
	movl %eax, (__kernel_physaddr - .Laddr)(%ebx)

	/* Init page pointer. */
	leal (early_pg_start - .Laddr)(%ebx), %eax
	movl %eax, (early_pg_ptr - .Laddr)(%ebx)

	/* Map identity early code. */
	leal (_early_code_start - .Laddr)(%ebx), %edi
	leal (_early_code_end - .Laddr)(%ebx), %esi
	call _early_map_identity_32

	/* Map identity kernel gdt. */
	leal (__kernel_gdt - .Laddr)(%ebx), %edi
	leal (__kernel_gdt_end - .Laddr)(%ebx), %esi
	call _early_map_identity_32

	/* Map whole kernel to its virtual address. */
	leal (__kernel_start - 0xFFFFFFFF00000000), %edi
	leal (__kernel_end - 0xFFFFFFFF00000000), %eax
	leal (__kernel_start - .Laddr)(%ebx), %esi
	call _early_map_kernel

	/* Map the first ~2GB. */
	leal (__kernel_vm_start - 0xFFFFFFFF00000000), %edi
	leal (__kernel_vm_end - 0xFFFFFFFF00000000), %eax
	xorl %esi, %esi
	call _early_map_kernel

	/* Enable PAE. */
	movl %cr4, %eax
	orl $CR4_PAE, %eax
	movl %eax, %cr4
	
	/* Load PML4 table address. */
	leal (early_pg_start - .Laddr)(%ebx), %eax
	movl %eax, %cr3

	/* Enable IA-32e. */
	movl $IA32_EFER, %ecx
	rdmsr
	orl $IA32_EFER_LME, %eax
	wrmsr

	/* Setup GDT. */
	leal (__kernel_gdt - .Laddr)(%ebx), %eax
	movl %eax, (early_gdtr.addr - .Laddr)(%ebx)
	lgdtl (early_gdtr - .Laddr)(%ebx)

	/* Enable paging. */
	movl %cr0, %eax
	orl $CR0_PG, %eax
	movl %eax, %cr0

	/* Adjust stack address. */
	leal (.Llret_stack - .Laddr)(%ebx), %esp

	/* Jump to 64 bits early routine. */
	movl $GDT_CODESEG64, 4(%esp)
	leal (_early_trampoline_64 - .Laddr)(%ebx), %eax
	movl %eax, (%esp)
	lretl
FUNC_END(_early_start)

ALIGN(4)
.Llret_stack:
	.skip 8
.Llret_stack_top:

.code64

FUNC_DECL_NOPRE(_early_trampoline_64)
	/* Reload GDT address. */
	lgdtq (__kernel_gdtr)

	/* Reset segment selectors. */
	xorw %ax, %ax
	movw %ax, %ds
	movw %ax, %es
	movw %ax, %fs
	movw %ax, %gs
	movw %ax, %ss

	leaq (_early64_start), %rax
	jmpq *%rax
FUNC_END(_early_trampoline_64)

.code32

/*
 * edi: Start address
 * esi: End address
 */
STATIC_FUNC_DECL_NOPRE(_early_map_identity_32)
_early_map_identity_32:
	push %ecx
	push %edi

	movl %esi, %ecx
	subl %edi, %ecx
	shrl $12, %ecx
	incl %ecx
.Lmap_identity_loop:
	call _early_map_single_identity_32
	addl $0x1000, %edi
	decl %ecx
	jnz .Lmap_identity_loop

	popl %edi
	popl %ecx
	ret
FUNC_END(_early_map_identity_32)

/*
 * edi: Address
 */
STATIC_FUNC_DECL_NOPRE(_early_map_single_identity_32)
_early_map_single_identity_32:
	push %eax
	push %ecx
	push %edx
	push %edi

	andl $~(PG_SIZE-1), %edi
	leal (early_pg_start - .Laddr)(%ebx), %eax

.Lmap_identity_pml4:
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_identity_pdpt
	movl $(PML4E_PRESENT | PML4E_RW), (%eax)
	movl (early_pg_ptr - .Laddr)(%ebx), %ecx
	addl $0x1000, %ecx
	orl %ecx, (%eax)
	movl %ecx, (early_pg_ptr - .Laddr)(%ebx)

.Lmap_identity_pdpt:
	movl %ecx, %eax
	movl %edi, %edx
	shrl $30, %edx
	andl $0x1FF, %edx
	shll $3, %edx
	addl %edx, %eax
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_identity_pd
	movl $(PDPTE_PRESENT | PDPTE_RW), (%eax)
	movl (early_pg_ptr - .Laddr)(%ebx), %ecx
	addl $0x1000, %ecx
	orl %ecx, (%eax)
	movl %ecx, (early_pg_ptr - .Laddr)(%ebx)

.Lmap_identity_pd:
	movl %ecx, %eax
	movl %edi, %edx
	shrl $21, %edx
	andl $0x1FF, %edx
	shll $3, %edx
	addl %edx, %eax
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_identity_pt
	movl $(PDE_PRESENT | PDE_RW), (%eax)
	movl (early_pg_ptr - .Laddr)(%ebx), %ecx
	addl $0x1000, %ecx
	orl %ecx, (%eax)
	movl %ecx, (early_pg_ptr - .Laddr)(%ebx)

.Lmap_identity_pt:
	movl %ecx, %eax
	movl %edi, %edx
	shrl $12, %edx
	andl $0x1FF, %edx
	shll $3, %edx
	addl %edx, %eax
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_identity_end
	movl $(PTE_PRESENT | PTE_RW), (%eax)
	orl %edi, (%eax)

.Lmap_identity_end:
	leal (early_pg_end - .Laddr)(%ebx), %eax
	movl (early_pg_ptr - .Laddr)(%ebx), %edx
	cmpl %eax, %edx
	jge _early_map_overflow

	pop %edi
	popl %edx
	popl %ecx
	popl %eax
	retl
FUNC_END(_early_map_single_identity_32)

/*
 * edi: Virtual start address
 * eax: Virtual end address
 * esi: Physical address
 */
STATIC_FUNC_DECL_NOPRE(_early_map_kernel)
_early_map_kernel:
	push %ecx
	push %edi
	push %esi

	movl %eax, %ecx
	subl %edi, %ecx
	shrl $12, %ecx
	incl %ecx
.Lmap_kernel_loop:
	call _early_map_single_kernel
	addl $0x1000, %esi
	addl $0x1000, %edi
	decl %ecx
	jnz .Lmap_kernel_loop

	popl %esi
	popl %edi
	popl %ecx
	retl
FUNC_END(_early_map_kernel)

/*
 * edi: Virtual (truncated) address
 * esi: Physical address
 */
STATIC_FUNC_DECL_NOPRE(_early_map_single_kernel)
_early_map_single_kernel:
	push %eax
	push %ecx
	push %edx
	push %edi
	push %esi

	andl $~(PG_SIZE-1), %edi
	andl $~(PG_SIZE-1), %esi
	leal (early_pg_start - .Laddr)(%ebx), %eax

.Lmap_pml4:
	movl $0x1FF, %edx /* Set bits 39 - 47 to 1 (kernel addresses are in the form 0xFFFFFFFFXXXXXXXX) */
	shll $3, %edx
	addl %edx, %eax
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_pdpt
	movl $(PML4E_PRESENT | PML4E_RW), (%eax)
	movl (early_pg_ptr - .Laddr)(%ebx), %ecx
	addl $0x1000, %ecx
	orl %ecx, (%eax)
	movl %ecx, (early_pg_ptr - .Laddr)(%ebx)

.Lmap_pdpt:
	movl %ecx, %eax
	movl %edi, %edx
	shrl $30, %edx
	orl $0x1FC, %edx /* Set bits 32 - 38 to 1 (kernel addresses are in the form 0xFFFFFFFFXXXXXXXX) */
	shll $3, %edx
	addl %edx, %eax
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_pd
	movl $(PDPTE_PRESENT | PDPTE_RW), (%eax)
	movl (early_pg_ptr - .Laddr)(%ebx), %ecx
	addl $0x1000, %ecx
	orl %ecx, (%eax)
	movl %ecx, (early_pg_ptr - .Laddr)(%ebx)

.Lmap_pd:
	movl %ecx, %eax
	movl %edi, %edx
	shrl $21, %edx
	andl $0x1FF, %edx
	shll $3, %edx
	addl %edx, %eax
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_pt
	movl $(PDE_PRESENT | PDE_RW), (%eax)
	movl (early_pg_ptr - .Laddr)(%ebx), %ecx
	addl $0x1000, %ecx
	orl %ecx, (%eax)
	movl %ecx, (early_pg_ptr - .Laddr)(%ebx)

.Lmap_pt:
	movl %ecx, %eax
	movl %edi, %edx
	shrl $12, %edx
	andl $0x1FF, %edx
	shll $3, %edx
	addl %edx, %eax
	movl (%eax), %ecx
	andl $~(PG_SIZE-1), %ecx
	cmpl $0, (%eax)
	jne .Lmap_end
	movl $(PTE_PRESENT | PTE_RW), (%eax)
	orl %esi, (%eax)

.Lmap_end:
	leal (early_pg_end - .Laddr)(%ebx), %eax
	movl (early_pg_ptr - .Laddr)(%ebx), %edx
	cmpl %eax, %edx
	jge _early_map_overflow

	popl %esi
	popl %edi
	popl %edx
	popl %ecx
	popl %eax
	retl
FUNC_END(_early_map_single_kernel)

/* Freeze if there is not enough memory to map the whole kernel. */
STATIC_FUNC_DECL_NOPRE(_early_map_overflow)
_early_map_overflow:
	cli
	hlt
	jmp _early_map_overflow
FUNC_END(_early_map_overflow)

_early_code_end:

.data

ALIGN(8)
early_gdtr:
early_gdtr.limit: .word 11 * 8 - 1
early_gdtr.addr:  .quad 0

ALIGN(8)
EXPORT(__kernel_gdtr)
__kernel_gdtr:
/* Limit */
.word 11 * 8 - 1
/* Address */
.quad __kernel_gdt

ALIGN(8)
EXPORT(__kernel_gdt)
__kernel_gdt:
/* Null descriptor */
.quad 0
/* 16 bits code */
.quad 0x00009A000000FFFF
/* 16 bits data */
.quad 0x000092000000FFFF
/* 32 bits code */
.quad 0x00CF9A000000FFFF
/* 32 bits data */
.quad 0x00CF92000000FFFF
/* 32 bits user code */
.quad 0x00CFFA000000FFFF
/* 32 bits user data */
.quad 0x00CFF2000000FFFF
/* 64 bits code */
.quad 0x0020980000000000
/* 64 bits data */
.quad 0x0000920000000000
/* 64 bits user code */
.quad 0x0020F80000000000
/* 64 bits user data */
.quad 0x0000F20000000000
EXPORT(__kernel_gdt_end)
__kernel_gdt_end:

.bss

ALIGN(8)
early_pg_ptr: .quad 0

ALIGN(8)
early_stack:
.skip 256
early_stack_top:

ALIGN(0x1000)
early_pg_start:
.skip 0x500000
early_pg_end:

ALIGN(8)
EXPORT(__kernel_physaddr)
__kernel_physaddr: .quad 0

ALIGN(8)
EXPORT(__early_kmicrofs_ptr)
__early_kmicrofs_ptr: .quad 0
