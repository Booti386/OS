/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"
#include "common/kmalloc.h"
#include "common/string.h"
#include "common/printk.h"
#include "arch/mmu.h"

extern int __kernel_start, __kernel_end;
extern int __kernel_vm_start, __kernel_vm_end;
extern uint32_t __kernel_physaddr;

#define PG_PRESENT (1 << 0)
#define PG_RW      (1 << 1)

#define PAGE_SIZE  0x1000
#define PAGE_MASK  (~(PAGE_SIZE - 1))

#define PAGE_ALIGN 0x1000
#define PG2M_ALIGN 0x200000

/* Bootstrap paging entry. */
static uint32_t __attribute__((aligned(PAGE_ALIGN))) root_pd[1024];

static void *pg_virt_init_ptr = NULL;
static void *pg_init_ptr = NULL;
static void *pg_virt_cur_ptr = NULL;
static void *pg_cur_ptr = NULL;

void *arch_map_phys_pages(void *phys, uint32_t n);

void *arch_get_phys_addr(void *virt)
{
	uint32_t virt_addr = (uint32_t)virt;

	uint16_t pd_idx = (virt_addr >> 22) & 0x3FF;
	uint16_t pt_idx = (virt_addr >> 12) & 0x3FF;

	uint32_t *pg_ptr = NULL;

	if(!root_pd[pd_idx])
		return NULL;
	pg_ptr = arch_map_phys_pages(VOIDPTR(root_pd[pd_idx]  & PAGE_MASK), 1);

	if(!pg_ptr[pt_idx])
		return NULL;

	return VOIDPTR((pg_ptr[pt_idx] & PAGE_MASK) | (virt_addr & (PAGE_SIZE - 1)));
}

static int arch_map_virt_to_phys_pages(void *virt, void *phys, uint32_t n)
{
	uint32_t virt_addr = (uint32_t)virt;
	uint32_t phys_addr = (uint32_t)phys;

	uint16_t pd_idx;
	uint16_t pt_idx;

	uint32_t *pg_ptr = 0;

	printk("mmu: arch_map_virt_to_phys_pages: semi-stub!");

	for(int i=0; i<n; i++) {
		pd_idx   = (virt_addr >> 22) & 0x3FF;
		pt_idx   = (virt_addr >> 12) & 0x3FF;

		if(!root_pd[pd_idx]) {
			root_pd[pd_idx] = PG_PRESENT | PG_RW;
			root_pd[pd_idx] |= (uint32_t)pg_cur_ptr;

			pg_ptr = arch_map_phys_pages(pg_cur_ptr, 1);

			memset(pg_ptr, 0, PAGE_SIZE);

			pg_cur_ptr = VOIDPTR((uint32_t)pg_cur_ptr + PAGE_SIZE);
		} else {
			pg_ptr = arch_map_phys_pages(VOIDPTR(root_pd[pd_idx] & PAGE_MASK), 1);
		}

		if(!pg_ptr[pt_idx]) {
			pg_ptr[pt_idx] = PG_PRESENT | PG_RW;
			pg_ptr[pt_idx] |= phys_addr;
		} else {
			/* Already mapped */
			return -1;
		}

		virt_addr += PAGE_SIZE;
		phys_addr += PAGE_SIZE;
	}

	return 0;
}

void *arch_map_phys_pages(void *phys, uint32_t n)
{
	uint32_t phys_addr = (uint32_t)phys;
	void *virt;

	/* Check if the range is already mapped. */
	if(phys_addr + PAGE_SIZE * n <= (uint32_t)&__kernel_vm_end - (uint32_t)&__kernel_vm_start)
		return VOIDPTR((uint32_t)&__kernel_vm_start + phys_addr);

	pg_virt_cur_ptr = VOIDPTR((uint32_t)pg_virt_cur_ptr - n * PAGE_SIZE);

	virt = pg_virt_cur_ptr;

	arch_map_virt_to_phys_pages(virt, phys, n);

	return virt + (phys_addr & PAGE_SIZE);
}

void *arch_map_phys(void *phys, uint32_t len)
{
	uint32_t len_aligned = len & PAGE_MASK;

	if(len - len_aligned != 0)
		len_aligned += PAGE_SIZE;

	len_aligned >>= 12;

	return arch_map_phys_pages(phys, len_aligned);
}

void *arch_map_pages(void *virt, uint32_t n)
{
	void *phys = pg_cur_ptr;

	pg_cur_ptr = VOIDPTR((uint32_t)pg_cur_ptr + n * PAGE_SIZE);

	arch_map_virt_to_phys_pages(virt, phys, n);

	return virt + ((uint32_t)phys & PAGE_MASK);;
}

void *arch_map(void *virt, uint32_t len)
{
	uint32_t len_aligned = len & PAGE_MASK;

	if(len - len_aligned != 0)
		len_aligned += PAGE_SIZE;

	len_aligned >>= 12;

	return arch_map_pages(virt, len_aligned);
}

int arch_unmap(void *virt)
{
	printk("mmu: arch_unmap: stub!");

	return 0;
}

void *arch_alloc_pages(uint32_t n)
{
	void *phys = pg_cur_ptr;

	pg_cur_ptr = VOIDPTR((uint32_t)pg_cur_ptr + n * PAGE_SIZE);

	return arch_map_phys_pages(phys, n);
}

void *arch_alloc(uint32_t len)
{
	uint32_t len_aligned = len & PAGE_MASK;

	if(len - len_aligned != 0)
		len_aligned += PAGE_SIZE;

	len_aligned >>= 12;

	return arch_alloc_pages(len_aligned);
}

int arch_paging_setup(void *mem_map)
{
	uint32_t kernel_size = (uint32_t)&__kernel_end - (uint32_t)&__kernel_start;
	uint32_t kernel_vm_size = (uint32_t)&__kernel_vm_end - (uint32_t)&__kernel_vm_start;

	if(!mem_map) {
		pg_virt_init_ptr = VOIDPTR((uint32_t)&__kernel_start - PAGE_SIZE);
		pg_init_ptr = VOIDPTR(((uint32_t)&__kernel_vm_start - (uint32_t)&__kernel_start) + __kernel_physaddr);
		pg_virt_cur_ptr = pg_virt_init_ptr;
		pg_cur_ptr = pg_init_ptr;

		/* Map the kernel. */
		arch_map_virt_to_phys_pages(&__kernel_start, VOIDPTR(__kernel_physaddr), kernel_size / PAGE_SIZE + 1);

		/* Map the first ~2GB (kernel vm space). */
		arch_map_virt_to_phys_pages(&__kernel_vm_start, VOIDPTR(0), kernel_vm_size / PAGE_SIZE + 1);

		/* Map identity the 1st MB. */
		arch_map_virt_to_phys_pages(VOIDPTR(0), VOIDPTR(0), 1024 * 1024 / PAGE_SIZE);

		__asm__ __volatile__("movl %0, %%cr3" :: "r"(((uint32_t)&root_pd - (uint32_t)&__kernel_start) + __kernel_physaddr));
	}

	return 0;
}
