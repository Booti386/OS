/*
 * Copyright (C) 2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef GAS_INTRIN_H
#define GAS_INTRIN_H 1

#include <macros.h>

#if __ASSEMBLER__
.macro func_decl_common name, global=1, nopreamble=0
	.pushsection .text
	.if \global
		.global \name
	.endif
	.type \name, STT_FUNC
	.func \name, \name
	\name :
	.if \nopreamble
		.cfi_startproc simple
	.else
		.cfi_startproc
	.endif
.endm

.macro static_func_decl name, nopreamble=0
	func_decl_common \name, 0, \nopreamble
.endm

.macro func_decl name, nopreamble=0
	func_decl_common \name, 1, \nopreamble
.endm

.macro func_end name
	.cfi_endproc
	.endfunc
	.size \name, .-\name
	.popsection
.endm
#endif

#define ALIGN(x) .align x
#define EXPORT(x) .global x
#define FUNC_DECL(name) func_decl name
#define FUNC_DECL_NOPRE(name) func_decl name, nopreamble=1
#define STATIC_FUNC_DECL(name) static_func_decl name
#define STATIC_FUNC_DECL_NOPRE(name) static_func_decl name, nopreamble=1
#define FUNC_END(name) func_end name

#endif /* GAS_INTRIN_H */
