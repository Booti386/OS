/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef MODES_H
#define MODES_H 1

#include <os/types.h>

struct vbe_ctx;

#define VBE_MEM_TYPE_TEXT         0
#define VBE_MEM_TYPE_CGA          1
#define VBE_MEM_TYPE_HERCULES     2
#define VBE_MEM_TYPE_PLANAR       3
#define VBE_MEM_TYPE_PACKED_PIX   4
#define VBE_MEM_TYPE_256          5
#define VBE_MEM_TYPE_DIRECT_COLOR 6
#define VBE_MEM_TYPE_YUV          7

union vbe_mode_attrs
{
	uint16_t attrs;
	struct __attribute__((packed)) {
		/* VBE 1.x */
		uint16_t hardware_support    : 1;
		uint16_t res0                : 1;
		uint16_t tty_funcs           : 1;
		uint16_t color               : 1;
		uint16_t graphics            : 1;

		/* VBE 2.0 */
		uint16_t no_vga_compat       : 1;
		uint16_t no_vga_win_mode     : 1;
		uint16_t linear_fb           : 1;
		uint16_t dbl_scan            : 1;

		/* VBE 3.0 */
		uint16_t interlaced          : 1;
		uint16_t triple_buffer       : 1;
		uint16_t dual_disp_startaddr : 1;

		uint16_t res1 : 4;
	} bits;
};

union vbe_window_attrs
{
	uint8_t attrs;
	struct __attribute__((packed)) {
		uint8_t exists   : 1;
		uint8_t readable : 1;
		uint8_t writable : 1;
		uint8_t res0     : 5;
	} bits;
};

struct vbe_mode_info
{
	/* VBE 1.0 */
	union vbe_mode_attrs mode_attrs;
	union vbe_window_attrs win_a_attrs;
	union vbe_window_attrs win_b_attrs;
	uint16_t win_granularity;
	uint16_t win_size;
	uint16_t win_a_startseg;
	uint16_t win_b_startseg;
	uint32_t win_pos_func_far;
	uint16_t bp_scan_line;

	/* VBE 1.2 */
	uint16_t width;
	uint16_t height;
	uint8_t char_width;
	uint8_t char_height;
	uint8_t nb_planes;
	uint8_t bpp;
	uint8_t nb_banks;
	uint8_t mem_type;
	uint8_t bank_len;
	uint8_t nb_img_pages;
	uint8_t res0;

	/* VBE 1.2 */
	uint8_t red_mask_size;
	uint8_t red_field_pos;
	uint8_t green_mask_size;
	uint8_t green_field_pos;
	uint8_t blue_mask_size;
	uint8_t blue_field_pos;
	uint8_t reserved_mask_size;
	uint8_t reserved_field_pos;
	uint8_t direct_color_mode_info;

	/* VBE 2.0 */
	uint32_t fb_phys_addr;
	uint32_t offscrn_ptr;
	uint16_t offscrn_len;

	/* VBE 3.0 */
	uint16_t lin_bpscanline;
	uint8_t bank_nb_imgs;
	uint8_t lin_nb_imgs;

	uint8_t lin_red_mask_size;
	uint8_t lin_red_field_pos;
	uint8_t lin_green_mask_size;
	uint8_t lin_green_field_pos;
	uint8_t lin_blue_mask_size;
	uint8_t lin_blue_field_pos;
	uint8_t lin_reserved_mask_size;
	uint8_t lin_reserved_field_pos;

	uint32_t max_pix_clock;

	uint8_t res1[189];
};

extern int vbe_set_mode(struct vbe_ctx *ctx, uint16_t mode);
extern int vbe_fetch_modes(struct vbe_ctx *ctx);

#endif /* MODES_H */
