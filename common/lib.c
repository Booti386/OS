/*
 * Copyright (C) 2014-2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "os/types.h"

unsigned int strtou(const char *str)
{
	unsigned int ret = 0;

	while(*str) {
		if('0' <= *str && *str <= '9')
			/* ret = ret * 10 + str[i] - '0' */
			ret = (ret << 1) + (ret << 3) + *str - '0';
		else
			break;
		str++;
	}
	return ret;
}

int strtoi(const char *str)
{
	int ret = 0;
	int sign = 1;

	if(*str == '-') {
		sign = -1;
		str++;
	} else if(*str == '+') {
		str++;
	}

	while(str) {
		if('0' <= *str && *str <= '9')
			/* ret = ret * 10 + str[i] - '0' */
			ret = (ret << 1) + (ret << 3) + *str - '0';
		else
			break;
		str++;
	}

	return sign * ret;
}

char *utostr(char *dst, unsigned int v)
{
	char c;
	int str_len = 0;

	if(v == 0) {
		dst[0] = '0';
		dst[1] = '\0';
		return dst;
	}

	while(v) {
		dst[str_len++] = '0' + v % 10;
		v /= 10;
	}

	dst[str_len] = '\0';

	for(int i=0; i < (str_len >> 1); i++) {
		c = dst[i];
		dst[i] = dst[str_len - 1 - i];
		dst[str_len - 1 - i] = c;
	}

	return dst;
}

char *itostr(char *dst, int v)
{
	char c;
	int str_len;

	if(v == 0) {
		dst[0] = '0';
		dst[1] = '\0';
		return dst;
	}

	if(v < 0) {
		dst[0] = '-';
		dst++;
		v = -v;
	}

	for(str_len=0; v != 0; str_len++) {
		dst[str_len] = '0' + v % 10;
		v /= 10;
	}

	dst[str_len]='\0';

	for(int i=0; i < (str_len >> 1); i++) {
		c = dst[i];
		dst[i] = dst[str_len - 1 - i];
		dst[str_len - 1 - i] = c;
	}

	return dst;
}

char *x32tostr(char *dst, uint32_t v, int zeroing)
{
	int i;
	int nb_leftshifts;
	char c;

	if(!zeroing && v == 0) {
		dst[0] = '0';
		dst[1] = '\0';
		return dst;
	}

	nb_leftshifts = 0;
	while(!zeroing && !(v >> 28) && v) {
		v <<= 4;
		nb_leftshifts++;
	}

	for(i=0; v; i++)
	{
		c = v >> 28;
		if(c <= 0x09)
			dst[i] = '0' + c;
		else
			dst[i] = 'A' + c - 0xA;
		v <<= 4;
	}

	for(; i<32/4 - nb_leftshifts; i++)
		dst[i]='0';
	dst[i]='\0';

	return dst;
}

char *x64tostr(char *dst, uint64_t v, int zeroing)
{
	int i;
	int nb_leftshifts;
	char c;

	if(!zeroing && v == 0) {
		dst[0] = '0';
		dst[1] = '\0';
		return dst;
	}

	nb_leftshifts = 0;
	while(!zeroing && !(v >> 60) && v) {
		v <<= 4;
		nb_leftshifts++;
	}

	for(i=0; v; i++)
	{
		c = v >> 60;
		if(c <= 0x09)
			dst[i] = '0' + c;
		else
			dst[i] = 'A' + c - 0xA;
		v <<= 4;
	}

	for(; i<64/4 - nb_leftshifts; i++)
		dst[i]='0';
	dst[i]='\0';

	return dst;
}
