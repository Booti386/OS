/*
 * Copyright (C) 2014-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <checksum.h>
#include <arch/mmu.h>
#include <common/string.h>
#include <common/printk.h>
#include <os/types.h>

#include "tables.h"

static int use_xsdt;
static rsdt_t *rsdt;
static xsdt_t *xsdt;

static rsdp_t *rsdp_lookup(void *start, size_t len)
{
	int ret;
	rsdp_t *rsdp;
	char *strt = start;
	size_t rsdp_len;
	size_t limit;

	if (len < sizeof(rsdp->v1))
		return NULL;

	limit = len - sizeof(rsdp->v1);

	for (size_t i = 0; i <= limit; i++)
	{
		rsdp = (rsdp_t *)&strt[i];

		ret = memcmp(rsdp->v1.sig, RSDP_SIG, sizeof(rsdp->v1.sig));
		if (ret != 0)
			continue;

		rsdp_len = 0;
		if (rsdp->v1.rev == 0)
		{
			rsdp_len = RSDP_REV0_LEN;
		}
		else if (rsdp->v1.rev == 2)
		{
			if (len - i < RSDP_REV2_LEN)
				continue;

			if (rsdp->v2.len < RSDP_REV2_LEN)
				continue;

			rsdp_len = rsdp->v2.len;
		}
		else
			continue;

		ret = checksum8(rsdp, rsdp_len);
		if (ret)
			continue;

		printk("ACPI: RSDP %p rev %d \"%s\"", rsdp, rsdp->v1.rev, rsdp->v1.oem_id);

		return rsdp;
	}

	return NULL;
}

static int check_header(desc_header_t *header, char sig[4])
{
	int ret;

	if(header->len < sizeof(*header))
		return -1;

	ret = memcmp(header->sig, sig, sizeof(header->sig));
	if(ret != 0)
		return -1;

	ret = checksum8(header, header->len);
	if(ret != 0)
		return -1;

	return 0;
}

static int check_xsdt(xsdt_t *_xsdt)
{
	int ret;

	ret = check_header(&_xsdt->header, "XSDT");
	if(ret < 0)
		return -1;

	printk("ACPI: XSDT %p rev %d \"%s\" %d entries", _xsdt, _xsdt->header.rev, _xsdt->header.oem_id, (_xsdt->header.len - sizeof(_xsdt->header)) / sizeof(_xsdt->entries[0]));

	return 0;
}

static int check_rsdt(rsdt_t *_rsdt)
{
	int ret;

	ret = check_header(&_rsdt->header, "RSDT");
	if(ret < 0)
		return -1;

	printk("ACPI: RSDT %p rev %d \"%s\" %d entries", _rsdt, _rsdt->header.rev, _rsdt->header.oem_id, (_rsdt->header.len - sizeof(_rsdt->header)) / sizeof(_rsdt->entries[0]));

	return 0;
}

int acpi_setup(void)
{
	int ret;
	rsdp_t *rsdp;
	void *ptr;

	/* Search into BIOS ROM */
	ptr = arch_map_phys(VOIDPTR(0xE0000), 0x20000);
	rsdp = rsdp_lookup(ptr, 0x20000);
	if (!rsdp)
	{
		/* Search into EBDA */
		ptr = arch_map_phys(VOIDPTR(0x9FC00), 1024);
		rsdp = rsdp_lookup(ptr, 1024);
	}

	if (!rsdp)
	{
		printk("ACPI: No valid RSDP found, ACPI is not available on this system.");
		return -1;
	}

	use_xsdt = 0;
	if (rsdp->v1.rev == 2 && rsdp->v2.xsdt_addr)
		use_xsdt = 1;

	if (use_xsdt)
	{
		xsdt = arch_map_phys(VOIDPTR(rsdp->v2.xsdt_addr), sizeof(*xsdt));

		ret = check_xsdt(xsdt);
		if (ret < 0)
			use_xsdt = 0;
	}

	if (!use_xsdt)
	{
		rsdt = arch_map_phys(VOIDPTR(rsdp->v1.rsdt_addr), sizeof(*rsdt));

		ret = check_rsdt(rsdt);
		if (ret < 0)
		{
			printk("ACPI: No valid XSDT/RSDT found, ACPI is not available on this system.");
			return -1;
		}
	}

	return 0;
}
