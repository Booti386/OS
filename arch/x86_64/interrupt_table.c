/*
 * Copyright (C) 2015-2018 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <gas_intrin.h>
#include <macros.h>
#include <arch/arch.h>
#include <arch/interrupt.h>
#include <arch/pic8259a.h>
#include <common/printk.h>
#include <common/trace.h>
#include <os/types.h>

#include "interrupt_table.h"

#define IRQ_BASE 0x20

void arch_excp_handler(unsigned int num, void *rsp)
{
	uint64_t *stack = rsp;

	printk("EXCP 0x%x: cs:rip=0x%x:%p, rflags=%p.", num, stack[1], stack[0], stack[2]);
	print_backtrace(&stack[3]);
	arch_halt();
}

void arch_excp_handler_errcode(unsigned int num, void *rsp, unsigned int errcode)
{
	uint64_t *stack = rsp;

	printk("EXCP 0x%x: cs:rip=0x%x:%p, rflags=%p, errcode=0x%x.", num, stack[1], stack[0], stack[2], errcode);
	print_backtrace(&stack[3]);
	arch_halt();
}

void arch_pgfault_handler_errcode(unsigned int num, void *rsp, unsigned int errcode)
{
	uint64_t *stack = rsp;
	void *addr;

	__asm__ __volatile__("movq %%cr2, %0" : "=r"(addr));
	printk("EXCP PAGE FAULT(0x%x): cs:rip=0x%x:%p, rflags=%p, addr=%p, errcode=0x%x.", num, stack[1], stack[0], stack[2], addr, errcode);
	print_backtrace(&stack[3]);
	arch_halt();
}

void arch_irq_handler(unsigned int num, void *rsp)
{
	(void)rsp;

	num -= IRQ_BASE;
	//printk("IRQ %d.", num);

	arch_interrupt_irq_run_handlers(num);

	/* Before returning from an IRQ, send EOI to the slave PIC if needed, then to the master PIC */
	if (num >= 8)
		arch_pic8259a_slave_eoi();
	arch_pic8259a_master_eoi();
}

void arch_master_spurious_irq_handler(unsigned int num, void *rsp)
{
	(void)rsp;

	num -= IRQ_BASE;
	printk("8259a (master): Spurious IRQ %d.", num);

	/* Do not send any EOI */
}

void arch_slave_spurious_irq_handler(unsigned int num, void *rsp)
{
	(void)rsp;

	num -= IRQ_BASE;
	printk("8259a (slave): Spurious IRQ %d.", num);

	/* Send an EOI only to the master PIC */
	arch_pic8259a_master_eoi();
}

void arch_syscall_handler(unsigned int num, void *rsp)
{
	uint64_t *stack = rsp;

	printk("syscall (%d)", num);
	print_backtrace(&stack[3]);
}

#define HND(num) [0x ## num] = arch_int_wrap_ ## num

static arch_interrupt_handler_t handler_table[INTERRUPT_TABLE_HANDLER_COUNT] = {
	/* CPU exceptions */
	HND(00),
	HND(01),
	HND(02),
	HND(03),
	HND(04),
	HND(05),
	HND(06),
	HND(07),
	HND(08),
	HND(09),
	HND(0A),
	HND(0B),
	HND(0C),
	HND(0D),
	HND(0E),
	HND(0F),
	HND(10),
	HND(11),
	HND(12),
	HND(13),
	HND(14),
	HND(15),
	HND(16),
	HND(17),
	HND(18),
	HND(19),
	HND(1A),
	HND(1B),
	HND(1C),
	HND(1D),
	HND(1E),
	HND(1F),

	/* IRQs */
	HND(20),
	HND(21),
	HND(22),
	HND(23),
	HND(24),
	HND(25),
	HND(26),
	HND(27),
	HND(28),
	HND(29),
	HND(2A),
	HND(2B),
	HND(2C),
	HND(2D),
	HND(2E),
	HND(2F),

	HND(41),
};

#undef HND

const arch_interrupt_handler_t *arch_interrupt_get_handler_table(void)
{
	return handler_table;
}
