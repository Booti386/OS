/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef ASM_MSR_H
#define ASM_MSR_H

#include "os/types.h"

static inline void _wrmsr(uint32_t msr, uint64_t val)
{
	__asm__ __volatile__ ("wrmsr\n" ::"d"((uint32_t)(val >> 32)), "a"((uint32_t)(val & 0xFFFFFFFF)), "c"(msr));
}

static inline uint64_t _rdmsr(uint32_t msr)
{
	uint32_t ret_low, ret_high;
	__asm__ __volatile__ ("rdmsr\n" :"=d"(ret_high), "=a"(ret_low) :"c"(msr));
	return ((uint64_t)ret_low & 0xFFFFFFFF) | ((uint64_t)ret_high << 32);
}

#endif
